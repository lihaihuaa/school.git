<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>学生管理</title>
    <%@ include file="/WEB-INF/pages/css_template.jsp" %>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                    aria-controls="navbar">
                <span class="sr-only">学生管理系统</span> <span class="icon-bar"></span>
                <span class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar2" aria-expanded="false"
                    aria-controls="navbar" style="float: left;">
                <span class="sr-only">切换导航</span> <span class="icon-bar"></span> <span
                    class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<%=path%>/mainUrl">学生管理系统</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <!-- 导航条菜单 -->
            <%@ include file="/WEB-INF/pages/header_nav_template.jsp" %>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div id="navbar2" class="col-sm-3 col-md-2 sidebar">
            <!-- 侧边栏 -->
            <jsp:include page="/WEB-INF/pages/side_nav_template.jsp">
                <jsp:param value="active" name="aStudents"/>
            </jsp:include>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="row">
                <div class="col-sm-7">
                    <div class="input-group">
                        <input id="search" type="text" class="form-control" placeholder="学号" value="">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#selectStudents">搜索</button>
                        </span>
                    </div>
                </div>
                <div class="col-sm-5">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addStudents">添加学生</button>
                </div>
            </div>
            
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>学号</th>
                        <th>姓名</th>
                        <th>年级</th>
                        <th>学院</th>
                        <th>专业</th>
                        <th>班级</th>
                        <th>性别</th>
                        <th>年龄</th>
                        <th>入学年份</th>
                        <th>编辑</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="student" items="${students}" varStatus="status">
                        <tr>
                            <th>${(status.index + 1) + (pb.currPage - 1) * rows}</th>
                            <th>${student.studentNo}</th>
                            <th>${student.studentName}</th>
                            <th>
                                <c:choose>
                                    <c:when test="${student.gender == 1}">
                                        男
                                    </c:when>
                                    <c:otherwise>
                                        女
                                    </c:otherwise>
                                </c:choose>
                            </th>
                            <th>
                                <c:forEach var="grade" items="${grades }">
                                    <c:if test="${grade.gradeNo == student.gradeNo}">${grade.gradeName}</c:if>
                                </c:forEach>
                            </th>
                            <th>
                                <c:forEach var="college" items="${colleges }">
                                    <c:if test="${college.collegeNo == student.collegeNo}">${college.collegeName}</c:if>
                                </c:forEach>
                            </th>
                            <th>
                                <c:forEach var="profession" items="${professions }">
                                    <c:if test="${profession.professionNo == student.professionNo}">${profession.professionName}</c:if>
                                </c:forEach>
                            </th>
                            <th>
                                <c:forEach var="Class" items="${classes }">
                                    <c:if test="${Class.classNo == student.classNo}">${Class.className}</c:if>
                                </c:forEach>
                            </th>
                            <th>${student.age}</th>
                            <th>${student.year}</th>
                            <th>
                                <button class="btn btn-default" 
                                        data-student-no="${student.studentNo }" data-toggle="modal" 
                                        data-target="#updateStudents">编辑
                                </button>
                                <button class="btn btn-danger" 
                                        data-student-no="${student.studentNo }" data-toggle="modal" 
                                        data-target="#deleteStudents">删除
                                </button>
                            </th>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <div>
                <nav aria-label="...">
                    <ul class="pager">
                        <!-- <li class="previous"><a href="#"><span
                                aria-hidden="true">&larr;</span> 上一页</a></li>
                        <li class="next"><a href="#">下一页 <span
                                aria-hidden="true">&rarr;</span></a></li> -->
                        <li class="previous">
                            <p>每页显示</p>
                            <select onchange="location.href='<%=path%>/admin/adminStudentsUrl?rows=' + this.value">
                                <option value="3" <c:if test="${rows == 3}">selected</c:if>>3</option>
                                <option value="5" <c:if test="${rows == 5}">selected</c:if>>5</option>
                                <option value="10" <c:if test="${rows == 10}">selected</c:if>>10</option>
                                <option value="20" <c:if test="${rows == 20}">selected</c:if>>20</option>
                                <option value="30" <c:if test="${rows == 30}">selected</c:if>>30</option>
                                <option value="40" <c:if test="${rows == 40}">selected</c:if>>40</option>
                                <option value="50" <c:if test="${rows == 50}">selected</c:if>>50</option>
                            </select>
                            <p>行数据</p>
                        </li>
                        <c:if test="${pb.currPage == 1}">
                            <li class="disabled">
                                <a href="<%=path%>/admin/adminStudentsUrl?currentPage=1&rows=${rows}">
                                    <span aria-hidden="true">&larr;</span> 上一页
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${pb.currPage != 1}">
                            <li>
                                <a href="<%=path%>/admin/adminStudentsUrl?currentPage=${pb.currPage - 1}&rows=${rows}">
                                    <span aria-hidden="true">&larr;</span> 上一页
                                </a>
                            </li>
                        </c:if>

                        <c:forEach begin="1" end="${pb.totalPage}" var="i">
                            <c:if test="${pb.currPage == i}">
                                <li class="active">
                                    <a href="<%=path%>/admin/adminStudentsUrl?currentPage=${i}&rows=${rows}">${i}</a>
                                </li>
                            </c:if>
                            <c:if test="${pb.currPage != i}">
                                <li>
                                    <a href="<%=path%>/admin/adminStudentsUrl?currentPage=${i}&rows=${rows}">${i}</a>
                                </li>
                            </c:if>
                        </c:forEach>

                        <c:if test="${pb.currPage + 1 > pb.totalPage}">
                            <li class="disabled">
                                <a href="<%=path%>/admin/adminStudentsUrl?currentPage=${pb.currPage}&rows=${rows}">下一页
                                    <span aria-hidden="true">&rarr;</span>
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${pb.currPage + 1 <= pb.totalPage}">
                            <li>
                                <a href="<%=path%>/admin/adminStudentsUrl?currentPage=${pb.currPage + 1}&rows=${rows}">下一页
                                    <span aria-hidden="true">&rarr;</span>
                                </a>
                            </li>
                        </c:if>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- add student -->
<div class="modal fade" id="addStudents" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/addStudents">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="add-myModalLabel">录入学生信息</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="student-no" class="control-label">学号:</label>
                        <input type="text" class="form-control" name="studentNo" id="student-no">
                    </div>
                    <div class="form-group">
                        <label for="student-name" class="control-label">姓名:</label>
                        <input type="text" class="form-control" name="studentName" id="student-name">
                    </div>
                    <div class="form-group">
                        <label for="grade-info" class="control-label">年级:</label>
                        <select class="form-control" name="gradeNo" id="grade-info">
                            <c:forEach var="grade" items="${grades }">
                                <option value="${grade.gradeNo }">${grade.gradeName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="college-info" class="control-label">学院:</label>
                        <select class="form-control" name="collegeNo" id="college-info">
                            <c:forEach var="college" items="${colleges }">
                                <option value="${college.collegeNo }">${college.collegeName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="profession-info" class="control-label">专业:</label>
                        <select class="form-control" name="professionNo" id="profession-info">
                            <c:forEach var="profession" items="${professions }">
                                <option value="${profession.professionNo }">${profession.professionName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="class-info" class="control-label">班级:</label>
                        <select class="form-control" name="classNo" id="class-info">
                            <c:forEach var="Class" items="${classes }">
                                <option value="${Class.classNo }">${Class.className }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="id-card" class="control-label">身份证号:</label>
                        <input type="text" class="form-control" name="idCard" id="id-card">
                    </div>
                    <div class="form-group">
                        <label for="gender" class="control-label">性别:</label>
                        <select class="form-control" name="gender" id="gender">
                            <option value="1">男</option>
                            <option value="2">女</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="age" class="control-label">年龄:</label>
                        <input type="text" class="form-control" name="age" id="age">
                    </div>
                    <div class="form-group">
                        <label for="year" class="control-label">入学年份:</label>
                        <input type="text" class="form-control" name="year" id="year">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">提交</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- select student -->
<div class="modal fade" id="selectStudents" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/adminStudentsUrl">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="select-myModalLabel">查询学生信息</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="select-student-no" class="control-label">学号:</label>
                        <input type="text" class="form-control" name="studentNo" id="select-student-no">
                    </div>
                    <div class="form-group">
                        <label for="select-student-name" class="control-label">姓名:</label>
                        <input type="text" class="form-control" name="studentName" id="select-student-name">
                    </div>
                    <div class="form-group">
                        <label for="select-grade" class="control-label">年级:</label>
                        <select class="form-control" name="gradeNo" id="select-grade">
                            <c:forEach var="grade" items="${grades }">
                                <option value="${grade.gradeNo }">${grade.gradeName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="select-college" class="control-label">学院:</label>
                        <select class="form-control" name="collegeNo" id="select-college">
                            <c:forEach var="college" items="${colleges }">
                                <option value="${college.collegeNo }">${college.collegeName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="select-profession" class="control-label">专业:</label>
                        <select class="form-control" name="professionNo" id="select-profession">
                            <c:forEach var="profession" items="${professions }">
                                <option value="${profession.professionNo }">${profession.professionName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="select-class" class="control-label">班级:</label>
                        <select class="form-control" name="classNo" id="select-class">
                            <c:forEach var="Class" items="${classes }">
                                <option value="${Class.classNo }">${Class.className }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="select-id-card" class="control-label">身份证号:</label>
                        <input type="text" class="form-control" name="idCard" id="select-id-card">
                    </div>
                    <div class="form-group">
                        <label for="select-gender" class="control-label">性别:</label>
                        <select class="form-control" name="gender" id="select-gender">
                            <option value="1">男</option>
                            <option value="2">女</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="select-student-des" class="control-label">个性签名:</label>
                        <input type="text" class="form-control" name="strdentDes" id="select-student-des">
                    </div>
                    <div class="form-group">
                        <label for="select-age" class="control-label">年龄:</label>
                        <input type="text" class="form-control" name="age" id="select-age">
                    </div>
                    <div class="form-group">
                        <label for="select-year" class="control-label">入学年份:</label>
                        <input type="text" class="form-control" name="year" id="select-year">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">确定</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- update student -->
<div class="modal fade" id="updateStudents" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/updateStudents">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="update-myModalLabel">更新学生信息</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="update-student-no" class="control-label">学号:</label>
                        <input type="text" readonly class="form-control" name="studentNo" id="update-student-no">
                    </div>
                    <div class="form-group">
                        <label for="update-student-name" class="control-label">姓名:</label>
                        <input type="text" class="form-control" name="studentName" id="update-student-name">
                    </div>
                    <div class="form-group">
                        <label for="update-grade" class="control-label">年级:</label>
                        <select class="form-control" name="gradeNo" id="update-grade">
                            <c:forEach var="grade" items="${grades }">
                                <option value="${grade.gradeNo }">${grade.gradeName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="update-college" class="control-label">学院:</label>
                        <select class="form-control" name="collegeNo" id="update-college">
                            <c:forEach var="college" items="${colleges }">
                                <option value="${college.collegeNo }">${college.collegeName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="update-profession" class="control-label">专业:</label>
                        <select class="form-control" name="professionNo" id="update-profession">
                            <c:forEach var="profession" items="${professions }">
                                <option value="${profession.professionNo }">${profession.professionName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="update-class" class="control-label">班级:</label>
                        <select class="form-control" name="classNo" id="update-class">
                            <c:forEach var="Class" items="${classes }">
                                <option value="${Class.classNo }">${Class.className }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="update-id-card" class="control-label">身份证号:</label>
                        <input type="text" class="form-control" name="idCard" id="update-id-card">
                    </div>
                    <div class="form-group">
                        <label for="update-gender" class="control-label">性别:</label>
                        <select class="form-control" name="gender" id="update-gender">
                            <option value="1">男</option>
                            <option value="2">女</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="update-age" class="control-label">年龄:</label>
                        <input type="text" class="form-control" name="age" id="update-age">
                    </div>
                    <div class="form-group">
                        <label for="update-year" class="control-label">入学年份:</label>
                        <input type="text" class="form-control" name="year" id="update-year">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">提交</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- delete student -->
<div class="modal fade" id="deleteStudents" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/deleteStudents">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="delete-myModalLabel">删除学生信息</h4>
                </div>
                <div class="modal-body">
                    确认要删除该同学的所有信息吗（该操作不可逆）？
                    <div class="form-group hidden">
                        <label for="delete-student-no" class="control-label">学号:</label>
                        <input type="text" class="form-control" name="studentNo" id="delete-student-no">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-danger">删除</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="../js/jquery-3.5.1.js"></script>
<script src="../js/bootstrap.js"></script>

<script>
    $('#selectStudents').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var studentNo = $("#search").val();
        var modal = $(this)
        var params = {
            "studentNo": studentNo
        }
        $.ajax({
            url: '<%=path%>/getStudents',
            type: "get",
            data: params,
            success: function (result) {
                modal.find('#select-student-no').val(result.studentNo)
                modal.find('#select-student-name').val(result.studentName)
                modal.find('#select-grade').val(result.gradeNo)
                modal.find('#select-college').val(result.collegeNo)
                modal.find('#select-profession').val(result.professionNo)
                modal.find('#select-class').val(result.classNo)
                modal.find('#select-id-card').val(result.idCard)
                modal.find('#select-gender').val(result.gender)
                modal.find('#select-student-des').val(result.description)
                modal.find('#select-age').val(result.age)
                modal.find('#select-year').val(result.year)
            }
        })
    })
    $('#updateStudents').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var studentNo = button.data('student-no')
        var modal = $(this)
        var params = {
            "studentNo": studentNo
        }
        $.ajax({
            url: '<%=path%>/getStudents',
            type: "get",
            data: params,
            success: function (result) {
                modal.find('#update-student-no').val(result.studentNo)
                modal.find('#update-student-name').val(result.studentName)
                modal.find('#update-grade').val(result.gradeNo)
                modal.find('#update-college').val(result.collegeNo)
                modal.find('#update-profession').val(result.professionNo)
                modal.find('#update-class').val(result.classNo)
                modal.find('#update-id-card').val(result.idCard)
                modal.find('#update-gender').val(result.gender)
                modal.find('#update-age').val(result.age)
                modal.find('#update-year').val(result.year)
            }
        })
    })

    $('#deleteStudents').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var studentNo = button.data('student-no')
        var modal = $(this)
        modal.find('#delete-student-no').val(studentNo)
    })
</script>
</body>

</html>