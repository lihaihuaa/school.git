<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>学院管理</title>
    <%@ include file="/WEB-INF/pages/css_template.jsp" %>
</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                    aria-controls="navbar">
                <span class="sr-only">学生管理系统</span> <span class="icon-bar"></span>
                <span class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar2" aria-expanded="false"
                    aria-controls="navbar" style="float: left;">
                <span class="sr-only">切换导航</span> <span class="icon-bar"></span> <span
                    class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<%=path%>/mainUrl">学生管理系统</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <!-- 导航条菜单 -->
            <%@ include file="/WEB-INF/pages/header_nav_template.jsp" %>

        </div>
    </div>
</nav>

<div class="container-fluid">
    <div id="navbar2" class="row">
        <div class="col-sm-3 col-md-2 sidebar">

            <!-- 侧边栏 -->
            <jsp:include page="/WEB-INF/pages/side_nav_template.jsp">
                <jsp:param value="active" name="aCollege"/>
            </jsp:include>

        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="row">
                <div class="col-sm-7">
                    <div class="input-group">
                        <input id="search" type="text" class="form-control" placeholder="学院号">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#selectCollege">搜索</button>
                        </span>
                    </div>
                </div>
                <div class="col-sm-5">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addCollege">添加学院</button>
                </div>
            </div>


            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>学院号</th>
                        <th>学院名字</th>
                        <th>编辑</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="college" items="${colleges }" varStatus="status">
                        <tr>
                            <th>${(status.index + 1) + (pb.currPage - 1) * rows}</th>
                            <th>${college.collegeNo }</th>
                            <th>${college.collegeName }</th>
                            <th>
                                <button class="btn btn-default"
                                        data-college-no="${college.collegeNo }" data-toggle="modal"
                                        data-target="#updateCollege">编辑
                                </button>
                                <button class="btn btn-danger"
                                        data-college-no="${college.collegeNo }" data-toggle="modal"
                                        data-target="#deleteCollege">删除
                                </button>
                            </th>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>


            <div>
                <nav aria-label="...">
                    <ul class="pager">
                        <!-- <li class="previous"><a href="#"><span
                                aria-hidden="true">&larr;</span> 上一页</a></li>
                        <li class="next"><a href="#">下一页 <span
                                aria-hidden="true">&rarr;</span></a></li> -->
                        <li class="previous">
                            <p>每页显示</p>
                            <select onchange="location.href='<%=path%>/admin/adminCollegeUrl?rows=' + this.value">
                                <option value="3" <c:if test="${rows == 3}">selected</c:if>>3</option>
                                <option value="5" <c:if test="${rows == 5}">selected</c:if>>5</option>
                                <option value="10" <c:if test="${rows == 10}">selected</c:if>>10</option>
                                <option value="20" <c:if test="${rows == 20}">selected</c:if>>20</option>
                                <option value="30" <c:if test="${rows == 30}">selected</c:if>>30</option>
                                <option value="40" <c:if test="${rows == 40}">selected</c:if>>40</option>
                                <option value="50" <c:if test="${rows == 50}">selected</c:if>>50</option>
                            </select>
                            <p>行数据</p>
                        </li>
                        <c:if test="${pb.currPage == 1}">
                            <li class="disabled">
                                <a href="<%=path%>/admin/adminCollegeUrl?currentPage=1&rows=${rows}">
                                    <span aria-hidden="true">&larr;</span> 上一页
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${pb.currPage != 1}">
                            <li>
                                <a href="<%=path%>/admin/adminCollegeUrl?currentPage=${pb.currPage - 1}&rows=${rows}">
                                    <span aria-hidden="true">&larr;</span> 上一页
                                </a>
                            </li>
                        </c:if>

                        <c:forEach begin="1" end="${pb.totalPage}" var="i">
                            <c:if test="${pb.currPage == i}">
                                <li class="active">
                                    <a href="<%=path%>/admin/adminCollegeUrl?currentPage=${i}&rows=${rows}">${i}</a>
                                </li>
                            </c:if>
                            <c:if test="${pb.currPage != i}">
                                <li>
                                    <a href="<%=path%>/admin/adminCollegeUrl?currentPage=${i}&rows=${rows}">${i}</a>
                                </li>
                            </c:if>
                        </c:forEach>

                        <c:if test="${pb.currPage + 1 > pb.totalPage}">
                            <li class="disabled">
                                <a href="<%=path%>/admin/adminCollegeUrl?currentPage=${pb.currPage}&rows=${rows}">下一页
                                    <span aria-hidden="true">&rarr;</span>
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${pb.currPage + 1 <= pb.totalPage}">
                            <li>
                                <a href="<%=path%>/admin/adminCollegeUrl?currentPage=${pb.currPage + 1}&rows=${rows}">下一页
                                    <span aria-hidden="true">&rarr;</span>
                                </a>
                            </li>
                        </c:if>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>

<!-- add college -->
<div class="modal fade" id="addCollege" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/addCollege">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="add-myModalLabel">录入学院信息</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="college-no" class="control-label">学院号:</label>
                        <input type="text" class="form-control" name="collegeNo" id="college-no">
                    </div>
                    <div class="form-group">
                        <label for="college-name" class="control-label">学院名字:</label>
                        <input type="text" class="form-control" name="collegeName" id="college-name">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary">提交</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- select college -->
<div class="modal fade" id="selectCollege" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/adminCollegeUrl">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="select-myModalLabel">查找学院信息</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="select-college-no" class="control-label">学院号:</label>
                        <input type="text" class="form-control" name="collegeNo" id="select-college-no">
                    </div>

                    <div class="form-group">
                        <label for="select-college-name" class="control-label">学院名字:</label>
                        <input type="text" class="form-control" name="collegeName" id="select-college-name">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary">确定</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- update college -->
<div class="modal fade" id="updateCollege" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/updateCollege">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="update-myModalLabel">更新学院信息</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="update-college-no" class="control-label">学院号:</label>
                        <input type="text" readonly class="form-control" name="collegeNo" id="update-college-no">
                    </div>

                    <div class="form-group">
                        <label for="update-college-name" class="control-label">学院名字:</label>
                        <input type="text" class="form-control" name="collegeName" id="update-college-name">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary">保存</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- delete College -->
<div class="modal fade" id="deleteCollege" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/deleteCollege">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="delete-myModalLabel">删除学院信息</h4>
                </div>
                <div class="modal-body">
                    确认要删除该学院的所有信息吗（该操作不可逆）？
                    <div class="form-group hidden">
                        <label for="delete-college-no" class="control-label">学院号:</label>
                        <input type="text" class="form-control" name="collegeNo" id="delete-college-no">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-danger">删除</button>
                </div>
            </form>
        </div>
    </div>
</div>


<script src="../js/jquery-3.5.1.js"></script>
<script src="../js/bootstrap.js"></script>

<script>
    $('#selectCollege').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var collegeNo = $("#search").val();
        var modal = $(this)
        var params = {
            "collegeNo": collegeNo
        }
        $.ajax({
            url: '<%=path%>/getCollege',
            type: "get",
            data: params,
            success: function (result) {
                modal.find('#select-college-no').val(result.collegeNo)
                modal.find('#select-college-name').val(result.collegeName)
            }
        })
    })
    $('#updateCollege').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var collegeNo = button.data('college-no')
        var modal = $(this)
        var params = {
            "collegeNo": collegeNo
        }
        $.ajax({
            url: '<%=path%>/getCollege',
            type: "get",
            data: params,
            success: function (result) {
                modal.find('#update-college-no').val(result.collegeNo)
                modal.find('#update-college-name').val(result.collegeName)
            }
        })
    })

    $('#deleteCollege').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var collegeNo = button.data('college-no')
        var modal = $(this)
        modal.find('#delete-college-no').val(collegeNo)
    })
</script>
</body>

</html>