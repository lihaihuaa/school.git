<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>专业管理</title>
    <%@ include file="/WEB-INF/pages/css_template.jsp" %>
</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                    aria-controls="navbar">
                <span class="sr-only">学生管理系统</span> <span class="icon-bar"></span>
                <span class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar2" aria-expanded="false"
                    aria-controls="navbar" style="float: left;">
                <span class="sr-only">切换导航</span> <span class="icon-bar"></span> <span
                    class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<%=path%>/mainUrl">学生管理系统</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <!-- 导航条菜单 -->
            <%@ include file="/WEB-INF/pages/header_nav_template.jsp" %>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div id="navbar2" class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <!-- 侧边栏 -->
            <jsp:include page="/WEB-INF/pages/side_nav_template.jsp">
                <jsp:param value="active" name="aProfession"/>
            </jsp:include>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="row">
                <div class="col-sm-7">
                    <div class="input-group">
                        <input id="search" type="text" class="form-control" placeholder="专业号">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#selectProfession">搜索</button>
                        </span>
                    </div>
                </div>
                <div class="col-sm-5">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addProfession">添加专业
                    </button>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>专业号</th>
                        <th>专业名字</th>
                        <th>学院号</th>
                        <th>学院名字</th>
                        <th>编辑</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="profession" items="${professions }" varStatus="status">
                        <tr>
                            <th>${(status.index + 1) + (pb.currPage - 1) * rows}</th>
                            <th>${profession.professionNo }</th>
                            <th>${profession.professionName }</th>
                            <th>${profession.collegeNo }</th>
                            <th>
                                <c:forEach var="college" items="${colleges }">
                                    <c:if test="${college.collegeNo == profession.collegeNo}">${college.collegeName}</c:if>
                                </c:forEach>
                            </th>
                            <th>
                                <button class="btn btn-default"
                                        data-profession-no="${profession.professionNo }" data-toggle="modal"
                                        data-target="#updateProfession">编辑
                                </button>
                                <button class="btn btn-danger"
                                        data-profession-no="${profession.professionNo }" data-toggle="modal"
                                        data-target="#deleteProfession">删除
                                </button>
                            </th>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>

            <div>
                <nav aria-label="...">
                    <ul class="pager">
                        <!-- <li class="previous"><a href="#"><span
                                aria-hidden="true">&larr;</span> 上一页</a></li>
                        <li class="next"><a href="#">下一页 <span
                                aria-hidden="true">&rarr;</span></a></li> -->
                        <li class="previous">
                            <p>每页显示</p>
                            <select onchange="location.href='<%=path%>/admin/adminProfessionUrl?rows=' + this.value">
                                <option value="3" <c:if test="${rows == 3}">selected</c:if>>3</option>
                                <option value="5" <c:if test="${rows == 5}">selected</c:if>>5</option>
                                <option value="10" <c:if test="${rows == 10}">selected</c:if>>10</option>
                                <option value="20" <c:if test="${rows == 20}">selected</c:if>>20</option>
                                <option value="30" <c:if test="${rows == 30}">selected</c:if>>30</option>
                                <option value="40" <c:if test="${rows == 40}">selected</c:if>>40</option>
                                <option value="50" <c:if test="${rows == 50}">selected</c:if>>50</option>
                            </select>
                            <p>行数据</p>
                        </li>
                        <c:if test="${pb.currPage == 1}">
                            <li class="disabled">
                                <a href="<%=path%>/admin/adminProfessionUrl?currentPage=1&rows=${rows}">
                                    <span aria-hidden="true">&larr;</span> 上一页
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${pb.currPage != 1}">
                            <li>
                                <a href="<%=path%>/admin/adminProfessionUrl?currentPage=${pb.currPage - 1}&rows=${rows}">
                                    <span aria-hidden="true">&larr;</span> 上一页
                                </a>
                            </li>
                        </c:if>
                        <c:forEach begin="1" end="${pb.totalPage}" var="i">
                            <c:if test="${pb.currPage == i}">
                                <li class="active">
                                    <a href="<%=path%>/admin/adminProfessionUrl?currentPage=${i}&rows=${rows}">${i}</a>
                                </li>
                            </c:if>
                            <c:if test="${pb.currPage != i}">
                                <li>
                                    <a href="<%=path%>/admin/adminProfessionUrl?currentPage=${i}&rows=${rows}">${i}</a>
                                </li>
                            </c:if>
                        </c:forEach>
                        <c:if test="${pb.currPage + 1 > pb.totalPage}">
                            <li class="disabled">
                                <a href="<%=path%>/admin/adminProfessionUrl?currentPage=${pb.currPage}&rows=${rows}">下一页
                                    <span aria-hidden="true">&rarr;</span>
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${pb.currPage + 1 <= pb.totalPage}">
                            <li>
                                <a href="<%=path%>/admin/adminProfessionUrl?currentPage=${pb.currPage + 1}&rows=${rows}">下一页
                                    <span aria-hidden="true">&rarr;</span>
                                </a>
                            </li>
                        </c:if>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- add profession -->
<div class="modal fade" id="addProfession" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/addProfession">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="add-myModalLabel">录入专业信息</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="profession-no" class="control-label">专业号:</label>
                        <input type="text" class="form-control" name="professionNo" id="profession-no">
                    </div>
                    <div class="form-group">
                        <label for="profession-name" class="control-label">专业名字:</label>
                        <input type="text" class="form-control" name="professionName" id="profession-name">
                    </div>
                    <div class="form-group">
                        <label for="college-info" class="control-label">学院:</label>
                        <select class="form-control" name="collegeNo" id="college-info">
                            <c:forEach var="college" items="${colleges }">
                                <option value="${college.collegeNo }">${college.collegeName }</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary">提交</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- select profession -->
<div class="modal fade" id="selectProfession" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/adminProfessionUrl">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="select-myModalLabel">查找专业信息</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="select-profession-no" class="control-label">专业号:</label>
                        <input type="text" class="form-control" name="professionNo" id="select-profession-no">
                    </div>
                    <div class="form-group">
                        <label for="select-profession-name" class="control-label">专业名字:</label>
                        <input type="text" class="form-control" name="professionName" id="select-profession-name">
                    </div>
                    <div class="form-group">
                        <label for="select-college" class="control-label">学院:</label>
                        <select class="form-control" name="collegeNo" id="select-college">
                            <c:forEach var="college" items="${colleges }">
                                <option value="${college.collegeNo }">${college.collegeName }</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary">确定</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- update profession -->
<div class="modal fade" id="updateProfession" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/updateProfession">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="update-myModalLabel">更新专业信息</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="update-profession-no" class="control-label">专业号:</label>
                        <input type="text" readonly class="form-control" name="professionNo" id="update-profession-no">
                    </div>
                    <div class="form-group">
                        <label for="update-profession-name" class="control-label">专业名字:</label>
                        <input type="text" class="form-control" name="professionName" id="update-profession-name">
                    </div>
                    <div class="form-group">
                        <label for="update-college" class="control-label">学院:</label>
                        <select class="form-control" name="collegeNo" id="update-college">
                            <c:forEach var="college" items="${colleges }">
                                <option value="${college.collegeNo }">${college.collegeName }</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary">保存</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- delete Profession -->
<div class="modal fade" id="deleteProfession" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/deleteProfession">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="delete-myModalLabel">删除专业信息</h4>
                </div>
                <div class="modal-body">
                    确认要删除该专业的所有信息吗（该操作不可逆）？
                    <div class="form-group hidden">
                        <label for="delete-profession-no" class="control-label">专业号:</label>
                        <input type="text" class="form-control" name="professionNo" id="delete-profession-no">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-danger">删除</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="../js/jquery-3.5.1.js"></script>
<script src="../js/bootstrap.js"></script>

<script>
    $('#selectProfession').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var professionNo = $("#search").val();
        var modal = $(this)
        var params = {
            "professionNo": professionNo
        }
        $.ajax({
            url: '<%=path%>/getProfession',
            type: "get",
            data: params,
            success: function (result) {
                modal.find('#select-profession-no').val(result.professionNo)
                modal.find('#select-profession-name').val(result.professionName)
                modal.find('#select-college').val(result.collegeNo)
            }
        })
    })
    $('#updateProfession').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var professionNo = button.data('profession-no')
        var modal = $(this)
        var params = {
            "professionNo": professionNo
        }
        $.ajax({
            url: '<%=path%>/getProfession',
            type: "get",
            data: params,
            success: function (result) {
                modal.find('#update-profession-no').val(result.professionNo)
                modal.find('#update-profession-name').val(result.professionName)
                modal.find('#update-college').val(result.collegeNo)
            }
        })
    })
    $('#deleteProfession').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var professionNo = button.data('profession-no')
        var modal = $(this)
        modal.find('#delete-profession-no').val(professionNo)
    })
</script>
</body>
</html>