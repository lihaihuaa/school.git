<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>课程管理</title>
    <%@ include file="/WEB-INF/pages/css_template.jsp" %>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                    aria-controls="navbar">
                <span class="sr-only">学生管理系统</span> <span class="icon-bar"></span>
                <span class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar2" aria-expanded="false"
                    aria-controls="navbar" style="float: left;">
                <span class="sr-only">切换导航</span> <span class="icon-bar"></span> <span
                    class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<%=path%>/mainUrl">学生管理系统</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <!-- 导航条菜单 -->
            <%@ include file="/WEB-INF/pages/header_nav_template.jsp" %>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div id="navbar2" class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <!-- 侧边栏 -->
            <jsp:include page="/WEB-INF/pages/side_nav_template.jsp">
                <jsp:param value="active" name="aCourse"/>
            </jsp:include>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="row">
                <div class="col-sm-7">
                    <div class="input-group">
                        <input id="search" type="text" class="form-control" placeholder="课程号">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#selectCourse">搜索</button>
                        </span>
                    </div>
                </div>
                <div class="col-sm-5">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#addCourse">添加课程
                    </button>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>课程号</th>
                        <th>课程名字</th>
                        <th>课程类型</th>
                        <th>课程学分</th>
                        <th>上限人数</th>
                        <th>年级名字</th>
                        <th>专业名字</th>
                        <th>教师名字</th>
                        <th>编辑</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="course" items="${courses }" varStatus="status">
                        <tr>
                            <th>${(status.index + 1) + (pb.currPage - 1) * rows}</th>
                            <th>${course.courseNo }</th>
                            <th>${course.courseName }</th>
                            <th>
                                <c:choose>
                                    <c:when test="${course.courseType == '1'}">
                                        必修课
                                    </c:when>
                                    <c:when test="${course.courseType == '2'}">
                                        选修课
                                    </c:when>
                                    <c:otherwise>
                                        实践课
                                    </c:otherwise>
                                </c:choose>
                            </th>
                            <th>${course.courseCredit }</th>
                            <th>
                                <c:choose>
                                    <c:when test="${course.courseNum == 0}">
                                        无上限
                                    </c:when>
                                    <c:otherwise>
                                        ${course.courseNum }
                                    </c:otherwise>
                                </c:choose>
                            </th>
                            <th>
                                <c:forEach var="grade" items="${grades }">
                                    <c:if test="${grade.gradeNo == course.gradeNo}">${grade.gradeName}</c:if>
                                </c:forEach>
                            </th>
                            <th>
                                <c:forEach var="profession" items="${professions }">
                                    <c:if test="${profession.professionNo == course.professionNo}">${profession.professionName}</c:if>
                                </c:forEach>
                            </th>
                            <th>
                                <c:forEach var="teacher" items="${teachers }">
                                    <c:if test="${teacher.teacherNo == course.teacherNo}">${teacher.teacherName}</c:if>
                                </c:forEach>
                            </th>
                            <th>
                                <button class="btn btn-default"
                                        data-course-no="${course.courseNo }" data-toggle="modal"
                                        data-target="#updateCourse">编辑
                                </button>
                                <button class="btn btn-danger"
                                        data-course-no="${course.courseNo }" data-toggle="modal"
                                        data-target="#deleteCourse">删除
                                </button>
                            </th>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>

            <div>
                <nav aria-label="...">
                    <ul class="pager">
                        <!-- <li class="previous"><a href="#"><span
                                aria-hidden="true">&larr;</span> 上一页</a></li>
                        <li class="next"><a href="#">下一页 <span
                                aria-hidden="true">&rarr;</span></a></li> -->
                        <li class="previous">
                            <p>每页显示</p>
                            <select onchange="location.href='<%=path%>/admin/adminProfessionCourseUrl?rows=' + this.value">
                                <option value="3" <c:if test="${rows == 3}">selected</c:if>>3</option>
                                <option value="5" <c:if test="${rows == 5}">selected</c:if>>5</option>
                                <option value="10" <c:if test="${rows == 10}">selected</c:if>>10</option>
                                <option value="20" <c:if test="${rows == 20}">selected</c:if>>20</option>
                                <option value="30" <c:if test="${rows == 30}">selected</c:if>>30</option>
                                <option value="40" <c:if test="${rows == 40}">selected</c:if>>40</option>
                                <option value="50" <c:if test="${rows == 50}">selected</c:if>>50</option>
                            </select>
                            <p>行数据</p>
                        </li>
                        <c:if test="${pb.currPage == 1}">
                            <li class="disabled">
                                <a href="<%=path%>/admin/adminProfessionCourseUrl?currentPage=1&rows=${rows}">
                                    <span aria-hidden="true">&larr;</span> 上一页
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${pb.currPage != 1}">
                            <li>
                                <a href="<%=path%>/admin/adminProfessionCourseUrl?currentPage=${pb.currPage - 1}&rows=${rows}">
                                    <span aria-hidden="true">&larr;</span> 上一页
                                </a>
                            </li>
                        </c:if>
                        <c:forEach begin="1" end="${pb.totalPage}" var="i">
                            <c:if test="${pb.currPage == i}">
                                <li class="active">
                                    <a href="<%=path%>/admin/adminProfessionCourseUrl?currentPage=${i}&rows=${rows}">${i}</a>
                                </li>
                            </c:if>
                            <c:if test="${pb.currPage != i}">
                                <li>
                                    <a href="<%=path%>/admin/adminProfessionCourseUrl?currentPage=${i}&rows=${rows}">${i}</a>
                                </li>
                            </c:if>
                        </c:forEach>
                        <c:if test="${pb.currPage + 1 > pb.totalPage}">
                            <li class="disabled">
                                <a href="<%=path%>/admin/adminProfessionCourseUrl?currentPage=${pb.currPage}&rows=${rows}">下一页
                                    <span aria-hidden="true">&rarr;</span>
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${pb.currPage + 1 <= pb.totalPage}">
                            <li>
                                <a href="<%=path%>/admin/adminProfessionCourseUrl?currentPage=${pb.currPage + 1}&rows=${rows}">下一页
                                    <span aria-hidden="true">&rarr;</span>
                                </a>
                            </li>
                        </c:if>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- add course -->
<div class="modal fade" id="addCourse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/addProfessionCourse">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="add-myModalLabel">录入课程信息</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="course-no" class="control-label">课程号:</label>
                        <input type="text" class="form-control" name="courseNo" id="course-no">
                    </div>
                    <div class="form-group">
                        <label for="class-name" class="control-label">课程名字:</label>
                        <input type="text" class="form-control" name="courseName" id="class-name">
                    </div>
                    <div class="form-group">
                        <label for="class-type" class="control-label">课程类型:</label>
                        <input type="text" class="form-control" name="courseType" id="class-type">
                    </div>
                    <div class="form-group">
                        <label for="class-credit" class="control-label">课程学分:</label>
                        <input type="text" class="form-control" name="courseCredit" id="class-credit">
                    </div>
                    <div class="form-group">
                        <label for="class-num" class="control-label">上限人数:</label>
                        <input type="text" class="form-control" name="courseNum" id="class-num">
                    </div>
                    <div class="form-group">
                        <label for="grade-info" class="control-label">年级:</label>
                        <select class="form-control" name="gradeNo" id="grade-info">
                            <c:forEach var="grade" items="${grades }">
                                <option value="${grade.gradeNo }">${grade.gradeName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="profession-info" class="control-label">专业:</label>
                        <select class="form-control" name="professionNo" id="profession-info">
                            <c:forEach var="profession" items="${professions }">
                                <option value="${profession.professionNo }">${profession.professionName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="teacher-info" class="control-label">教师:</label>
                        <select class="form-control" name="teacherNo" id="teacher-info">
                            <c:forEach var="teacher" items="${teachers }">
                                <option value="${teacher.teacherNo }">${teacher.teacherName }</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary">提交</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- select course -->
<div class="modal fade" id="selectCourse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/adminProfessionCourseUrl">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="select-myModalLabel">查找课程信息</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="select-course-no" class="control-label">课程号:</label>
                        <input type="text" class="form-control" name="courseNo" id="select-course-no">
                    </div>
                    <div class="form-group">
                        <label for="select-course-name" class="control-label">课程名字:</label>
                        <input type="text" class="form-control" name="courseName" id="select-course-name">
                    </div>
                    <div class="form-group">
                        <label for="select-course-type" class="control-label">课程类型:</label>
                        <input type="text" class="form-control" name="courseType" id="select-course-type">
                    </div>
                    <div class="form-group">
                        <label for="select-course-credit" class="control-label">课程学分:</label>
                        <input type="text" class="form-control" name="courseCredit" id="select-course-credit">
                    </div>
                    <div class="form-group">
                        <label for="select-course-num" class="control-label">上限人数:</label>
                        <input type="text" class="form-control" name="courseNum" id="select-course-num">
                    </div>
                    <div class="form-group">
                        <label for="select-grade" class="control-label">年级:</label>
                        <select class="form-control" name="gradeNo" id="select-grade">
                            <c:forEach var="grade" items="${grades }">
                                <option value="${grade.gradeNo }">${grade.gradeName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="select-profession" class="control-label">专业:</label>
                        <select class="form-control" name="professionNo" id="select-profession">
                            <c:forEach var="profession" items="${professions }">
                                <option value="${profession.professionNo }">${profession.professionName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="select-teacher" class="control-label">教师:</label>
                        <select class="form-control" name="teacherNo" id="select-teacher">
                            <c:forEach var="teacher" items="${teachers }">
                                <option value="${teacher.teacherNo }">${teacher.teacherName }</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary">确定</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- update course -->
<div class="modal fade" id="updateCourse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/updateCourse">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="update-myModalLabel">更新课程信息</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="update-course-no" class="control-label">课程号:</label>
                        <input type="text" readonly class="form-control" name="courseNo" id="update-course-no">
                    </div>
                    <div class="form-group">
                        <label for="update-course-name" class="control-label">课程名字:</label>
                        <input type="text" class="form-control" name="courseName" id="update-course-name">
                    </div>
                    <div class="form-group">
                        <label for="update-course-type" class="control-label">课程类型(不建议修改):</label>
                        <input type="text" class="form-control" name="courseType" id="update-course-type">
                    </div>
                    <div class="form-group">
                        <label for="update-course-credit" class="control-label">课程学分:</label>
                        <input type="text" class="form-control" name="courseCredit" id="update-course-credit">
                    </div>
                    <div class="form-group">
                        <label for="update-course-num" class="control-label">上限人数(不建议修改):</label>
                        <input type="text" class="form-control" name="courseNum" id="update-course-num">
                    </div>
                    <div class="form-group">
                        <label for="update-grade" class="control-label">年级:</label>
                        <select class="form-control" name="gradeNo" id="update-grade">
                            <c:forEach var="grade" items="${grades }">
                                <option value="${grade.gradeNo }">${grade.gradeName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="update-profession" class="control-label">专业:</label>
                        <select class="form-control" name="professionNo" id="update-profession">
                            <c:forEach var="profession" items="${professions }">
                                <option value="${profession.professionNo }">${profession.professionName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="update-teacher" class="control-label">教师:</label>
                        <select class="form-control" name="teacherNo" id="update-teacher">
                            <c:forEach var="teacher" items="${teachers }">
                                <option value="${teacher.teacherNo }">${teacher.teacherName }</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary">保存</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- delete course -->
<div class="modal fade" id="deleteCourse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/deleteCourse">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="delete-myModalLabel">删除课程信息</h4>
                </div>
                <div class="modal-body">
                    确认要删除该课程的所有信息吗（该操作不可逆）？
                    <div class="form-group hidden">
                        <label for="delete-course-no" class="control-label">课程号:</label>
                        <input type="text" class="form-control" name="courseNo" id="delete-course-no">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-danger">删除</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="../js/jquery-3.5.1.js"></script>
<script src="../js/bootstrap.js"></script>

<script>
    $('#selectCourse').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var courseNo = $("#search").val();
        var modal = $(this)
        var params = {
            "courseNo": courseNo
        }
        $.ajax({
            url: '<%=path%>/getProfessionCourse',
            type: "get",
            data: params,
            success: function (result) {
                modal.find('#select-course-no').val(result.courseNo)
                modal.find('#select-course-name').val(result.courseName)
                modal.find('#select-course-type').val(result.courseType)
                modal.find('#select-course-credit').val(result.courseCredit)
                modal.find('#select-course-num').val(result.courseNum)
                modal.find('#select-geade').val(result.gradeNo)
                modal.find('#select-profession').val(result.professionNo)
                modal.find('#select-teacher').val(result.teacherNo)
            }
        })
    })
    $('#updateCourse').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var courseNo = button.data('course-no')
        var modal = $(this)
        var params = {
            "courseNo": courseNo
        }
        $.ajax({
            url: '<%=path%>/getProfessionCourse',
            type: "get",
            data: params,
            success: function (result) {
                modal.find('#update-course-no').val(result.courseNo)
                modal.find('#update-course-name').val(result.courseName)
                modal.find('#update-course-type').val(result.courseType)
                modal.find('#update-course-credit').val(result.courseCredit)
                modal.find('#update-course-num').val(result.courseNum)
                modal.find('#update-grade').val(result.gradeNo)
                modal.find('#update-profession').val(result.professionNo)
                modal.find('#update-teacher').val(result.teacherNo)
            }
        })
    })
    $('#deleteCourse').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var courseNo = button.data('course-no')
        var modal = $(this)
        modal.find('#delete-course-no').val(courseNo)
    })
</script>
</body>
</html>