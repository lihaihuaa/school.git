<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>选课管理</title>
    <%@ include file="/WEB-INF/pages/css_template.jsp" %>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                    aria-controls="navbar">
                <span class="sr-only">学生管理系统</span> <span class="icon-bar"></span>
                <span class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar2" aria-expanded="false"
                    aria-controls="navbar" style="float: left;">
                <span class="sr-only">切换导航</span> <span class="icon-bar"></span> <span
                    class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<%=path%>/mainUrl">学生管理系统</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <!-- 导航条菜单 -->
            <%@ include file="/WEB-INF/pages/header_nav_template.jsp" %>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div id="navbar2" class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <!-- 侧边栏 -->
            <jsp:include page="/WEB-INF/pages/side_nav_template.jsp">
                <jsp:param value="active" name="aElective"/>
            </jsp:include>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="row">
                <div class="col-sm-7">
                    <div class="input-group">
                        <input id="search1" type="text" class="form-control" placeholder="课程号">
                        <input id="search2" type="text" class="form-control" placeholder="学生学号">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#selectElective">搜索</button>
                        </span>
                    </div>
                </div>
                <div class="col-sm-5">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addElectiveStudent">添加课程
                    </button>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>专业号</th>
                        <th>专业名字</th>
                        <th>课程号</th>
                        <th>课程名字</th>
                        <th>学生学号</th>
                        <th>学生名字</th>
                        <th>编辑</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="elective" items="${electives }" varStatus="status">
                        <tr>
                            <th>${(status.index + 1) + (pb.currPage - 1) * rows}</th>
                            <th>${elective.professionNo }</th>
                            <th>
                                <c:forEach var="profession" items="${professions }">
                                    <c:if test="${profession.professionNo == elective.professionNo}">${profession.professionName}</c:if>
                                </c:forEach>
                            </th>
                            <th>${elective.courseNo }</th>
                            <th>
                                <c:forEach var="course" items="${courses }">
                                    <c:if test="${course.courseNo == elective.courseNo}">${course.courseName}</c:if>
                                </c:forEach>
                            </th>
                            <th>${elective.studentNo }</th>
                            <th>
                                <c:forEach var="student" items="${students }">
                                    <c:if test="${student.studentNo == elective.studentNo}">${student.studentName}</c:if>
                                </c:forEach>
                            </th>
                            <th>
                                <button class="btn btn-default"
                                        data-elective-id="${elective.id }" data-toggle="modal"
                                        data-target="#updateElectiveStudent">编辑
                                </button>
                                <button class="btn btn-danger"
                                        data-elective-id="${elective.id }" data-toggle="modal"
                                        data-target="#deleteElectiveStudent">删除
                                </button>
                            </th>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>

            <div>
                <nav aria-label="...">
                    <ul class="pager">
                        <!-- <li class="previous"><a href="#"><span
                                aria-hidden="true">&larr;</span> 上一页</a></li>
                        <li class="next"><a href="#">下一页 <span
                                aria-hidden="true">&rarr;</span></a></li> -->
                        <li class="previous">
                            <p>每页显示</p>
                            <select onchange="location.href='<%=path%>/admin/adminElectiveStudentUrl?rows=' + this.value">
                                <option value="3" <c:if test="${rows == 3}">selected</c:if>>3</option>
                                <option value="5" <c:if test="${rows == 5}">selected</c:if>>5</option>
                                <option value="10" <c:if test="${rows == 10}">selected</c:if>>10</option>
                                <option value="20" <c:if test="${rows == 20}">selected</c:if>>20</option>
                                <option value="30" <c:if test="${rows == 30}">selected</c:if>>30</option>
                                <option value="40" <c:if test="${rows == 40}">selected</c:if>>40</option>
                                <option value="50" <c:if test="${rows == 50}">selected</c:if>>50</option>
                            </select>
                            <p>行数据</p>
                        </li>
                        <c:if test="${pb.currPage == 1}">
                            <li class="disabled">
                                <a href="<%=path%>/admin/adminElectiveStudentUrl?currentPage=1&rows=${rows}">
                                    <span aria-hidden="true">&larr;</span> 上一页
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${pb.currPage != 1}">
                            <li>
                                <a href="<%=path%>/admin/adminElectiveStudentUrl?currentPage=${pb.currPage - 1}&rows=${rows}">
                                    <span aria-hidden="true">&larr;</span> 上一页
                                </a>
                            </li>
                        </c:if>
                        <c:forEach begin="1" end="${pb.totalPage}" var="i">
                            <c:if test="${pb.currPage == i}">
                                <li class="active">
                                    <a href="<%=path%>/admin/adminElectiveStudentUrl?currentPage=${i}&rows=${rows}">${i}</a>
                                </li>
                            </c:if>
                            <c:if test="${pb.currPage != i}">
                                <li>
                                    <a href="<%=path%>/admin/adminElectiveStudentUrl?currentPage=${i}&rows=${rows}">${i}</a>
                                </li>
                            </c:if>
                        </c:forEach>
                        <c:if test="${pb.currPage + 1 > pb.totalPage}">
                            <li class="disabled">
                                <a href="<%=path%>/admin/adminElectiveStudentUrl?currentPage=${pb.currPage}&rows=${rows}">下一页
                                    <span aria-hidden="true">&rarr;</span>
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${pb.currPage + 1 <= pb.totalPage}">
                            <li>
                                <a href="<%=path%>/admin/adminElectiveStudentUrl?currentPage=${pb.currPage + 1}&rows=${rows}">下一页
                                    <span aria-hidden="true">&rarr;</span>
                                </a>
                            </li>
                        </c:if>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- add elective -->
<div class="modal fade" id="addElectiveStudent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/addElectiveStudent">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="add-myModalLabel">录入选课信息</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="profession-info" class="control-label">专业:</label>
                        <select class="form-control" name="professionNo" id="profession-info">
                            <c:forEach var="profession" items="${professions }">
                                <option value="${profession.professionNo }">${profession.professionName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="course-info" class="control-label">课程:</label>
                        <select class="form-control" name="courseNo" id="course-info">
                            <c:forEach var="course" items="${electiveUnNum }">
                                <option value="${course.courseNo }">${course.courseName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="student-info" class="control-label">学生:</label>
                        <select class="form-control" name="studentNo" id="student-info">
                            <c:forEach var="student" items="${students }">
                                <option value="${student.studentNo }">${student.studentName }</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary">提交</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- select elective -->
<div class="modal fade" id="selectElective" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/adminElectiveStudentUrl">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="select-myModalLabel">查找选课信息</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="select-profession" class="control-label">专业:</label>
                        <select class="form-control" name="professionNo" id="select-profession">
                            <c:forEach var="profession" items="${professions }">
                                <option value="${profession.professionNo }">${profession.professionName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group" id="type1">
                        <label for="select-course" class="control-label">课程:</label>
                        <select class="form-control" name="courseNo" id="select-course">
                            <c:forEach var="course" items="${electiveUnNum }">
                                <option value="${course.courseNo }">${course.courseName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group" id="type2">
                        <label for="select-student—num" class="control-label">选课人数:</label>
                        <input type="text" class="form-control" name="studentNum" id="select-student—num">
                    </div>
                    <div class="form-group" id="type3">
                        <label for="select-elective-student" class="control-label">具体学生:</label>
                        <select class="form-control" name="studentList" id="select-elective-student"></select>
                    </div>
                    <div class="form-group" id="type4">
                        <label for="select-student" class="control-label">学生:</label>
                        <select class="form-control" name="studentNo" id="select-student">
                            <c:forEach var="student" items="${students }">
                                <option value="${student.studentNo }">${student.studentName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group" id="type5">
                        <label for="select-course—num" class="control-label">选课总数:</label>
                        <input type="text" class="form-control" name="courseNum" id="select-course—num">
                    </div>
                    <div class="form-group" id="type6">
                        <label for="select-elective-course" class="control-label">具体课程:</label>
                        <select class="form-control" name="courseList" id="select-elective-course"></select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary">确定</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- update elective -->
<div class="modal fade" id="updateElectiveStudent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/updateElectiveStudent">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="update-myModalLabel">更新选课信息</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="update-course-id" class="control-label">id:</label>
                        <input type="text" readonly class="form-control" name="id" id="update-course-id">
                    </div>
                    <div class="form-group">
                        <label for="update-profession" class="control-label">专业:</label>
                        <select class="form-control" name="professionNo" id="update-profession">
                            <c:forEach var="profession" items="${professions }">
                                <option value="${profession.professionNo }">${profession.professionName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="update-course" class="control-label">课程:</label>
                        <select class="form-control" name="courseNo" id="update-course">
                            <c:forEach var="course" items="${electiveUnNum }">
                                <option value="${course.courseNo }">${course.courseName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="update-student" class="control-label">学生:</label>
                        <select class="form-control" name="studentNo" id="update-student">
                            <c:forEach var="student" items="${students }">
                                <option value="${student.studentNo }">${student.studentName }</option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary">保存</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- delete Elective -->
<div class="modal fade" id="deleteElectiveStudent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/deleteElectiveStudent">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="delete-myModalLabel">删除选课信息</h4>
                </div>
                <div class="modal-body">
                    确认要删除该选课的所有信息吗（该操作不可逆）？
                    <div class="form-group hidden">
                        <label for="delete-elective-id" class="control-label">id:</label>
                        <input type="text" class="form-control" name="electiveNo" id="delete-elective-id">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-danger">删除</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="../js/jquery-3.5.1.js"></script>
<script src="../js/bootstrap.js"></script>

<script>
    $('#selectElective').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var courseNo = $("#search1").val();
        var studentNo = $("#search2").val();
        var modal = $(this)
        var params = {
            "courseNo": courseNo,
            "studentNo": studentNo,
            "id": null
        }
        if (courseNo != '' && courseNo != null) {
            $('#type1').removeClass("hidden");
            $('#type2').removeClass("hidden");
            $('#type3').removeClass("hidden");
        }else{
            $('#type1').addClass("hidden");
            $('#type2').addClass("hidden");
            $('#type3').addClass("hidden");
        }
        if (studentNo != '' && studentNo != null) {
            $('#type4').removeClass("hidden");
            $('#type5').removeClass("hidden");
            $('#type6').removeClass("hidden");
        }else{
            $('#type4').addClass("hidden");
            $('#type5').addClass("hidden");
            $('#type6').addClass("hidden");
        }
        $.ajax({
            url: '<%=path%>/getElectiveStudent',
            type: "get",
            data: params,
            success: function (result) {
                modal.find('#select-profession').val(result.professionNo)
                modal.find('#select-course').val(result.courseNo)
                modal.find('#select-student—num').val(result.studentNum)
                modal.find('#select-student').val(result.studentNo)
                modal.find('#select-course—num').val(result.courseNum)
                if (result && result.studentList.length > 0) {
                    var project = result.studentList;
                    var pro = $('#select-elective-student');
                    var options;
                    $(project).each(function () {
                        options += '<option value="' + this.studentNo + '" >' + this.studentName + '</option>';
                    });
                    pro.append(options);
                }
                if (result && result.courseList.length > 0) {
                    var project = result.courseList;
                    var pro = $('#select-elective-course');
                    var options;
                    $(project).each(function () {
                        options += '<option value="' + this.courseNo + '" >' + this.courseName + '</option>';
                    });
                    pro.append(options);
                }
            }
        })
    })
    $('#updateElectiveStudent').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var id = button.data('elective-id')
        var modal = $(this)
        var params = {
            "courseNo": null,
            "studentNo": null,
            "id": id
        }
        $.ajax({
            url: '<%=path%>/getElectiveStudent',
            type: "get",
            data: params,
            success: function (result) {
                modal.find('#update-course-id').val(result.id)
                modal.find('#update-profession').val(result.professionNo)
                modal.find('#update-course').val(result.courseNo)
                modal.find('#update-student').val(result.studentNo)
            }
        })
    })
    $('#deleteElectiveStudent').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var id = button.data('elective-id')
        var modal = $(this)
        modal.find('#delete-elective-id').val(id)
    })
</script>
</body>
</html>