<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>教师管理</title>
    <%@ include file="/WEB-INF/pages/css_template.jsp" %>
</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                    aria-controls="navbar">
                <span class="sr-only">学生管理系统</span> <span class="icon-bar"></span>
                <span class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar2" aria-expanded="false"
                    aria-controls="navbar" style="float: left;">
                <span class="sr-only">切换导航</span> <span class="icon-bar"></span> <span
                    class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<%=path%>/mainUrl">学生管理系统</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <!-- 导航条菜单 -->
            <%@ include file="/WEB-INF/pages/header_nav_template.jsp" %>

        </div>
    </div>
</nav>
<div class="container-fluid">
    <div id="navbar2" class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <!-- 侧边栏 -->
            <jsp:include page="/WEB-INF/pages/side_nav_template.jsp">
                <jsp:param value="active" name="aTeachers"/>
            </jsp:include>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="row">
                <div class="col-sm-7">
                    <div class="input-group">
                        <input id="search" type="text" class="form-control" placeholder="教师工号">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#selectTeachers">搜索</button>
                        </span>
                    </div>
                </div>
                <div class="col-sm-5">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                            data-target="#addTeachers">添加教师
                    </button>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>教师工号</th>
                        <th>教师姓名</th>
                        <th>性别</th>
                        <th>编辑</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="teacher" items="${teachers }" varStatus="status">
                        <tr>
                            <th>${(status.index + 1) + (pb.currPage - 1) * rows}</th>
                            <th>${teacher.teacherNo }</th>
                            <th>${teacher.teacherName }</th>
                            <th>
                                <c:choose>
                                    <c:when test="${teacher.gender == 1}">
                                        男
                                    </c:when>
                                    <c:otherwise>
                                        女
                                    </c:otherwise>
                                </c:choose>
                            </th>
                            <th>
                                <button class="btn btn-default"
                                        data-teacher-no="${teacher.teacherNo }" data-toggle="modal"
                                        data-target="#updateTeachers">编辑
                                </button>
                                <button class="btn btn-danger"
                                        data-teacher-no="${teacher.teacherNo }" data-toggle="modal"
                                        data-target="#deleteTeachers">删除
                                </button>
                            </th>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
            <div>
                <nav aria-label="...">
                    <ul class="pager">
                        <!-- <li class="previous"><a href="#"><span
                                aria-hidden="true">&larr;</span> 上一页</a></li>
                        <li class="next"><a href="#">下一页 <span
                                aria-hidden="true">&rarr;</span></a></li> -->
                        <li class="previous">
                            <p>每页显示</p>
                            <select onchange="location.href='<%=path%>/admin/adminTeachersUrl?rows=' + this.value">
                                <option value="3" <c:if test="${rows == 3}">selected</c:if>>3</option>
                                <option value="5" <c:if test="${rows == 5}">selected</c:if>>5</option>
                                <option value="10" <c:if test="${rows == 10}">selected</c:if>>10</option>
                                <option value="20" <c:if test="${rows == 20}">selected</c:if>>20</option>
                                <option value="30" <c:if test="${rows == 30}">selected</c:if>>30</option>
                                <option value="40" <c:if test="${rows == 40}">selected</c:if>>40</option>
                                <option value="50" <c:if test="${rows == 50}">selected</c:if>>50</option>
                            </select>
                            <p>行数据</p>
                        </li>
                        <c:if test="${pb.currPage == 1}">
                            <li class="disabled">
                                <a href="<%=path%>/admin/adminTeachersUrl?currentPage=1&rows=${rows}">
                                    <span aria-hidden="true">&larr;</span> 上一页
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${pb.currPage != 1}">
                            <li>
                                <a href="<%=path%>/admin/adminTeachersUrl?currentPage=${pb.currPage - 1}&rows=${rows}">
                                    <span aria-hidden="true">&larr;</span> 上一页
                                </a>
                            </li>
                        </c:if>

                        <c:forEach begin="1" end="${pb.totalPage}" var="i">
                            <c:if test="${pb.currPage == i}">
                                <li class="active">
                                    <a href="<%=path%>/admin/adminTeachersUrl?currentPage=${i}&rows=${rows}">${i}</a>
                                </li>
                            </c:if>
                            <c:if test="${pb.currPage != i}">
                                <li>
                                    <a href="<%=path%>/admin/adminTeachersUrl?currentPage=${i}&rows=${rows}">${i}</a>
                                </li>
                            </c:if>
                        </c:forEach>
                        <c:if test="${pb.currPage + 1 > pb.totalPage}">
                            <li class="disabled">
                                <a href="<%=path%>/admin/adminTeachersUrl?currentPage=${pb.currPage}&rows=${rows}">下一页
                                    <span aria-hidden="true">&rarr;</span>
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${pb.currPage + 1 <= pb.totalPage}">
                            <li>
                                <a href="<%=path%>/admin/adminTeachersUrl?currentPage=${pb.currPage + 1}&rows=${rows}">下一页
                                    <span aria-hidden="true">&rarr;</span>
                                </a>
                            </li>
                        </c:if>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- add teacher -->
<div class="modal fade" id="addTeachers" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/addTeachers">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="add-myModalLabel">录入教师信息</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="teacher-no" class="control-label">教师工号:</label>
                        <input type="text" class="form-control" name="teacherNo" id="teacher-no">
                    </div>
                    <div class="form-group">
                        <label for="teacher-name" class="control-label">教师名:</label>
                        <input type="text" class="form-control" name="teacherName" id="teacher-name">
                    </div>
                    <div class="form-group">
                        <label for="gender" class="control-label">性别:</label>
                        <select class="form-control" name="gender" id="gender">
                            <option value="1">男</option>
                            <option value="2">女</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary">提交</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- select teacher -->
<div class="modal fade" id="selectTeachers" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/adminTeachersUrl">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="select-myModalLabel">查找教师信息</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="select-teacher-no" class="control-label">教师工号:</label>
                        <input type="text" class="form-control" name="teacherNo" id="select-teacher-no">
                    </div>
                    <div class="form-group">
                        <label for="select-teacher-name" class="control-label">姓名:</label>
                        <input type="text" class="form-control" name="teacherName" id="select-teacher-name">
                    </div>
                    <div class="form-group">
                        <label for="select-gender" class="control-label">性别:</label>
                        <select class="form-control" name="gender" id="select-gender">
                            <option value="1">男</option>
                            <option value="2">女</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary">确定</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- update teacher -->
<div class="modal fade" id="updateTeachers" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/updateTeachers">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="update-myModalLabel">更新教师信息</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="update-teacher-no" class="control-label">教师工号:</label>
                        <input type="text" readonly class="form-control" name="teacherNo" id="update-teacher-no">
                    </div>
                    <div class="form-group">
                        <label for="update-teacher-name" class="control-label">姓名:</label>
                        <input type="text" class="form-control" name="teacherName" id="update-teacher-name">
                    </div>
                    <div class="form-group">
                        <label for="update-gender" class="control-label">性别:</label>
                        <select class="form-control" name="gender" id="update-gender">
                            <option value="1">男</option>
                            <option value="2">女</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary">保存</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- delete Teacher -->
<div class="modal fade" id="deleteTeachers" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/deleteTeachers">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="delete-myModalLabel">删除教师信息</h4>
                </div>
                <div class="modal-body">
                    确认要删除该教师的所有信息吗（该操作不可逆）？
                    <div class="form-group hidden">
                        <label for="delete-teacher-no" class="control-label">工号:</label>
                        <input type="text" class="form-control" name="teacherNo" id="delete-teacher-no">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-danger">删除</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="../js/jquery-3.5.1.js"></script>
<script src="../js/bootstrap.js"></script>

<script>
    $('#selectTeachers').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var teacherNo = $("#search").val();
        var modal = $(this)
        var params = {
            "teacherNo": teacherNo
        }
        $.ajax({
            url: '<%=path%>/getTeachers',
            type: "get",
            data: params,
            success: function (result) {
                modal.find('#select-teacher-no').val(result.teacherNo)
                modal.find('#select-teacher-name').val(result.teacherName)
                modal.find('#select-gender').val(result.gender)
            }
        })
    })
    $('#updateTeachers').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var teacherNo = button.data('teacher-no')
        var modal = $(this)
        var params = {
            "teacherNo": teacherNo
        }
        $.ajax({
            url: '<%=path%>/getTeachers',
            type: "get",
            data: params,
            success: function (result) {
                modal.find('#update-teacher-no').val(result.teacherNo)
                modal.find('#update-teacher-name').val(result.teacherName)
                modal.find('#update-gender').val(result.gender)
            }
        })
    })

    $('#deleteTeachers').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var teacherNo = button.data('teacher-no')
        var modal = $(this)
        modal.find('#delete-teacher-no').val(teacherNo)
    })
</script>
</body>

</html>