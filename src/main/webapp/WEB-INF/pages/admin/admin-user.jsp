<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>用户管理</title>
    <%@ include file="/WEB-INF/pages/css_template.jsp" %>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                    aria-controls="navbar">
                <span class="sr-only">学生管理系统</span> <span class="icon-bar"></span>
                <span class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar2" aria-expanded="false"
                    aria-controls="navbar" style="float: left;">
                <span class="sr-only">切换导航</span> <span class="icon-bar"></span> <span
                    class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<%=path%>/mainUrl">学生管理系统</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <!-- 导航条菜单 -->
            <%@ include file="/WEB-INF/pages/header_nav_template.jsp" %>
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div class="row">
        <div id="navbar2" class="col-sm-3 col-md-2 sidebar">
            <!-- 侧边栏 -->
            <jsp:include page="/WEB-INF/pages/side_nav_template.jsp">
                <jsp:param value="active" name="aUser"/>
            </jsp:include>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="row">
                <div class="col-sm-7">
                    <div class="input-group">
                        <input id="search" type="text" class="form-control" placeholder="账号">
                        <span class="input-group-btn">
								<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#selectUser">搜索</button>
                        </span>
                    </div>
                </div>
                <div class="col-sm-5">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addUser">添加用户账号</button>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>用户名字</th>
                        <th>账号</th>
                        <th>密码</th>
                        <th>账号状态</th>
                        <th>编辑</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="user" items="${users }" varStatus="status">
                        <tr>
                            <th>${(status.index + 1) + (pb.currPage - 1) * rows}</th>
                            <th>${user.displayName}</th>
                            <th>${user.username }</th>
                            <th>${user.psd}</th>
                            <th>${user.state }</th>
                            <th>
                                <button class="btn btn-default"
                                        data-user-username="${user.username }" data-toggle="modal"
                                        data-target="#updateUser">编辑
                                </button>
                                <button class="btn btn-danger"
                                        data-user-id="${user.userId }" data-toggle="modal"
                                        data-target="#deleteUser">删除
                                </button>
                            </th>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>

            <div>
                <nav aria-label="...">
                    <ul class="pager">
                        <!-- <li class="previous"><a href="#"><span
                                aria-hidden="true">&larr;</span> 上一页</a></li>
                        <li class="next"><a href="#">下一页 <span
                                aria-hidden="true">&rarr;</span></a></li> -->
                        <li class="previous">
                            <p>每页显示</p>
                            <select onchange="location.href='<%=path%>/admin/adminUserUrl?rows=' + this.value">
                                <option value="3" <c:if test="${rows == 3}">selected</c:if>>3</option>
                                <option value="5" <c:if test="${rows == 5}">selected</c:if>>5</option>
                                <option value="10" <c:if test="${rows == 10}">selected</c:if>>10</option>
                                <option value="20" <c:if test="${rows == 20}">selected</c:if>>20</option>
                                <option value="30" <c:if test="${rows == 30}">selected</c:if>>30</option>
                                <option value="40" <c:if test="${rows == 40}">selected</c:if>>40</option>
                                <option value="50" <c:if test="${rows == 50}">selected</c:if>>50</option>
                            </select>
                            <p>行数据</p>
                        </li>
                        <c:if test="${pb.currPage == 1}">
                            <li class="disabled">
                                <a href="<%=path%>/admin/adminUserUrl?currentPage=1&rows=${rows}">
                                    <span aria-hidden="true">&larr;</span> 上一页
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${pb.currPage != 1}">
                            <li>
                                <a href="<%=path%>/admin/adminUserUrl?currentPage=${pb.currPage - 1}&rows=${rows}">
                                    <span aria-hidden="true">&larr;</span> 上一页
                                </a>
                            </li>
                        </c:if>

                        <c:forEach begin="1" end="${pb.totalPage}" var="i">
                            <c:if test="${pb.currPage == i}">
                                <li class="active">
                                    <a href="<%=path%>/admin/adminUserUrl?currentPage=${i}&rows=${rows}">${i}</a>
                                </li>
                            </c:if>
                            <c:if test="${pb.currPage != i}">
                                <li>
                                    <a href="<%=path%>/admin/adminUserUrl?currentPage=${i}&rows=${rows}">${i}</a>
                                </li>
                            </c:if>
                        </c:forEach>

                        <c:if test="${pb.currPage + 1 > pb.totalPage}">
                            <li class="disabled">
                                <a href="<%=path%>/admin/adminUserUrl?currentPage=${pb.currPage}&rows=${rows}">下一页
                                    <span aria-hidden="true">&rarr;</span>
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${pb.currPage + 1 <= pb.totalPage}">
                            <li>
                                <a href="<%=path%>/admin/adminUserUrl?currentPage=${pb.currPage + 1}&rows=${rows}">下一页
                                    <span aria-hidden="true">&rarr;</span>
                                </a>
                            </li>
                        </c:if>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- add user -->
<div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/addUser">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="add-myModalLabel">添加账号</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="user-type" class="control-label">账号类型:</label>
                        <select class="form-control" name="usertype" id="user-type">
                            <option value="2">学生账号</option>
                            <option value="3">教师账号</option>
                        </select>
                    </div>
                    <div class="form-group" id="type2">
                        <label for="student-no" class="control-label">学生:</label>
                        <select class="form-control" name="studentNo" id="student-no">
                            <c:forEach var="student" items="${students }">
                                <option value="${student.studentNo }">${student.studentName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group hidden" id="type3">
                        <label for="teacher-no" class="control-label">老师:</label>
                        <select class="form-control" name="teacherNo" id="teacher-no">
                            <c:forEach var="teacher" items="${teachers }">
                                <option value="${teacher.teacherNo }">${teacher.teacherName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="user-password" class="control-label">密码:</label>
                        <input type="text" class="form-control" name="password" id="user-password">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary">提交</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- select user -->
<div class="modal fade" id="selectUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/adminUserUrl">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="select-myModalLabel">查看用户</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="select-user-id" class="control-label">id:</label>
                        <input type="text" class="form-control" name="userId" id="select-user-id">
                    </div>
                    <div class="form-group">
                        <label for="select-user-name" class="control-label">用户名字:</label>
                        <input type="text" class="form-control" name="userName" id="select-user-name">
                    </div>
                    <div class="form-group">
                        <label for="select-user-username" class="control-label">账号:</label>
                        <input type="text" class="form-control" name="userUsername" id="select-user-username">
                    </div>
                    <div class="form-group">
                        <label for="select-user-password" class="control-label">密码:</label>
                        <input type="text" class="form-control" name="userPassword" id="select-user-password">
                    </div>
                    <div class="form-group">
                        <label for="select-user-state" class="control-label">账号状态:</label>
                        <input type="text" class="form-control" name="userState" id="select-user-state">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">确定</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- update user -->
<div class="modal fade" id="updateUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/updateUser">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="update-myModalLabel">修改用户状态</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="update-user-id" class="control-label">id:</label>
                        <input type="text" readonly class="form-control" name="userId" id="update-user-id">
                    </div>
                    <div class="form-group">
                        <label for="update-user-name" class="control-label">用户名字:</label>
                        <input type="text" readonly class="form-control" name="userName" id="update-user-name">
                    </div>
                    <div class="form-group">
                        <label for="update-user-username" class="control-label">账号:</label>
                        <input type="text" readonly class="form-control" name="userUsername" id="update-user-username">
                    </div>
                    <div class="form-group">
                        <label for="update-user-password" class="control-label">密码:</label>
                        <input type="text" readonly class="form-control" name="userPassword" id="update-user-password">
                    </div>
                    <div class="form-group">
                        <label for="update-user-state" class="control-label">账号状态:</label>
                        <input type="text" class="form-control" name="userState" id="update-user-state">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">提交</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- delete user -->
<div class="modal fade" id="deleteUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/deleteUser">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="delete-myModalLabel">删除账号信息</h4>
                </div>
                <div class="modal-body">
                    确认要删除该同学的所有信息吗（该操作不可逆）？
                    <div class="form-group hidden">
                        <label for="delete-user-id" class="control-label">用户id:</label>
                        <input type="text" class="form-control" name="userId" id="delete-user-id">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-danger">删除</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="../js/jquery-3.5.1.js"></script>
<script src="../js/bootstrap.js"></script>

<script>
    $('#user-type').on('change', function () {
        var usertype = $("#user-type").val();
        if (usertype == '3') {
            $('#type3').removeClass("hidden");
            $('#type2').addClass("hidden");
        } else {
            $('#type2').removeClass("hidden");
            $('#type3').addClass("hidden");
        }

    })
    $('#selectUser').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var username = $("#search").val();
        var modal = $(this)
        var params = {
            "username": username
        }
        $.ajax({
            url: '<%=path%>/getUserS',
            type: "get",
            data: params,
            success: function (result) {
                modal.find('#select-user-id').val(result.userId)
                modal.find('#select-user-name').val(result.displayName)
                modal.find('#select-user-username').val(result.username)
                modal.find('#select-user-password').val(result.psd)
                modal.find('#select-user-state').val(result.state)
            }
        })
    })
    $('#updateUser').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var username = button.data('user-username')
        var modal = $(this)
        var params = {
            "username": username
        }
        $.ajax({
            url: '<%=path%>/getUserS',
            type: "get",
            data: params,
            success: function (result) {
                modal.find('#update-user-id').val(result.userId)
                modal.find('#update-user-name').val(result.displayName)
                modal.find('#update-user-username').val(result.username)
                modal.find('#update-user-password').val(result.psd)
                modal.find('#update-user-state').val(result.state)
            }
        })
    })
    $('#deleteUser').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var userId = button.data('user-id')
        var modal = $(this)
        modal.find('#delete-user-id').val(userId)
    })
</script>
</body>

</html>