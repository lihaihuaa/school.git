<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>成绩管理</title>
    <%@ include file="/WEB-INF/pages/css_template.jsp" %>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                    aria-controls="navbar">
                <span class="sr-only">学生管理系统</span> <span class="icon-bar"></span>
                <span class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar2" aria-expanded="false"
                    aria-controls="navbar" style="float: left;">
                <span class="sr-only">切换导航</span> <span class="icon-bar"></span> <span
                    class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<%=path%>/mainUrl">学生管理系统</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <!-- 导航条菜单 -->
            <%@ include file="/WEB-INF/pages/header_nav_template.jsp" %>
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div class="row">
        <div id="navbar2" class="col-sm-3 col-md-2 sidebar">
            <!-- 侧边栏 -->
            <jsp:include page="/WEB-INF/pages/side_nav_template.jsp">
                <jsp:param value="active" name="aScores"/>
            </jsp:include>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="row">
                <div class="col-sm-7">
                    <div class="input-group">
                        <input id="search1" type="text" class="form-control" placeholder="课程号">
                        <input id="search2" type="text" class="form-control" placeholder="学号">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#selectScores">搜索</button>
                        </span>
                    </div>
                </div>
                <div class="col-sm-5">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addScores">成绩录入</button>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>课程号</th>
                        <th>课程名</th>
                        <th>学号</th>
                        <th>姓名</th>
                        <th>分数</th>
                        <th>绩点</th>
                        <th>编辑</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="score" items="${scores }" varStatus="status">
                        <tr>
                            <th>${(status.index + 1) + (pb.currPage - 1) * rows}</th>
                            <th>${score.courseNo }</th>
                            <th>
                                <c:forEach var="course" items="${courses }">
                                    <c:if test="${course.courseNo == score.courseNo}">${course.courseName}</c:if>
                                </c:forEach>
                            </th>
                            <th>${score.studentNo }</th>
                            <th>
                                <c:forEach var="student" items="${students }">
                                    <c:if test="${student.studentNo == score.studentNo}">${student.studentName}</c:if>
                                </c:forEach>
                            </th>
                            <th>${score.score }</th>
                            <th>${score.gradePoint }</th>
                            <th>
                                <button class="btn btn-default"
                                        data-score-id="${ score.id}" data-toggle="modal"
                                        data-target="#updateScores">编辑
                                </button>
                                <button class="btn btn-danger"
                                        data-score-id="${ score.id}" data-toggle="modal"
                                        data-target="#deleteScores">删除
                                </button>
                            </th>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>

            <div>
                <nav aria-label="...">
                    <ul class="pager">
                        <!-- <li class="previous"><a href="#"><span
                                aria-hidden="true">&larr;</span> 上一页</a></li>
                        <li class="next"><a href="#">下一页 <span
                                aria-hidden="true">&rarr;</span></a></li> -->
                        <li class="previous">
                            <p>每页显示</p>
                            <select onchange="location.href='<%=path%>/admin/adminScoresUrl?rows=' + this.value">
                                <option value="3" <c:if test="${rows == 3}">selected</c:if>>3</option>
                                <option value="5" <c:if test="${rows == 5}">selected</c:if>>5</option>
                                <option value="10" <c:if test="${rows == 10}">selected</c:if>>10</option>
                                <option value="20" <c:if test="${rows == 20}">selected</c:if>>20</option>
                                <option value="30" <c:if test="${rows == 30}">selected</c:if>>30</option>
                                <option value="40" <c:if test="${rows == 40}">selected</c:if>>40</option>
                                <option value="50" <c:if test="${rows == 50}">selected</c:if>>50</option>
                            </select>
                            <p>行数据</p>
                        </li>
                        <c:if test="${pb.currPage == 1}">
                            <li class="disabled">
                                <a href="<%=path%>/admin/adminScoresUrl?currentPage=1&rows=${rows}">
                                    <span aria-hidden="true">&larr;</span> 上一页
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${pb.currPage != 1}">
                            <li>
                                <a href="<%=path%>/admin/adminScoresUrl?currentPage=${pb.currPage - 1}&rows=${rows}">
                                    <span aria-hidden="true">&larr;</span> 上一页
                                </a>
                            </li>
                        </c:if>
                        <c:forEach begin="1" end="${pb.totalPage}" var="i">
                            <c:if test="${pb.currPage == i}">
                                <li class="active">
                                    <a href="<%=path%>/admin/adminScoresUrl?currentPage=${i}&rows=${rows}">${i}</a>
                                </li>
                            </c:if>
                            <c:if test="${pb.currPage != i}">
                                <li>
                                    <a href="<%=path%>/admin/adminScoresUrl?currentPage=${i}&rows=${rows}">${i}</a>
                                </li>
                            </c:if>
                        </c:forEach>
                        <c:if test="${pb.currPage + 1 > pb.totalPage}">
                            <li class="disabled">
                                <a href="<%=path%>/admin/adminScoresUrl?currentPage=${pb.currPage}&rows=${rows}">下一页
                                    <span aria-hidden="true">&rarr;</span>
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${pb.currPage + 1 <= pb.totalPage}">
                            <li>
                                <a href="<%=path%>/admin/adminScoresUrl?currentPage=${pb.currPage + 1}&rows=${rows}">下一页
                                    <span aria-hidden="true">&rarr;</span>
                                </a>
                            </li>
                        </c:if>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- add student -->
<div class="modal fade" id="addScores" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/addScores">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="add-myModalLabel">录入学生成绩</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="profession-no" class="control-label">选择专业:</label>
                        <select class="form-control" name="professionNo" id="profession-no">
                            <c:forEach var="profession" items="${professions }">
                                <option value="${profession.professionNo }">${profession.professionName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group hidden" id="type1">
                        <label for="course-no" class="control-label">课程:</label>
                        <select class="form-control" name="courseNo" id="course-no"></select>
                    </div>
                    <div class="form-group hidden" id="type2">
                        <label for="student-no" class="control-label">学生:</label>
                        <select class="form-control" name="studentNo" id="student-no"></select>
                    </div>
                    <div class="form-group hidden" id="type3">
                        <label for="score" class="control-label">分数:</label>
                        <input type="text" class="form-control" name="score" id="score">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary">提交</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- select score -->
<div class="modal fade" id="selectScores" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/adminScoresUrl">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="select-myModalLabel">查询分数</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="select-id" class="control-label">id:</label>
                        <input type="text" readonly class="form-control" name="id" id="select-id">
                    </div>
                    <div class="form-group">
                        <label for="select-profession" class="control-label">专业:</label>
                        <select class="form-control" name="professionNo" id="select-profession">
                            <c:forEach var="profession" items="${professions }">
                                <option value="${profession.professionNo }">${profession.professionName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="select-course-no" class="control-label">课程号:</label>
                        <input type="text" class="form-control" name="courseNo" id="select-course-no">
                    </div>
                    <div class="form-group">
                        <label for="select-course" class="control-label">课程:</label>
                        <select class="form-control" name="courseNo" id="select-course">
                            <c:forEach var="course" items="${courses }">
                                <option value="${course.courseNo }">${course.courseName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="select-student-no" class="control-label">学号:</label>
                        <input type="text" class="form-control" name="studentNo" id="select-student-no">
                    </div>
                    <div class="form-group">
                        <label for="select-student" class="control-label">学生:</label>
                        <select class="form-control" name="studentNo" id="select-student">
                            <c:forEach var="student" items="${students }">
                                <option value="${student.studentNo }">${student.studentName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="select-score" class="control-label">分数:</label> 
                        <input type="text" class="form-control" name="score" id="select-score">
                    </div>
                    <div class="form-group">
                        <label for="select-gradePoint" class="control-label">绩点:</label>
                        <input type="text" class="form-control" name="gradePoint" id="select-gradePoint">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary">确定</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- update score -->
<div class="modal fade" id="updateScores" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/updateScores">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="update-myModalLabel">更新分数</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="update-id" class="control-label">id:</label>
                        <input type="text" readonly class="form-control" name="id" id="update-id">
                    </div>
                    <div class="form-group">
                        <label for="update-profession" class="control-label">专业:</label>
                        <select readonly class="form-control" name="professionNo" id="update-profession">
                            <c:forEach var="profession" items="${professions }">
                                <option value="${profession.professionNo }">${profession.professionName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="update-course-no" class="control-label">课程号:</label>
                        <input readonly type="text" class="form-control" name="courseNo" id="update-course-no">
                    </div>
                    <div class="form-group">
                        <label for="update-course" class="control-label">课程:</label>
                        <select readonly class="form-control" name="courseNo" id="update-course">
                            <c:forEach var="course" items="${courses }">
                                <option value="${course.courseNo }">${course.courseName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="update-student-no" class="control-label">学号:</label>
                        <input readonly type="text" class="form-control" name="studentNo" id="update-student-no">
                    </div>
                    <div class="form-group">
                        <label for="update-student" class="control-label">学生:</label>
                        <select readonly class="form-control" name="studentNo" id="update-student">
                            <c:forEach var="student" items="${students }">
                                <option value="${student.studentNo }">${student.studentName }</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="update-score" class="control-label">分数:</label>
                        <input type="text" class="form-control" name="score" id="update-score">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-primary">保存</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- delete Score -->
<div class="modal fade" id="deleteScores" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<%=path%>/admin/deleteScores">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="delete-myModalLabel">删除成绩</h4>
                </div>
                <div class="modal-body">
                    确认要删除该条成绩的所有信息吗（该操作不可逆）？
                    <div class="form-group hidden">
                        <label for="delete-score-id" class="control-label">id:</label>
                        <input type="text" class="form-control" name="id" id="delete-score-id">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="submit" class="btn btn-danger">删除</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="../js/jquery-3.5.1.js"></script>
<script src="../js/bootstrap.js"></script>
<script>
    $('#profession-no').on('change', function () {
        var professionNo = $("#profession-no").val();
        $('#type1').removeClass("hidden");
        $('#type2').removeClass("hidden");
        $('#type3').removeClass("hidden");
        $('#course-no').html("");
        $('#student-no').html("");
        var params = {
            "professionNo": professionNo
        }
        $.ajax({
            url: '<%=path%>/getAddScores',
            type: "get",
            data: params,
            success: function (result) {
                if (result && result.courseList.length > 0) {
                    var project = result.courseList;
                    var pro = $('#course-no');
                    var options;
                    $(project).each(function () {
                        options += '<option value="' + this.courseNo + '" >' + this.courseName + '</option>';
                    });
                    pro.append(options);
                }
                if (result && result.studentList.length > 0) {
                    var project2 = result.studentList;
                    var pro2 = $('#student-no');
                    var options2;
                    $(project2).each(function () {
                        options2 += '<option value="' + this.studentNo + '" >' + this.studentName + '</option>';
                    });
                    pro2.append(options2);
                }
            }
        })
    })
    $('#selectScores').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var courseNo = $("#search1").val();
        var studentNo = $("#search2").val();
        var modal = $(this)
        var params = {
            "courseNo": courseNo,
            "studentNo": studentNo,
            "id": null
        }
        if (courseNo == '' || courseNo == null || studentNo == '' && studentNo == null) {
            alert("课程号和学生学号不能为空！");
        }
        $.ajax({
            url: '<%=path%>/getScores',
            type: "get",
            data: params,
            success: function (result) {
                modal.find('#select-id').val(result.scores.id)
                modal.find('#select-profession').val(result.professionNo)
                modal.find('#select-course-no').val(result.scores.courseNo)
                modal.find('#select-course').val(result.scores.courseNo)
                modal.find('#select-student-no').val(result.scores.studentNo)
                modal.find('#select-student').val(result.scores.studentNo)
                modal.find('#select-score').val(result.scores.score)
                modal.find('#select-gradePoint').val(result.scores.gradePoint)
            }
        })
    })
    $('#updateScores').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var scoreId = button.data('score-id')
        var modal = $(this)
        var params = {
            "courseNo": null,
            "studentNo": null,
            "id": scoreId
        }
        $.ajax({
            url: '<%=path%>/getScores',
            type: "get",
            data: params,
            success: function (result) {
                modal.find('#update-id').val(result.scores.id)
                modal.find('#update-profession').val(result.professionNo)
                modal.find('#update-course-no').val(result.scores.courseNo)
                modal.find('#update-course').val(result.scores.courseNo)
                modal.find('#update-student-no').val(result.scores.studentNo)
                modal.find('#update-student').val(result.scores.studentNo)
                modal.find('#update-score').val(result.scores.score)
            }
        })
    })
    $('#deleteScores').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var scoreId = button.data('score-id')
        var modal = $(this)
        modal.find('#delete-score-id').val(scoreId)
    })
</script>
</body>
</html>
