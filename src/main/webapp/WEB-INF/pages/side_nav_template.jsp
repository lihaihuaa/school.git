<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
%>
<ul class="nav nav-sidebar">
    <c:choose>
        <c:when test="${loginUser.userType == 1 }">
            <li class="	<%=request.getParameter("1")%>">
                <a href="<%=path%>/mainUrl">首页</a>
            </li>
            <li class="	<%=request.getParameter("aGrade")%>">
                <a href="<%=path%>/admin/adminGradeUrl">年级管理</a>
            </li>
            <li class="	<%=request.getParameter("aCollege")%>">
                <a href="<%=path%>/admin/adminCollegeUrl">学院管理</a>
            </li>
            <li class="	<%=request.getParameter("aProfession")%>">
                <a href="<%=path%>/admin/adminProfessionUrl">专业管理</a>
            </li>
            <li class="	<%=request.getParameter("aClass")%>">
                <a href="<%=path%>/admin/adminProfessionClassUrl">班级管理</a>
            </li>
            <li class="	<%=request.getParameter("aCourse")%>">
                <a href="<%=path%>/admin/adminProfessionCourseUrl">课程管理</a>
            </li>
            <li class="	<%=request.getParameter("aElective")%>">
                <a href="<%=path%>/admin/adminElectiveStudentUrl">选课管理</a>
            </li>
            <li class="	<%=request.getParameter("aScores")%>">
                <a href="<%=path%>/admin/adminScoresUrl">成绩管理</a>
            </li>
            <li class="	<%=request.getParameter("aStudents")%>">
                <a href="<%=path%>/admin/adminStudentsUrl">学生管理</a>
            </li>
            <li class="	<%=request.getParameter("aTeachers")%>">
                <a href="<%=path%>/admin/adminTeachersUrl">教师管理</a>
            </li>
            <li class="	<%=request.getParameter("aUser")%>">
                <a href="<%=path%>/admin/adminUserUrl">账号管理</a>
            </li>
        </c:when>

        <c:when test="${loginUser.userType == 2 }">
            <li class="	<%=request.getParameter("1")%>">
                <a href="<%=path%>/mainUrl">首页</a>
            </li>
            <li class="	<%=request.getParameter("sCourse")%>">
                <a href="<%=path%>/user/student/courseUrl">课程信息</a>
            </li>
            <li class="	<%=request.getParameter("sElective")%>">
                <a href="<%=path%>/user/student/electiveUrl">选课信息</a>
            </li>
            <li class="	<%=request.getParameter("sScore")%>">
                <a href="<%=path%>/user/student/scoreUrl">成绩信息</a>
            </li>
            <li class="	<%=request.getParameter("sInfo")%>">
                <a href="<%=path%>/user/student/infoUrl">档案信息</a>
            </li>
        </c:when>

        <c:otherwise>
            <li class="	<%=request.getParameter("1")%>">
                <a href="<%=path%>/mainUrl">首页</a>
            </li>
            <li class="	<%=request.getParameter("tClass")%>">
                <a href="<%=path%>/user/teacher/classUrl">班级信息</a>
            </li>
            <li class="	<%=request.getParameter("tCourse")%>">
                <a href="<%=path%>/user/teacher/courseUrl">课程信息</a>
            </li>
            <li class="	<%=request.getParameter("tElective")%>">
                <a href="<%=path%>/user/teacher/electiveUrl">选课信息</a>
            </li>
            <li class="	<%=request.getParameter("tScore")%>">
                <a href="<%=path%>/user/teacher/scoreUrl">成绩信息</a>
            </li>
            <li class="	<%=request.getParameter("tInfo")%>">
                <a href="<%=path%>/user/teacher/infoUrl">档案信息</a>
            </li>
        </c:otherwise>
    </c:choose>

</ul>