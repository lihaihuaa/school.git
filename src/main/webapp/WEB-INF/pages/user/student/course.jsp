<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>课程信息</title>
    <%@ include file="/WEB-INF/pages/css_template.jsp" %>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                    aria-controls="navbar">
                <span class="sr-only">学生管理系统</span> <span class="icon-bar"></span>
                <span class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar2" aria-expanded="false"
                    aria-controls="navbar" style="float: left;">
                <span class="sr-only">切换导航</span> <span class="icon-bar"></span> <span
                    class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<%=path%>/mainUrl">学生管理系统</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <!-- 导航条菜单 -->
            <%@ include file="/WEB-INF/pages/header_nav_template.jsp" %>
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div class="row">
        <div id="navbar2" class="col-sm-3 col-md-2 sidebar">
            <!-- 侧边栏 -->
            <jsp:include page="/WEB-INF/pages/side_nav_template.jsp">
                <jsp:param value="active" name="sCourse"/>
            </jsp:include>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>课程号</th>
                        <th>课程名字</th>
                        <th>课程类型</th>
                        <th>课程学分</th>
                        <th>上限人数</th>
                        <th>授课老师</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="course" items="${courses }" varStatus="status">
                        <tr>
                            <th>${(status.index + 1) + (pb.currPage - 1) * rows}</th>
                            <th>${course.courseNo }</th>
                            <th>${course.courseName }</th>
                            <th>
                                <c:choose>
                                    <c:when test="${course.courseType == '1'}">
                                        必修课
                                    </c:when>
                                    <c:when test="${course.courseType == '2'}">
                                        选修课
                                    </c:when>
                                    <c:otherwise>
                                        实践课
                                    </c:otherwise>
                                </c:choose>
                            </th>
                            <th>${course.courseCredit }</th>
                            <th>
                                <c:choose>
                                    <c:when test="${course.courseNum == 0}">
                                        无上限
                                    </c:when>
                                    <c:otherwise>
                                        ${course.courseNum }
                                    </c:otherwise>
                                </c:choose>
                            </th>
                            <th>
                                <c:forEach var="teacher" items="${teachers }">
                                    <c:if test="${teacher.teacherNo == course.teacherNo}">${teacher.teacherName}</c:if>
                                </c:forEach>
                            </th>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>

            <!-- 分页 -->
            <div>
                <nav aria-label="...">
                    <ul class="pager hidden">
                        <li class="previous"><a href="#"><span
                                aria-hidden="true">&larr;</span> 上一页</a></li>
                        <li class="next"><a href="#">下一页 <span
                                aria-hidden="true">&rarr;</span></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<script src="./js/jquery-3.5.1.js"></script>
<script src="./js/bootstrap.js"></script>
</body>
</html>