<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <title>说明</title>
    <%@ include file="/WEB-INF/pages/css_template.jsp" %>

</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                    aria-controls="navbar">
                <span class="sr-only">学生管理系统</span> <span class="icon-bar"></span>
                <span class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar2" aria-expanded="false"
                    aria-controls="navbar" style="float: left;">
                <span class="sr-only">切换导航</span> <span class="icon-bar"></span> <span
                    class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<%=path%>/mainUrl">学生管理系统</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <!-- 导航条菜单 -->
            <%@ include file="/WEB-INF/pages/header_nav_template.jsp" %>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div id="navbar2" class="col-sm-3 col-md-2 sidebar">

            <!-- 侧边栏 -->
            <jsp:include page="/WEB-INF/pages/side_nav_template.jsp">
                <jsp:param value="active" name="0"/>
            </jsp:include>

        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">联系方式</h1>
            <div class="media">
                <div class="media-left media-middle">
                    <a href="#"> <img class="media-object" src="<%=basePath%>/img/nav0.jpg" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">学校教务信息管理系统</h4>
                    <a href="https://www.gxust.edu.cn/">学校：广西科技大学</a><br>
                    <a href="https://www.gxust.edu.cn/qdszxy/">学院：启迪数字学院</a><br>
                    <a href="https://wpa.qq.com/msgrd?v=3&uin=76365404&site=qq&menu=yes">作者：李海华</a><br>
                    <a href="https://wpa.qq.com/msgrd?v=3&uin=76365404&site=qq&menu=yes">作者QQ：76365404</a><br>
                    <a href="mailto:76365404@qq.com">QQ邮箱:76365404@qq.com</a><br>
                    <b>个人评价：</b>为人诚恳勤奋好学、脚踏实地，十分热爱互联网，做事及相处比较有耐心，遇到问题能及时寻找解决方案，乐于挑战困难，有团队合作意识，服从团队安排，乐于学习新技术，接受加班！<br>
                </div>
            </div>

        </div>
    </div>
</div>
<script src="./js/jquery-3.5.1.js"></script>
<script src="./js/bootstrap.js"></script>
</body>
</html>