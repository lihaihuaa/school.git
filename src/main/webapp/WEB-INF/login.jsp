<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>学校教务信息管理系统</title>

    <%
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
    %>

    <link rel="stylesheet" href="<%=basePath%>/css/bootstrap.css">

    <style>
        body {
            background-image: url("<%=basePath%>/img/bg.jpg");
            background-size: cover;
            color: white;
        }

        .loginBox {
            width: 400px;
            height: 300px;
            position: absolute;
            top: 50%;
            left: 50%;
            margin-left: -200px;
            margin-top: -150px;
        }

        .loginBtn {
            height: 40px;
            width: 200px;
            text-align: center;
            margin: auto;
        }

        .loginLabel {
            text-align: center;
            font-size: 2em;
        }

    </style>
</head>

<body>
<div class="container">
    <div class="loginBox">
        <form class="form-horizontal" action="<%=basePath%>/loginUrl" method="post">
            <div class="loginLabel">
                <label style="color: red">学校管理系统</label>
            </div>

            <div class="form-group" style="margin-top: 20px;">
                <label for="username" class="col-sm-2 control-label">账号</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" name="username" id="username" placeholder="请输入用户名">
                </div>
            </div>
            <div class="form-group" style="margin-top: 25px;">
                <label for="password" class="col-sm-2 control-label">密码</label>
                <div class="col-sm-8">
                    <input type="password" class="form-control" name="password" id="password" placeholder="请输入密码">
                </div>
            </div>

            <div class="loginBtn">
                <button type="submit" class=" btn btn-info loginBtn">登录</button>
            </div>
        </form>
    </div>
</div>
<script src="./js/jquery-3.5.1.js"></script>
<script src="./js/bootstrap.js"></script>
</body>
</html>