<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>学校教务信息管理系统</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <%
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
    %>
    <link rel="stylesheet" href="<%=basePath%>/bootstrap4/bootstrap.css">
    <style type="text/css">
        .login-wrapper {
            width: 100vw;
            height: 100vh;
            color: #fff;
        }

        .login {
            background: rgba(0,206,209, 0.6);
        }

        .slide-img {
            display: block;
            width: 100vw;
            height: 100vh;
        }
        .btn-my {
            color: #fff;
            background-color: #66CDAA;
            border: none;
        }

        .btn-my:hover {
            color: #fff;
            background-color: #218838;
            border-color: #1e7e34;
        }
    </style>
</head>
<body>
<div id="mySlider" class="carousel slide h-100 position-fixed" data-ride="carousel">
    <ol class="carousel-indicators position-fixed">
        <li data-target="#mySlider" data-slide-to="0" class="active"></li>
        <li data-target="#mySlider" data-slide-to="1"></li>
        <li data-target="#mySlider" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="slide-img" src="img/bg.jpg" />
        </div>
        <div class="carousel-item">
            <img class="slide-img" src="img/bg1.jpg" />
        </div>
        <div class="carousel-item">
            <img class="slide-img" src="img/bg2.jpg" />
        </div>
    </div>
    <a class="carousel-control-prev position-fixed" href="#mySlider" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">上一个</span>
    </a>
    <a class="carousel-control-next position-fixed" href="#mySlider" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">下一个</span>
    </a>
</div>

<div class="d-flex justify-content-center login-wrapper">
    <div class="col-sm-8 col-md-6 col-xl-4 p-4 card align-self-center login">
        <div class="card-header text-center bg-transparent">
            <h4 class="mb-0">学校管理系统</h4>
        </div>
        <div class="card-body">
            <form action="<%=basePath%>/loginUrl" method="post">
                <div class="form-group text-left">
                    <input type="text" class="form-control required" placeholder="用户名" autocomplete="off" name="username" autofocus="autofocus" maxlength="32">
                </div>
                <div class="form-group text-left">
                    <input type="password" class="form-control required" name="password" placeholder="密码" maxlength="32">
                </div>
                <div class="">
                    <button type="submit" class="btn btn-my btn-block" >登&nbsp;&nbsp;录</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="./js/jquery-3.5.1.js"></script>
<script src="./bootstrap4/bootstrap.js"></script>
</body>
</html>
