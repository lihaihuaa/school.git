package com.kuhh.filters;

import com.kuhh.pojo.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@WebFilter(value = "/user/student/*", initParams = {
        @WebInitParam(name = "encoding", value = "UTF-8")
})
public class UserStudentFilter implements Filter {
    private String encoding;

    @Override
    public void destroy() {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        //设置编码
        response.setCharacterEncoding(encoding);
        HttpServletRequest req = (HttpServletRequest) request;
        req.setCharacterEncoding(encoding);
        User user = (User) req.getSession().getAttribute("loginUser");
        Byte type = user.getUserType();
        if (type != 2) {
            //不是学生用户，跳转到错误页面
            req.setAttribute("msg", "抱歉，您不是学生用户，您没有权限访问！");
            req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, response);
        } else {
            chain.doFilter(req, response);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //过滤器初始化的时候调用该方法
        encoding = filterConfig.getInitParameter("encoding");
        //默认值是UTF-8
        if (encoding == null) {
            encoding = "UTF-8";
        }
    }
}
