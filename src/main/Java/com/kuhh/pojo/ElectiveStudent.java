package com.kuhh.pojo;

import java.util.Date;

public class ElectiveStudent {
	//序号id
	private Integer id;
	//专业号
	private String professionNo;
	//课程号
	private String courseNo;
	//学生学号
	private String studentNo;
	//创建时间
	private Date createTime;
	//修改时间
	private Date updateTime;
	
	public ElectiveStudent() {
		super();
	}

	public ElectiveStudent(Integer id, String professionNo, String courseNo, String studentNo, Date createTime, Date updateTime) {
		this.id = id;
		this.professionNo = professionNo;
		this.courseNo = courseNo;
		this.studentNo = studentNo;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getProfessionNo() {
		return professionNo;
	}

	public void setProfessionNo(String professionNo) {
		this.professionNo = professionNo;
	}

	public String getCourseNo() {
		return courseNo;
	}

	public void setCourseNo(String courseNo) {
		this.courseNo = courseNo;
	}

	public String getStudentNo() {
		return studentNo;
	}

	public void setStudentNo(String studentNo) {
		this.studentNo = studentNo;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		return "ElectiveStudent{" +
				"id=" + id +
				", professionNo='" + professionNo + '\'' +
				", courseNo='" + courseNo + '\'' +
				", studentNo='" + studentNo + '\'' +
				", createTime=" + createTime +
				", updateTime=" + updateTime +
				'}';
	}
}
