package com.kuhh.pojo;

import java.util.Date;

public class ProfessionClass {
	//班级No号
	private String classNo;
	//班级名字
	private String className;
	//专业No号
	private String professionNo;
	//教师工号
	private String teacherNo;
	//创建时间
	private Date createTime;
	//修改时间
	private Date updateTime;
	
	public ProfessionClass() {
		super();
	}

	public ProfessionClass(String classNo, String className, String professionNo, String teacherNo, Date createTime,
			Date updateTime) {
		super();
		this.classNo = classNo;
		this.className = className;
		this.professionNo = professionNo;
		this.teacherNo = teacherNo;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	public String getClassNo() {
		return classNo;
	}

	public void setClassNo(String classNo) {
		this.classNo = classNo;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getProfessionNo() {
		return professionNo;
	}

	public void setProfessionNo(String professionNo) {
		this.professionNo = professionNo;
	}

	public String getTeacherNo() {
		return teacherNo;
	}

	public void setTeacherNo(String teacherNo) {
		this.teacherNo = teacherNo;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		return "ProfessionClass [classNo=" + classNo + ", className=" + className + ", professionNo=" + professionNo
				+ ", teacherNo=" + teacherNo + ", createTime=" + createTime + ", updateTime=" + updateTime + "]";
	}

}
