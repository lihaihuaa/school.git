package com.kuhh.pojo;

import java.util.Date;

public class LoginHistory {
	private Integer id;
	
	private Integer userId;
	
	private String ip;
	
	private Date createTime;
	
	private Integer num;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public LoginHistory() {
		super();
		// TODO 自动生成的构造函数存根
	}

	public LoginHistory(Integer id, Integer userId, String ip, Date createTime, Integer num) {
		super();
		this.id = id;
		this.userId = userId;
		this.ip = ip;
		this.createTime = createTime;
		this.num = num;
	}

}
