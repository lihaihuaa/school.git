package com.kuhh.pojo;

import java.util.Date;

public class Grade {
	//年级No号
	private String gradeNo;
	//年级名字
	private String gradeName;
	//教师工号
	private String teacherNo;
	//创建时间
	private Date createTime;
	//修改时间
	private Date updateTime;
	
	public Grade() {
		super();
	}

	public Grade(String gradeNo, String gradeName, String teacherNo, Date createTime, Date updateTime) {
		super();
		this.gradeNo = gradeNo;
		this.gradeName = gradeName;
		this.teacherNo = teacherNo;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	public String getGradeNo() {
		return gradeNo;
	}

	public void setGradeNo(String gradeNo) {
		this.gradeNo = gradeNo;
	}

	public String getGradeName() {
		return gradeName;
	}

	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}

	public String getTeacherNo() {
		return teacherNo;
	}

	public void setTeacherNo(String teacherNo) {
		this.teacherNo = teacherNo;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		return "Grade [gradeNo=" + gradeNo + ", gradeName=" + gradeName + ", teacherNo=" + teacherNo + ", createTime="
				+ createTime + ", updateTime=" + updateTime + "]";
	}
	
}
