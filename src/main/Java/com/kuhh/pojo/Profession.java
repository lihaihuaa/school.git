package com.kuhh.pojo;

import java.util.Date;

public class Profession {
	//专业No号
	private String professionNo;
	//专业名字
	private String professionName;
	//学院No号
	private String collegeNo;
	//创建时间
	private Date createTime;
	//修改时间
	private Date updateTime;
	
	public Profession() {
		super();
	}
	public Profession(String professionNo, String professionName, String collegeNo, Date createTime, Date updateTime) {
		super();
		this.professionNo = professionNo;
		this.professionName = professionName;
		this.collegeNo = collegeNo;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}
	public String getProfessionNo() {
		return professionNo;
	}
	public void setProfessionNo(String professionNo) {
		this.professionNo = professionNo;
	}
	public String getProfessionName() {
		return professionName;
	}
	public void setProfessionName(String professionName) {
		this.professionName = professionName;
	}
	public String getCollegeNo() {
		return collegeNo;
	}
	public void setCollegeNo(String collegeNo) {
		this.collegeNo = collegeNo;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	@Override
	public String toString() {
		return "Profession [professionNo=" + professionNo + ", professionName=" + professionName + ", collegeNo="
				+ collegeNo + ", createTime=" + createTime + ", updateTime=" + updateTime + "]";
	}
	
	
}
