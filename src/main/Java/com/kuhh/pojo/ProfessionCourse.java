package com.kuhh.pojo;

import java.util.Date;

public class ProfessionCourse {
	//课程号
	private String courseNo;
	//课程名字
	private String courseName;
	//课程类型
	private String courseType;
	//课程学分
	private String courseCredit;
	//课程上限人数，默认为空，表示无上限
	private Integer courseNum;
	//年级No号
	private String gradeNo;
	//专业No号
	private String professionNo;
	//教师工号
	private String teacherNo;
	//创建时间
	private Date createTime;
	//修改时间
	private Date updateTime;
	
	public ProfessionCourse() {
		super();
	}

	public ProfessionCourse(String courseNo, String courseName, String courseType, String courseCredit, Integer courseNum, String gradeNo, String professionNo, String teacherNo, Date createTime, Date updateTime) {
		this.courseNo = courseNo;
		this.courseName = courseName;
		this.courseType = courseType;
		this.courseCredit = courseCredit;
		this.courseNum = courseNum;
		this.gradeNo = gradeNo;
		this.professionNo = professionNo;
		this.teacherNo = teacherNo;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	public String getCourseNo() {
		return courseNo;
	}

	public void setCourseNo(String courseNo) {
		this.courseNo = courseNo;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseType() {
		return courseType;
	}

	public void setCourseType(String courseType) {
		this.courseType = courseType;
	}

	public String getCourseCredit() {
		return courseCredit;
	}

	public void setCourseCredit(String courseCredit) {
		this.courseCredit = courseCredit;
	}

	public Integer getCourseNum() {
		return courseNum;
	}

	public void setCourseNum(Integer courseNum) {
		this.courseNum = courseNum;
	}

	public String getGradeNo() {
		return gradeNo;
	}

	public void setGradeNo(String gradeNo) {
		this.gradeNo = gradeNo;
	}

	public String getProfessionNo() {
		return professionNo;
	}

	public void setProfessionNo(String professionNo) {
		this.professionNo = professionNo;
	}

	public String getTeacherNo() {
		return teacherNo;
	}

	public void setTeacherNo(String teacherNo) {
		this.teacherNo = teacherNo;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		return "ProfessionCourse{" +
				"courseNo='" + courseNo + '\'' +
				", courseName='" + courseName + '\'' +
				", courseType='" + courseType + '\'' +
				", courseCredit='" + courseCredit + '\'' +
				", courseNum=" + courseNum +
				", gradeNo='" + gradeNo + '\'' +
				", professionNo='" + professionNo + '\'' +
				", teacherNo='" + teacherNo + '\'' +
				", createTime=" + createTime +
				", updateTime=" + updateTime +
				'}';
	}
}
