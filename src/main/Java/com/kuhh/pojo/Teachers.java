package com.kuhh.pojo;

import java.util.Date;

public class Teachers {
	//教师工号
	private String teacherNo;
	//教师姓名
	private String teacherName;
	//性别，1男性，2女性
	private Byte gender;
	//创建时间
	private Date createTime;
	//修改时间
	private Date updateTime;
	
	public Teachers() {
		super();
	}
	public Teachers(String teacherNo, String teacherName, Byte gender, Date createTime, Date updateTime) {
		super();
		this.teacherNo = teacherNo;
		this.teacherName = teacherName;
		this.gender = gender;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}
	public String getTeacherNo() {
		return teacherNo;
	}
	public void setTeacherNo(String teacherNo) {
		this.teacherNo = teacherNo;
	}
	public String getTeacherName() {
		return teacherName;
	}
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}
	public Byte getGender() {
		return gender;
	}
	public void setGender(Byte gender) {
		this.gender = gender;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	@Override
	public String toString() {
		return "Teachers [teacherNo=" + teacherNo + ", teacherName=" + teacherName + ", gender=" + gender
				+ ", createTime=" + createTime + ", updateTime=" + updateTime + "]";
	}
	
	
}
