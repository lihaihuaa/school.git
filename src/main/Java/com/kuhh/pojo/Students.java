package com.kuhh.pojo;

import java.util.Date;

public class Students {
	//学生学号
	private String studentNo;
	//学生姓名
	private String studentName;
	//年级No号
	private String gradeNo;
	//学院No号
	private String collegeNo;
	//专业No号
	private String professionNo;
	//班级No号
	private String classNo;
	//个性签名
	private String description;
	//身份证号码
	private String idCard;
	//年龄
	private Integer age;
	//性别，1男性，2女性
	private Byte gender;
	//入学时间
	private String year;
	//创建时间
	private Date createTime;
	//修改时间
	private Date updateTime;
	
	public Students() {
		super();
	}

	public Students(String studentNo, String studentName, String gradeNo, String collegeNo, String professionNo,
			String classNo, String description, String idCard, Integer age, Byte gender, String year, Date createTime,
			Date updateTime) {
		super();
		this.studentNo = studentNo;
		this.studentName = studentName;
		this.gradeNo = gradeNo;
		this.collegeNo = collegeNo;
		this.professionNo = professionNo;
		this.classNo = classNo;
		this.description = description;
		this.idCard = idCard;
		this.age = age;
		this.gender = gender;
		this.year = year;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

    public String getStudentNo() {
		return studentNo;
	}

	public void setStudentNo(String studentNo) {
		this.studentNo = studentNo;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getGradeNo() {
		return gradeNo;
	}

	public void setGradeNo(String gradeNo) {
		this.gradeNo = gradeNo;
	}

	public String getCollegeNo() {
		return collegeNo;
	}

	public void setCollegeNo(String collegeNo) {
		this.collegeNo = collegeNo;
	}

	public String getProfessionNo() {
		return professionNo;
	}

	public void setProfessionNo(String professionNo) {
		this.professionNo = professionNo;
	}

	public String getClassNo() {
		return classNo;
	}

	public void setClassNo(String classNo) {
		this.classNo = classNo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Byte getGender() {
		return gender;
	}

	public void setGender(Byte gender) {
		this.gender = gender;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		return "Students [studentNo=" + studentNo + ", studentName=" + studentName + ", gradeNo=" + gradeNo
				+ ", collegeNo=" + collegeNo + ", professionNo=" + professionNo + ", classNo=" + classNo
				+ ", description=" + description + ", idCard=" + idCard + ", age=" + age + ", gender=" + gender
				+ ", year=" + year + ", createTime=" + createTime + ", updateTime=" + updateTime + "]";
	}
	
	
}
