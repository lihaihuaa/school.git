package com.kuhh.pojo;

import java.util.Date;

public class Scores {
    //序号
    private Integer id;
    //课程号
    private String courseNo;
    //学生学号
    private String studentNo;
    //课程分数
    private float score;
    //课程绩点
    private Integer gradePoint;
    //创建时间
    private Date createTime;
    //修改时间
    private Date updateTime;

    public Scores() {
    }

    public Scores(Integer id, String courseNo, String studentNo, float score, Integer gradePoint, Date createTime, Date updateTime) {
        this.id = id;
        this.courseNo = courseNo;
        this.studentNo = studentNo;
        this.score = score;
        this.gradePoint = gradePoint;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCourseNo() {
        return courseNo;
    }

    public void setCourseNo(String courseNo) {
        this.courseNo = courseNo;
    }

    public String getStudentNo() {
        return studentNo;
    }

    public void setStudentNo(String studentNo) {
        this.studentNo = studentNo;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public Integer getGradePoint() {
        return gradePoint;
    }

    public void setGradePoint(Integer gradePoint) {
        this.gradePoint = gradePoint;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "Scores{" +
                "id=" + id +
                ", courseNo='" + courseNo + '\'' +
                ", studentNo='" + studentNo + '\'' +
                ", score=" + score +
                ", gradePoint=" + gradePoint +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
