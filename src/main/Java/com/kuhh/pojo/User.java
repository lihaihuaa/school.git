package com.kuhh.pojo;

import java.util.Date;

public class User {
	private Integer userId;
	
	private Byte userType;
	
	private String username;
	
	private String password;
	
	private String psd;
	
	private String studentTeacherNo;
	
	private String displayName;
	
	private Byte state;
	
	private Date createTime;
	
	private Date updateTime;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Byte getUserType() {
		return userType;
	}

	public void setUserType(Byte userType) {
		this.userType = userType;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStudentTeacherNo() {
		return studentTeacherNo;
	}

	public void setStudentTeacherNo(String studentTeacherNo) {
		this.studentTeacherNo = studentTeacherNo;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Byte getState() {
		return state;
	}

	public void setState(Byte state) {
		this.state = state;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getPsd() {
		return psd;
	}

	public void setPsd(String psd) {
		this.psd = psd;
	}
	
	@Override
	public String toString() {
		return "User [userId=" + userId + ", userType=" + userType + ", username=" + username + ", password=" + password
				+ ", psd=" + psd + ", studentTeacherNo=" + studentTeacherNo + ", displayName=" + displayName + ", state=" + state
				+ ", createTime=" + createTime + ", updateTime=" + updateTime + "]";
	}

	public User() {
		super();
		// TODO 自动生成的构造函数存根
	}

	public User(Integer userId, Byte userType, String username, String password, String psd, String studentTeacherNo,
			String displayName, Byte state, Date createTime, Date updateTime) {
		super();
		this.userId = userId;
		this.userType = userType;
		this.username = username;
		this.password = password;
		this.psd = psd;
		this.studentTeacherNo = studentTeacherNo;
		this.displayName = displayName;
		this.state = state;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}
}
