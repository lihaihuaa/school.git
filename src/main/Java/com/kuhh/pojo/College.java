package com.kuhh.pojo;

import java.util.Date;

public class College {
	//学院No号
	private String collegeNo;
	//学院名字
	private String collegeName;
	//创建时间
	private Date createTime;
	//修改时间
	private Date updateTime;
	
	public College() {
		super();
	}

	public College(String collegeNo, String collegeName, Date createTime, Date updateTime) {
		super();
		this.collegeNo = collegeNo;
		this.collegeName = collegeName;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	public String getCollegeNo() {
		return collegeNo;
	}

	public void setCollegeNo(String collegeNo) {
		this.collegeNo = collegeNo;
	}

	public String getCollegeName() {
		return collegeName;
	}

	public void setCollegeName(String collegeName) {
		this.collegeName = collegeName;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		return "College [collegeNo=" + collegeNo + ", collegeName=" + collegeName + ", createTime=" + createTime
				+ ", updateTime=" + updateTime + "]";
	}
	
	
}
