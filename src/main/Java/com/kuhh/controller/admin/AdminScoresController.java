package com.kuhh.controller.admin;

import com.kuhh.pojo.*;
import com.kuhh.service.IPageBeanService;
import com.kuhh.service.IProfessionCourseService;
import com.kuhh.service.IProfessionService;
import com.kuhh.service.IStudentsService;
import com.kuhh.service.impl.PageBeanServiceImpl;
import com.kuhh.service.impl.ProfessionCourseServiceImpl;
import com.kuhh.service.impl.ProfessionServiceImpl;
import com.kuhh.service.impl.StudentsServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/admin/adminScoresUrl")
public class AdminScoresController extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private IPageBeanService pageBeanService = new PageBeanServiceImpl();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String currentPage = request.getParameter("currentPage");
        String rows = request.getParameter("rows");
        if(currentPage == null || "".equals(currentPage)){
            currentPage = "1";
        }
        if(rows == null || "".equals(rows)){
            rows = "5";
        }

        List<Scores> scores = pageBeanService.findScoresByPage(currentPage,rows);
        PageBean<Scores> bean = pageBeanService.getScoresByPage(currentPage,rows);

        IProfessionCourseService professionCourseService = new ProfessionCourseServiceImpl();
        List<ProfessionCourse> courses = professionCourseService.getProfessionCourseList();
        IStudentsService studentsService = new StudentsServiceImpl();
        List<Students> students = studentsService.getStudentList();
        IProfessionService professionService = new ProfessionServiceImpl();
        List<Profession> professions = professionService.getProfessionList();

        request.setAttribute("rows", rows);
        request.setAttribute("pb", bean);
        request.setAttribute("scores", scores);
        request.setAttribute("professions", professions);
        request.setAttribute("courses", courses);
        request.setAttribute("students", students);
        request.getRequestDispatcher("/WEB-INF/pages/admin/admin-scores.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

}
