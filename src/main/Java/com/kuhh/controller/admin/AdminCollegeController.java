package com.kuhh.controller.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kuhh.pojo.College;
import com.kuhh.pojo.PageBean;
import com.kuhh.service.IPageBeanService;
import com.kuhh.service.impl.PageBeanServiceImpl;

@WebServlet("/admin/adminCollegeUrl")
public class AdminCollegeController extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
	private IPageBeanService pageBeanService = new PageBeanServiceImpl();
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String currentPage = request.getParameter("currentPage");
        String rows = request.getParameter("rows");
        if(currentPage == null || "".equals(currentPage)){
            currentPage = "1";
        }
        if(rows == null || "".equals(rows)){
            rows = "5";
        }
        
        List<College> colleges = pageBeanService.findCollegeByPage(currentPage,rows);
        PageBean<College> bean = pageBeanService.getCollegeByPage(currentPage,rows);
        
        request.setAttribute("rows", rows);
        request.setAttribute("pb", bean);
	    request.setAttribute("colleges", colleges);
		request.getRequestDispatcher("/WEB-INF/pages/admin/admin-college.jsp").forward(request, response);
        
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
