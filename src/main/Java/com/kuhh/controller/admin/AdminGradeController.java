package com.kuhh.controller.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kuhh.pojo.Grade;
import com.kuhh.pojo.PageBean;
import com.kuhh.pojo.Teachers;
import com.kuhh.service.IPageBeanService;
import com.kuhh.service.ITeachersService;
import com.kuhh.service.impl.PageBeanServiceImpl;
import com.kuhh.service.impl.TeachersServiceImpl;

@WebServlet("/admin/adminGradeUrl")
public class AdminGradeController extends HttpServlet{

	private static final long serialVersionUID = 1L;
	private IPageBeanService pageBeanService = new PageBeanServiceImpl();
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String currentPage = request.getParameter("currentPage");
        String rows = request.getParameter("rows");
        if(currentPage == null || "".equals(currentPage)){
            currentPage = "1";
        }
        if(rows == null || "".equals(rows)){
            rows = "5";
        }
        
        List<Grade> grades= pageBeanService.findGradeByPage(currentPage,rows);
		PageBean<Grade> bean = pageBeanService.getGradeByPage(currentPage,rows);
    	
    	ITeachersService teachersService = new TeachersServiceImpl();
    	List<Teachers> teachers = teachersService.getTeacherList();
    	
    	request.setAttribute("rows", rows);
        request.setAttribute("pb", bean);
		request.setAttribute("grades", grades);
		request.setAttribute("teachers", teachers);
		request.getRequestDispatcher("/WEB-INF/pages/admin/admin-grade.jsp").forward(request, response);
    	
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}