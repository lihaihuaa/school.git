package com.kuhh.controller.admin;

import com.kuhh.pojo.PageBean;
import com.kuhh.pojo.Teachers;
import com.kuhh.service.IPageBeanService;
import com.kuhh.service.impl.PageBeanServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/admin/adminTeachersUrl")
public class AdminTeachersController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private IPageBeanService pageBeanService = new PageBeanServiceImpl();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String currentPage = request.getParameter("currentPage");
        String rows = request.getParameter("rows");
        if(currentPage == null || "".equals(currentPage)){
            currentPage = "1";
        }
        if(rows == null || "".equals(rows)){
            rows = "5";
        }

        List<Teachers> teachers = pageBeanService.findTeachersByPage(currentPage, rows);
        PageBean<Teachers> bean = pageBeanService.getTeachersByPage(currentPage, rows);

        request.setAttribute("rows", rows);
        request.setAttribute("pb", bean);
        request.setAttribute("teachers", teachers);
        request.getRequestDispatcher("/WEB-INF/pages/admin/admin-teachers.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
