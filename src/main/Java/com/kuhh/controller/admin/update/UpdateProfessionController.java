package com.kuhh.controller.admin.update;

import com.kuhh.pojo.Profession;
import com.kuhh.service.IProfessionService;
import com.kuhh.service.impl.ProfessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@WebServlet("/admin/updateProfession")
public class UpdateProfessionController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String professionNo = request.getParameter("professionNo");
        String professionName = request.getParameter("professionName");
        String collegeNo = request.getParameter("collegeNo");
        Date updateTime = new Date();
        Profession profession = new Profession(professionNo, professionName, collegeNo, null, updateTime);

        String msg = "";
        String path = "/admin/adminProfessionUrl";
        IProfessionService professionService = new ProfessionServiceImpl();
        try {
            professionService.updateProfession(profession);
        } catch (Exception e) {
            e.printStackTrace();
            msg = e.getMessage();
            path = "/WEB-INF/pages/error.jsp";
            request.setAttribute("msg", msg);
        }
        request.getRequestDispatcher(path).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
