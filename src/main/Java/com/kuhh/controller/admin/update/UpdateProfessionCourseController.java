package com.kuhh.controller.admin.update;

import com.kuhh.pojo.ProfessionCourse;
import com.kuhh.service.IProfessionCourseService;
import com.kuhh.service.impl.ProfessionCourseServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@WebServlet("/admin/updateProfessionCourse")
public class UpdateProfessionCourseController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String courseNo = request.getParameter("courseNo");
        String courseName = request.getParameter("courseName");
        String courseType = request.getParameter("courseType");
        String courseCredit = request.getParameter("courseCredit");
        String courseNum = request.getParameter("courseNum");
        String gradeNo = request.getParameter("gradeNo");
        String professionNo = request.getParameter("professionNo");
        String teacherNo = request.getParameter("teacherNo");
        Date updateTime = new Date();
        ProfessionCourse professionCourse = new ProfessionCourse(courseNo, courseName, courseType,courseCredit, Integer.parseInt(courseNum), gradeNo, professionNo, teacherNo, null, updateTime);

        String msg = "";
        String path = "/admin/adminProfessionCourseUrl";
        IProfessionCourseService professionCourseService = new ProfessionCourseServiceImpl();
        try {
            professionCourseService.updateProfessionCourse(professionCourse);
        } catch (Exception e) {
            e.printStackTrace();
            msg = e.getMessage();
            path = "/WEB-INF/pages/error.jsp";
            request.setAttribute("msg", msg);
        }
        request.getRequestDispatcher(path).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
