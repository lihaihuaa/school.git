package com.kuhh.controller.admin.update;

import com.kuhh.pojo.Scores;
import com.kuhh.service.IScoresService;
import com.kuhh.service.impl.ScoresServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@WebServlet("/admin/updateScores")
public class UpdateScoresController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        String courseNo = request.getParameter("courseNo");
        String studentNo = request.getParameter("studentNo");
        Float score = Float.valueOf(request.getParameter("score"));
        Float s = score / 10;
        Integer gradePoint = s.intValue();
        Date updateTime = new Date();
        Scores scores = new Scores(Integer.parseInt(id), courseNo, studentNo, score, gradePoint, null, updateTime);

        String msg = "";
        String path = "/admin/adminScoresUrl";
        IScoresService scoresService = new ScoresServiceImpl();
        try {
            scoresService.updateScore(scores);
        } catch (Exception e) {
            e.printStackTrace();
            msg = e.getMessage();
            path = "/WEB-INF/pages/error.jsp";
            request.setAttribute("msg", msg);
        }
        request.getRequestDispatcher(path).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
