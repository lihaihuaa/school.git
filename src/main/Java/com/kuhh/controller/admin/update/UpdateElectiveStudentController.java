package com.kuhh.controller.admin.update;

import com.kuhh.pojo.ElectiveStudent;
import com.kuhh.pojo.ProfessionCourse;
import com.kuhh.pojo.Students;
import com.kuhh.service.IElectiveStudentService;
import com.kuhh.service.impl.ElectiveStudentServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@WebServlet("/admin/updateElectiveStudent")
public class UpdateElectiveStudentController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        String professionNo = request.getParameter("professionNo");
        String courseNo = request.getParameter("courseNo");
        String studentNo = request.getParameter("studentNo");
        Date updateTime = new Date();
        ElectiveStudent electiveStudent = new ElectiveStudent(Integer.parseInt(id),professionNo, courseNo, studentNo, null, updateTime);

        IElectiveStudentService electiveStudentService = new ElectiveStudentServiceImpl();
        ElectiveStudent electiveStudent2 = electiveStudentService.getElectiveStudentById(Integer.parseInt(id));
        List<ProfessionCourse> courseList = electiveStudentService.getElectiveStudentListByStudentNo(studentNo);
        List<Students> studentsList = electiveStudentService.getElectiveStudentListByCourseNo(courseNo);

        if(electiveStudent2 == null){
            request.setAttribute("msg","选课ID为"+id+"的选课信息不存在");
            request.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(request, response);
            return;
        }
        if(courseList != null && studentsList != null){
            boolean flag = false;
            for (ProfessionCourse course:
                 courseList) {
                if (course.getCourseNo() != electiveStudent2.getCourseNo()){
                    flag = true;
                }
            }
            for (Students student:
                    studentsList) {
                if (student.getStudentNo() != electiveStudent2.getStudentNo()){
                    flag = true;
                }
            }
            if (flag){
                request.setAttribute("msg","该选课信息已存在");
                request.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(request, response);
            }
        }else {
            electiveStudentService.updateElectiveStudent(electiveStudent);
            request.getRequestDispatcher("/admin/adminElectiveStudentUrl").forward(request, response);
        }
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
