package com.kuhh.controller.admin.update;

import com.kuhh.pojo.ProfessionClass;
import com.kuhh.service.IProfessionClassService;
import com.kuhh.service.impl.ProfessionClassServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@WebServlet("/admin/updateProfessionClass")
public class UpdateProfessionClassController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String classNo = request.getParameter("classNo");
        String className = request.getParameter("className");
        String professionNo = request.getParameter("professionNo");
        String teacherNo = request.getParameter("teacherNo");
        Date updateTime = new Date();
        ProfessionClass professionClass = new ProfessionClass(classNo, className, professionNo, teacherNo, null, updateTime);

        String msg = "";
        String path = "/admin/adminProfessionClassUrl";
        IProfessionClassService professionClassService = new ProfessionClassServiceImpl();
        try {
            professionClassService.updateProfessionClass(professionClass);
        } catch (Exception e) {
            e.printStackTrace();
            msg = e.getMessage();
            path = "/WEB-INF/pages/error.jsp";
            request.setAttribute("msg", msg);
        }
        request.getRequestDispatcher(path).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
