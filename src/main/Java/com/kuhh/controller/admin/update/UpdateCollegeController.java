package com.kuhh.controller.admin.update;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kuhh.pojo.College;
import com.kuhh.service.ICollegeService;
import com.kuhh.service.impl.CollegeServiceImpl;

@WebServlet("/admin/updateCollege")
public class UpdateCollegeController extends HttpServlet{

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String collegeNo = request.getParameter("collegeNo");
		String collegeName = request.getParameter("collegeName");
		Date updateTime = new Date();
		College college = new College();
		college.setCollegeNo(collegeNo);
		college.setCollegeName(collegeName);
		college.setUpdateTime(updateTime);

		String msg = "";
		String path = "/admin/adminCollegeUrl";
		ICollegeService collegeService = new CollegeServiceImpl();
		try {
			collegeService.updateCollege(college);
		} catch (Exception e) {
			e.printStackTrace();
			msg = e.getMessage();
			path = "/WEB-INF/pages/error.jsp";
			request.setAttribute("msg", msg);
		}
		request.getRequestDispatcher(path).forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
