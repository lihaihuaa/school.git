package com.kuhh.controller.admin.update;

import com.kuhh.pojo.Students;
import com.kuhh.service.IStudentsService;
import com.kuhh.service.impl.StudentsServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@WebServlet("/admin/updateStudents")
public class UpdateStudentsController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String studentNo = request.getParameter("studentNo");
        String studentName = request.getParameter("studentName");
        String gradeNo = request.getParameter("gradeNo");
        String collegeNo = request.getParameter("collegeNo");
        String professionNo = request.getParameter("professionNo");
        String classNo = request.getParameter("classNo");
        String idCard = request.getParameter("idCard");
        Byte gender = Byte.parseByte(request.getParameter("gender"));
        String year = request.getParameter("year");
        Integer age = Integer.valueOf(request.getParameter("age"));
        Date updateTime = new Date();
        Students students = new Students(studentNo, studentName, gradeNo, collegeNo, professionNo, classNo, null, idCard,age, gender, year, null, updateTime);

        String msg = "";
        String path = "/admin/adminStudentsUrl";
        IStudentsService studentsService = new StudentsServiceImpl();
        try {
            studentsService.updateStudent(students);
        } catch (Exception e) {
            e.printStackTrace();
            msg = e.getMessage();
            path = "/WEB-INF/pages/error.jsp";
            request.setAttribute("msg", msg);
        }
        request.getRequestDispatcher("/admin/adminStudentsUrl").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
