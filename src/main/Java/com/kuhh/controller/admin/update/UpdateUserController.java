package com.kuhh.controller.admin.update;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kuhh.service.IUserService;
import com.kuhh.service.impl.UserServiceImpl;

@WebServlet("/admin/updateUser")
public class UpdateUserController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String userId = request.getParameter("userId");
		String state = request.getParameter("userState");
		IUserService UserService = new UserServiceImpl();
		UserService.changeStatus(Byte.valueOf(state), Integer.parseInt(userId));
		request.getRequestDispatcher("/admin/adminUserUrl").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
