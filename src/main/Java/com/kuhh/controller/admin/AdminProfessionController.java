package com.kuhh.controller.admin;

import com.kuhh.pojo.College;
import com.kuhh.pojo.PageBean;
import com.kuhh.pojo.Profession;
import com.kuhh.service.ICollegeService;
import com.kuhh.service.IPageBeanService;
import com.kuhh.service.impl.CollegeServiceImpl;
import com.kuhh.service.impl.PageBeanServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/admin/adminProfessionUrl")
public class AdminProfessionController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private IPageBeanService pageBeanService = new PageBeanServiceImpl();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String currentPage = request.getParameter("currentPage");
        String rows = request.getParameter("rows");
        if(currentPage == null || "".equals(currentPage)){
            currentPage = "1";
        }
        if(rows == null || "".equals(rows)){
            rows = "5";
        }

        List<Profession> professions = pageBeanService.findProfessionByPage(currentPage,rows);
        PageBean<Profession> bean = pageBeanService.getProfessionByPage(currentPage,rows);

        ICollegeService collegeService = new CollegeServiceImpl();
        List<College> colleges = collegeService.getCollegeList();

        request.setAttribute("rows", rows);
        request.setAttribute("pb", bean);
        request.setAttribute("professions", professions);
        request.setAttribute("colleges", colleges);
        request.getRequestDispatcher("/WEB-INF/pages/admin/admin-profession.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }
}
