package com.kuhh.controller.admin;

import com.kuhh.pojo.*;
import com.kuhh.service.*;
import com.kuhh.service.impl.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/admin/adminStudentsUrl")
public class AdminStudentsController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private IPageBeanService pageBeanService = new PageBeanServiceImpl();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String currentPage = request.getParameter("currentPage");
        String rows = request.getParameter("rows");
        if(currentPage == null || "".equals(currentPage)){
            currentPage = "1";
        }
        if(rows == null || "".equals(rows)){
            rows = "5";
        }

        List<Students> students = pageBeanService.findStudentsByPage(currentPage, rows);
        PageBean<Students> bean = pageBeanService.getStudentsByPage(currentPage, rows);

        IGradeService gradeService = new GradeServiceImpl();
        List<Grade> grades = gradeService.getGradeList();

        ICollegeService collegeService = new CollegeServiceImpl();
        List<College> colleges = collegeService.getCollegeList();

        IProfessionService professionService = new ProfessionServiceImpl();
        List<Profession> professions = professionService.getProfessionList();

        IProfessionClassService professionClassService = new ProfessionClassServiceImpl();
        List<ProfessionClass> classes = professionClassService.getProfessionClassList();

        request.setAttribute("rows", rows);
        request.setAttribute("pb", bean);
        request.setAttribute("students", students);
        request.setAttribute("grades", grades);
        request.setAttribute("colleges", colleges);
        request.setAttribute("professions", professions);
        request.setAttribute("classes", classes);
        request.getRequestDispatcher("/WEB-INF/pages/admin/admin-students.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
