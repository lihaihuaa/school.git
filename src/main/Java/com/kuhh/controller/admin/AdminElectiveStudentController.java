package com.kuhh.controller.admin;

import com.kuhh.pojo.*;
import com.kuhh.service.*;
import com.kuhh.service.impl.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/admin/adminElectiveStudentUrl")
public class AdminElectiveStudentController extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private IPageBeanService pageBeanService = new PageBeanServiceImpl();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String currentPage = request.getParameter("currentPage");
        String rows = request.getParameter("rows");
        if(currentPage == null || "".equals(currentPage)){
            currentPage = "1";
        }
        if(rows == null || "".equals(rows)){
            rows = "5";
        }

        List<ElectiveStudent> electives = pageBeanService.findElectiveStudentByPage(currentPage,rows);
        PageBean<ElectiveStudent> bean = pageBeanService.getElectiveStudentByPage(currentPage,rows);

        IElectiveStudentService electiveStudentService = new ElectiveStudentServiceImpl();
        List<ProfessionCourse> electiveUnNum = electiveStudentService.getElectiveUnNum();
        IProfessionCourseService professionCourseService = new ProfessionCourseServiceImpl();
        List<ProfessionCourse> courses = professionCourseService.getProfessionCourseList();
        IStudentsService studentsService = new StudentsServiceImpl();
        List<Students> students = studentsService.getStudentList();
        IProfessionService professionService = new ProfessionServiceImpl();
        List<Profession> professions = professionService.getProfessionList();

        request.setAttribute("rows", rows);
        request.setAttribute("pb", bean);
        request.setAttribute("electives", electives);
        request.setAttribute("professions", professions);
        request.setAttribute("electiveUnNum",electiveUnNum);
        request.setAttribute("courses",courses);
        request.setAttribute("students", students);
        request.getRequestDispatcher("/WEB-INF/pages/admin/admin-electiveStudent.jsp").forward(request, response);

    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
