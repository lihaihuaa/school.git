package com.kuhh.controller.admin.delete;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kuhh.service.IGradeService;
import com.kuhh.service.impl.GradeServiceImpl;

@WebServlet("/admin/deleteGrade")
public class DeleteGradeController extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String gradeNo = request.getParameter("gradeNo");
		IGradeService gradeService = new GradeServiceImpl();
		gradeService.deleteGradeByNo(gradeNo);
		request.getRequestDispatcher("/admin/adminGradeUrl").forward(request, response);
		
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}
