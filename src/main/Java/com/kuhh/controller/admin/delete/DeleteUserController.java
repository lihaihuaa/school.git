package com.kuhh.controller.admin.delete;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kuhh.service.IUserService;
import com.kuhh.service.impl.UserServiceImpl;

@WebServlet("/admin/deleteUser")
public class DeleteUserController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer userId = Integer.valueOf( request.getParameter("userId"));
		IUserService userService = new UserServiceImpl();
		userService.deleteUser(userId);
		request.getRequestDispatcher("/admin/adminUserUrl").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
