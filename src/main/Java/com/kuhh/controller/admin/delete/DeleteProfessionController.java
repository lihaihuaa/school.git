package com.kuhh.controller.admin.delete;

import com.kuhh.service.IProfessionService;
import com.kuhh.service.impl.ProfessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/admin/deleteProfession")
public class DeleteProfessionController extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String professionNo = request.getParameter("professionNo");
        IProfessionService professionService = new ProfessionServiceImpl();
        professionService.deleteProfessionByNo(professionNo);
        request.getRequestDispatcher("/admin/adminProfessionUrl").forward(request, response);

    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
