package com.kuhh.controller.admin.delete;

import com.kuhh.service.IProfessionCourseService;
import com.kuhh.service.impl.ProfessionCourseServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/admin/deleteProfessionCourse")
public class DeleteProfessionCourseController extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String courseNo = request.getParameter("courseNo");
        IProfessionCourseService professionCourseService = new ProfessionCourseServiceImpl();
        professionCourseService.deleteProfessionCourseByNo(courseNo);
        request.getRequestDispatcher("/admin/adminProfessionCourseUrl").forward(request, response);

    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
