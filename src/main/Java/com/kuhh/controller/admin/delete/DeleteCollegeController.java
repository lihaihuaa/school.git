package com.kuhh.controller.admin.delete;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kuhh.service.ICollegeService;
import com.kuhh.service.impl.CollegeServiceImpl;

@WebServlet("/admin/deleteCollege")
public class DeleteCollegeController extends HttpServlet{

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String collegeNo = request.getParameter("collegeNo");
		ICollegeService collegeService = new CollegeServiceImpl();
		collegeService.deleteCollegeByNo(collegeNo);
		request.getRequestDispatcher("/admin/adminCollegeUrl").forward(request, response);
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
