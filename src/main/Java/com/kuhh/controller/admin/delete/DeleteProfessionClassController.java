package com.kuhh.controller.admin.delete;

import com.kuhh.service.IProfessionClassService;
import com.kuhh.service.impl.ProfessionClassServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/admin/deleteProfessionClass")
public class DeleteProfessionClassController extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String classNo = request.getParameter("classNo");
        IProfessionClassService professionClassService = new ProfessionClassServiceImpl();
        professionClassService.deleteProfessionClassByNo(classNo);
        request.getRequestDispatcher("/admin/adminProfessionClassUrl").forward(request, response);

    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
