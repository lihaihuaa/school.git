package com.kuhh.controller.admin.delete;

import com.kuhh.service.IScoresService;
import com.kuhh.service.impl.ScoresServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/admin/deleteScores")
public class DeleteScoresController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        IScoresService scoresService = new ScoresServiceImpl();
        scoresService.deleteScore(Integer.parseInt(id));
        request.getRequestDispatcher("/admin/adminScoresUrl").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
