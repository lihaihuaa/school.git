package com.kuhh.controller.admin.add;

import com.kuhh.pojo.Teachers;
import com.kuhh.service.ITeachersService;
import com.kuhh.service.impl.TeachersServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@WebServlet("/admin/addTeachers")
public class AddTeachersController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String teacherNo = request.getParameter("teacherNo");
        String teacherName = request.getParameter("teacherName");
        Byte gender = Byte.valueOf(request.getParameter("gender"));
        Date createTime = new Date();
        Date updateTime = new Date();
        Teachers teachers = new Teachers(teacherNo, teacherName, gender, createTime, updateTime);

        String msg = "";
        String path = "/admin/adminTeachersUrl";
        ITeachersService teachersService = new TeachersServiceImpl();
        try {
            teachersService.addTeacher(teachers);
        } catch (Exception e) {
            e.printStackTrace();
            msg = e.getMessage();
            path = "/WEB-INF/pages/error.jsp";
            request.setAttribute("msg", msg);
        }
        request.getRequestDispatcher(path).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
