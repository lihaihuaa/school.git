package com.kuhh.controller.admin.add;

import com.kuhh.pojo.ProfessionCourse;
import com.kuhh.pojo.Scores;
import com.kuhh.pojo.Students;
import com.kuhh.service.IElectiveStudentService;
import com.kuhh.service.IProfessionCourseService;
import com.kuhh.service.IScoresService;
import com.kuhh.service.IStudentsService;
import com.kuhh.service.impl.ElectiveStudentServiceImpl;
import com.kuhh.service.impl.ProfessionCourseServiceImpl;
import com.kuhh.service.impl.ScoresServiceImpl;
import com.kuhh.service.impl.StudentsServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@WebServlet("/admin/addScores")
public class AddScoresController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String courseNo = request.getParameter("courseNo");
        String studentNo = request.getParameter("studentNo");
        Float score = Float.valueOf(request.getParameter("score"));
        Float s = score / 10;
        Integer gradePoint = s.intValue();
        Date createTime = new Date();
        Date updateTime = new Date();
        Scores scores = new Scores(null, courseNo, studentNo, score, gradePoint, createTime, updateTime);
        IScoresService scoresService = new ScoresServiceImpl();
        List<Scores> scoreListByCourseNo = scoresService.getScoreListByCourseNo(courseNo);
        List<Scores> scoreListByStudentNo = scoresService.getScoreListByStudentNo(studentNo);

        IElectiveStudentService electiveStudentService = new ElectiveStudentServiceImpl();
        List<Students> studentListByCourseNo = electiveStudentService.getElectiveStudentListByCourseNo(courseNo);
        List<ProfessionCourse> coursesListByStudentNo = electiveStudentService.getElectiveStudentListByStudentNo(studentNo);

        if (scoreListByCourseNo.size() != 0 && scoreListByStudentNo.size() != 0){
            request.setAttribute("msg","课程号为"+courseNo+"和学生学号为"+studentNo+"的成绩信息已存在");
            request.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(request, response);
        }else{
            if (studentListByCourseNo.size() != 0 && coursesListByStudentNo.size() != 0){
                IProfessionCourseService professionCourseService = new ProfessionCourseServiceImpl();
                ProfessionCourse course = professionCourseService.getProfessionCourseByNo(courseNo);
                IStudentsService studentsService = new StudentsServiceImpl();
                Students student = studentsService.getStudentByNo(studentNo);
                if (Integer.parseInt(course.getGradeNo())>Integer.parseInt(student.getGradeNo())){
                    request.setAttribute("msg","学生学号为"+studentNo+"的所在当前年级及以下年级还未包含课程号为"+courseNo+"的课程");
                    request.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(request, response);
                    return;
                }
                if(student!=null && student.getProfessionNo()!=null && !student.getProfessionNo().equals(course.getProfessionNo())){
                    request.setAttribute("msg","学生学号为"+studentNo+"和课程号为"+courseNo+"的所在专业不对付");
                    request.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(request, response);
                    return;
                }
            }
            scoresService.addScore(scores);
            request.getRequestDispatcher("/admin/adminScoresUrl").forward(request, response);
        }


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
