package com.kuhh.controller.admin.add;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kuhh.pojo.User;
import com.kuhh.service.IUserService;
import com.kuhh.service.impl.UserServiceImpl;
import com.kuhh.utils.EncryptUtils;

@WebServlet("/admin/addUser")
public class AddUserController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String usertype = request.getParameter("usertype");
		String studentNo = request.getParameter("studentNo");
		String teacherNo = request.getParameter("teacherNo");
		String password = request.getParameter("password");

		User user = new User();
		if("2".equals(usertype)){
			user.setUsername(studentNo);
			user.setStudentTeacherNo(studentNo);
			user.setUserType((byte)2);
		}
		if("3".equals(usertype)){
			user.setUsername(teacherNo);
			user.setStudentTeacherNo(teacherNo);
			user.setUserType((byte)3);
		}
		user.setPassword(EncryptUtils.MD5Encode(password));
		user.setPsd(password);
		user.setState((byte)1);
		user.setCreateTime(new Date());
		user.setUpdateTime(new Date());

		IUserService userService = new UserServiceImpl();
		if("2".equals(usertype)){
			userService.addUser(user);
		}
		if("3".equals(usertype)){
			userService.addUserTeacher(user);
		}
		request.getRequestDispatcher("/admin/adminUserUrl").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
