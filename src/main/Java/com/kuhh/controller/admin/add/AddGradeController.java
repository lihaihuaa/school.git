package com.kuhh.controller.admin.add;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kuhh.pojo.Grade;
import com.kuhh.service.IGradeService;
import com.kuhh.service.impl.GradeServiceImpl;

@WebServlet("/admin/addGrade")
public class AddGradeController extends HttpServlet{

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String gradeNo = request.getParameter("gradeNo");
		String gradeName = request.getParameter("gradeName");
		String teacherNo = request.getParameter("teacherNo");
		Date createTime = new Date();
		Date updateTime = new Date();
		Grade grade = new Grade(gradeNo,gradeName,teacherNo,createTime,updateTime);

		String msg = "";
		String path = "/admin/adminGradeUrl";
		IGradeService gradeService = new GradeServiceImpl();
		try {
			gradeService.addGrade(grade);
		} catch (Exception e) {
			e.printStackTrace();
			msg = e.getMessage();
			path = "/WEB-INF/pages/error.jsp";
			request.setAttribute("msg", msg);
		}
		request.getRequestDispatcher(path).forward(request, response);
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
