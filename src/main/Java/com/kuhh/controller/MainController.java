package com.kuhh.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kuhh.pojo.ProfessionCourse;
import com.kuhh.pojo.Scores;
import com.kuhh.pojo.Students;
import com.kuhh.pojo.Teachers;
import com.kuhh.service.*;
import com.kuhh.service.impl.*;

/**
 * 跳转到主界面
 * @author Administrator
 */
@WebServlet("/mainUrl")
public class MainController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String courseNo = request.getParameter("courseNo");
		String top = request.getParameter("top");
		IScoresService scoresService = new ScoresServiceImpl();
		List<Scores> scoresList = scoresService.getCourseTopScoreList(courseNo,top);

		IProfessionCourseService professionCourseService = new ProfessionCourseServiceImpl();
		List<ProfessionCourse> courseList = professionCourseService.getProfessionCourseList();

		IStudentsService studentsService = new StudentsServiceImpl();
		List<Students> studentList = studentsService.getStudentList();

		ITeachersService teachersService = new TeachersServiceImpl();
		List<Teachers> teacherList = teachersService.getTeacherList();
		int teacherNum = teacherList.size();
		int studentNum = studentList.size();
		int courseNum = courseList.size();
		int onlineNum = (int) request.getServletContext().getAttribute("onlineNum");
		long startTime = (long) request.getServletContext().getAttribute("startTime");
		int visits = (int) request.getServletContext().getAttribute("visits");
		int days = (int)((System.currentTimeMillis() - startTime) / (1000*3600*24)) + 1;
		
		request.setAttribute("visits", visits);
		request.setAttribute("teacherNum", teacherNum);
		request.setAttribute("studentNum", studentNum);
		request.setAttribute("courseNum", courseNum);
		request.setAttribute("onlineNums", onlineNum);
		request.setAttribute("days", days);
		request.setAttribute("scores", scoresList);
		request.setAttribute("courses", courseList);
		request.setAttribute("students", studentList);
		request.setAttribute("top", top);
		request.setAttribute("courseNo", courseNo);
		
		request.getRequestDispatcher("/WEB-INF/pages/main.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
