package com.kuhh.controller.user.teacher;

import com.kuhh.pojo.*;
import com.kuhh.service.*;
import com.kuhh.service.impl.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/user/teacher/electiveUrl")
public class ElectiveController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = (User) request.getSession().getAttribute("loginUser");
        IElectiveStudentService electiveStudentService = new ElectiveStudentServiceImpl();
        List<ElectiveStudent> electives = electiveStudentService.getElectiveListByTeacherNo(user.getStudentTeacherNo());
        IProfessionService professionService = new ProfessionServiceImpl();
        List<Profession> professionList = professionService.getProfessionList();
        IProfessionCourseService professionCourseService = new ProfessionCourseServiceImpl();
        List<ProfessionCourse> courseList = professionCourseService.getProfessionCourseList();
        IStudentsService studentsService = new StudentsServiceImpl();
        List<Students> studentList = studentsService.getStudentList();

        request.setAttribute("electives", electives);
        request.setAttribute("professionList", professionList);
        request.setAttribute("courseList", courseList);
        request.setAttribute("studentList", studentList);
        request.getRequestDispatcher("/WEB-INF/pages/user/teacher/elective.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
