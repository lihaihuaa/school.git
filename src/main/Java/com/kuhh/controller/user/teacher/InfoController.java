package com.kuhh.controller.user.teacher;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kuhh.pojo.Teachers;
import com.kuhh.pojo.User;
import com.kuhh.service.ITeachersService;
import com.kuhh.service.impl.TeachersServiceImpl;

@WebServlet("/user/teacher/infoUrl")
public class InfoController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User) request.getSession().getAttribute("loginUser");
		ITeachersService teachersService = new TeachersServiceImpl();
		Teachers teacher = teachersService.getTeacherByTeacherNo(user.getStudentTeacherNo());
		request.setAttribute("teacher", teacher);
		request.getRequestDispatcher("/WEB-INF/pages/user/teacher/info.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
