package com.kuhh.controller.user.teacher;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kuhh.pojo.*;
import com.kuhh.service.*;
import com.kuhh.service.impl.*;

@WebServlet("/user/teacher/scoreUrl")
public class ScoreController extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User) request.getSession().getAttribute("loginUser");
		IScoresService scoresService = new ScoresServiceImpl();
		List<Scores> scores = scoresService.getScoreListByTeacherNo(user.getStudentTeacherNo());
		List<Object> beforeScoreNum = scoresService.getScoreRanking(scores);
		ITeachersService teachersService = new TeachersServiceImpl();
		List<Teachers> teachers = teachersService.getTeacherList();
		IStudentsService studentsService = new StudentsServiceImpl();
		List<Students> students = studentsService.getStudentList();
		IProfessionCourseService professionCourseService = new ProfessionCourseServiceImpl();
		List<ProfessionCourse> courses = professionCourseService.getProfessionCourseList();

		request.setAttribute("scores", scores);
		request.setAttribute("beforeScoreNum", beforeScoreNum);
		request.setAttribute("courses", courses);
		request.setAttribute("teachers", teachers);
		request.setAttribute("students", students);
		request.getRequestDispatcher("/WEB-INF/pages/user/teacher/score.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
