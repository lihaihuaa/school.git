package com.kuhh.controller.user.teacher;

import com.kuhh.pojo.Profession;
import com.kuhh.pojo.ProfessionClass;
import com.kuhh.pojo.Teachers;
import com.kuhh.pojo.User;
import com.kuhh.service.IProfessionClassService;
import com.kuhh.service.IProfessionService;
import com.kuhh.service.ITeachersService;
import com.kuhh.service.impl.ProfessionClassServiceImpl;
import com.kuhh.service.impl.ProfessionServiceImpl;
import com.kuhh.service.impl.TeachersServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/user/teacher/classUrl")
public class ClassController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = (User) request.getSession().getAttribute("loginUser");
        IProfessionClassService professionClassService = new ProfessionClassServiceImpl();
        List<ProfessionClass> classes = professionClassService.getClassListByTeacherNo(user.getStudentTeacherNo());

        IProfessionService professionService = new ProfessionServiceImpl();
        List<Profession> professions = professionService.getProfessionList();

        ITeachersService teachersService = new TeachersServiceImpl();
        List<Teachers> teachers = teachersService.getTeacherList();

        request.setAttribute("classes", classes);
        request.setAttribute("professions", professions);
        request.setAttribute("teachers", teachers);
        request.getRequestDispatcher("/WEB-INF/pages/user/teacher/class.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
