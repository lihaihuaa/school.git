package com.kuhh.controller.user.teacher;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kuhh.pojo.*;
import com.kuhh.service.*;
import com.kuhh.service.impl.*;

@WebServlet("/user/teacher/courseUrl")
public class CourseController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User) request.getSession().getAttribute("loginUser");
		IProfessionCourseService professionCourseService = new ProfessionCourseServiceImpl();
		List<ProfessionCourse> courses = professionCourseService.getCourseByTeacherNo(user.getStudentTeacherNo());
		IGradeService gradeService = new GradeServiceImpl();
		List<Grade> grades = gradeService.getGradeList();
		IProfessionService professionService = new ProfessionServiceImpl();
		List<Profession> professions = professionService.getProfessionList();
		ITeachersService teachersService = new TeachersServiceImpl();
		List<Teachers> teachers = teachersService.getTeacherList();

		request.setAttribute("courses", courses);
		request.setAttribute("grades", grades);
		request.setAttribute("professions", professions);
		request.setAttribute("teachers", teachers);
		request.getRequestDispatcher("/WEB-INF/pages/user/teacher/course.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
