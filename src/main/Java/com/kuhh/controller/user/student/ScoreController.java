package com.kuhh.controller.user.student;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kuhh.pojo.ProfessionCourse;
import com.kuhh.pojo.Scores;
import com.kuhh.pojo.Teachers;
import com.kuhh.pojo.User;
import com.kuhh.service.IProfessionCourseService;
import com.kuhh.service.IScoresService;
import com.kuhh.service.ITeachersService;
import com.kuhh.service.impl.ProfessionCourseServiceImpl;
import com.kuhh.service.impl.ScoresServiceImpl;
import com.kuhh.service.impl.TeachersServiceImpl;

@WebServlet("/user/student/scoreUrl")
public class ScoreController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = (User) request.getSession().getAttribute("loginUser");
		IScoresService scoresService = new ScoresServiceImpl();
		List<Scores> scores = scoresService.getScoreListByStudentNo(user.getStudentTeacherNo());
		List<Object> beforeScoreNum = scoresService.getScoreRanking(scores);
		ITeachersService teachersService = new TeachersServiceImpl();
		List<Teachers> teachers = teachersService.getTeacherList();
		IProfessionCourseService professionCourseService = new ProfessionCourseServiceImpl();
		List<ProfessionCourse> courses = professionCourseService.getProfessionCourseList();

		request.setAttribute("scores", scores);
		request.setAttribute("beforeScoreNum", beforeScoreNum);
		request.setAttribute("courses", courses);
		request.setAttribute("teachers", teachers);
		request.getRequestDispatcher("/WEB-INF/pages/user/student/score.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
