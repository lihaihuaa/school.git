package com.kuhh.controller.user.student;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kuhh.pojo.Students;
import com.kuhh.pojo.User;
import com.kuhh.service.IStudentsService;
import com.kuhh.service.impl.StudentsServiceImpl;

@WebServlet("/user/student/infoUrl")
public class InfoController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		IStudentsService studentsService = new StudentsServiceImpl();
		User user = (User) request.getSession().getAttribute("loginUser");
		Students student = studentsService.getStudentByNo(user.getStudentTeacherNo());
		request.setAttribute("student", student);
		request.getRequestDispatcher("/WEB-INF/pages/user/student/info.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
