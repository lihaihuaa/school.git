package com.kuhh.controller.user.student;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kuhh.pojo.User;
import com.kuhh.service.IStudentsService;
import com.kuhh.service.impl.StudentsServiceImpl;

@WebServlet("/user/student/updateInfo")
public class UpdateInfoController extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String description = request.getParameter("description");
		User user = (User) request.getSession().getAttribute("loginUser");
		IStudentsService studentsService = new StudentsServiceImpl();
		studentsService.updateStudent(user.getStudentTeacherNo(), description);
		request.getRequestDispatcher("/user/student/infoUrl").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
