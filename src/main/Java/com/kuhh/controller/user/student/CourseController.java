package com.kuhh.controller.user.student;

import com.kuhh.pojo.ProfessionCourse;
import com.kuhh.pojo.Students;
import com.kuhh.pojo.Teachers;
import com.kuhh.pojo.User;
import com.kuhh.service.IElectiveStudentService;
import com.kuhh.service.IProfessionCourseService;
import com.kuhh.service.IStudentsService;
import com.kuhh.service.ITeachersService;
import com.kuhh.service.impl.ElectiveStudentServiceImpl;
import com.kuhh.service.impl.ProfessionCourseServiceImpl;
import com.kuhh.service.impl.StudentsServiceImpl;
import com.kuhh.service.impl.TeachersServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/user/student/courseUrl")
public class CourseController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = (User) request.getSession().getAttribute("loginUser");
        IStudentsService studentsService = new StudentsServiceImpl();
        Students student = studentsService.getStudentByNo(user.getStudentTeacherNo());
        IElectiveStudentService electiveStudentService = new ElectiveStudentServiceImpl();
        List<ProfessionCourse> courseList = electiveStudentService.getElectiveStudentListByStudentNo(user.getStudentTeacherNo());
        IProfessionCourseService professionCourseService = new ProfessionCourseServiceImpl();
        List<ProfessionCourse> courseList2 = professionCourseService.getCourseByGradeNoAndProfessionNo(student.getGradeNo(), student.getProfessionNo());
        List<ProfessionCourse> courses = new ArrayList<>();
        courses.addAll(courseList);
        courses.addAll(courseList2);

        ITeachersService teachersService = new TeachersServiceImpl();
        List<Teachers> teachers = teachersService.getTeacherList();

        request.setAttribute("courses", courses);
        request.setAttribute("teachers", teachers);
        request.getRequestDispatcher("/WEB-INF/pages/user/student/course.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
