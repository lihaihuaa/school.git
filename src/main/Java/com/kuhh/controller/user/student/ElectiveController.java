package com.kuhh.controller.user.student;

import com.kuhh.pojo.*;
import com.kuhh.service.*;
import com.kuhh.service.impl.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/user/student/electiveUrl")
public class ElectiveController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = (User) request.getSession().getAttribute("loginUser");
        IElectiveStudentService electiveStudentService = new ElectiveStudentServiceImpl();
        List<ProfessionCourse> courses = electiveStudentService.getElectiveStudentListByStudentNo(user.getStudentTeacherNo());
        IGradeService gradeService = new GradeServiceImpl();
        List<Grade> gradeList = gradeService.getGradeList();
        IProfessionService professionService = new ProfessionServiceImpl();
        List<Profession> professionList = professionService.getProfessionList();
        ITeachersService teachersService = new TeachersServiceImpl();
        List<Teachers> teacherList = teachersService.getTeacherList();

        request.setAttribute("courses", courses);
        request.setAttribute("gradeList", gradeList);
        request.setAttribute("professionList", professionList);
        request.setAttribute("teacherList", teacherList);
        request.getRequestDispatcher("/WEB-INF/pages/user/student/elective.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
