package com.kuhh.controller.ajax;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuhh.pojo.College;
import com.kuhh.service.ICollegeService;
import com.kuhh.service.impl.CollegeServiceImpl;

@WebServlet("/getCollege")
public class GetCollege extends HttpServlet{

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String collegeNo = request.getParameter("collegeNo");
		ICollegeService collegeService = new CollegeServiceImpl();
		College college = collegeService.getCollegeByNo(collegeNo);

		response.setContentType("text/json; charset=utf-8");
		PrintWriter writer = response.getWriter();
		ObjectMapper objectMapper = new ObjectMapper();
		writer.print(objectMapper.writeValueAsString(college));
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
