package com.kuhh.controller.ajax;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuhh.pojo.ElectiveStudent;
import com.kuhh.pojo.ProfessionCourse;
import com.kuhh.pojo.Students;
import com.kuhh.service.IElectiveStudentService;
import com.kuhh.service.IProfessionCourseService;
import com.kuhh.service.IStudentsService;
import com.kuhh.service.impl.ElectiveStudentServiceImpl;
import com.kuhh.service.impl.ProfessionCourseServiceImpl;
import com.kuhh.service.impl.StudentsServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebServlet("/getElectiveStudent")
public class GetElectiveStudent extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String courseNo = request.getParameter("courseNo");
        String studentNo = request.getParameter("studentNo");
        String id = request.getParameter("id");

        IElectiveStudentService electiveStudentService = new ElectiveStudentServiceImpl();
        List<Students> studentsList = electiveStudentService.getElectiveStudentListByCourseNo(courseNo);
        List<ProfessionCourse> courseList = electiveStudentService.getElectiveStudentListByStudentNo(studentNo);
        IProfessionCourseService professionCourseService = new ProfessionCourseServiceImpl();
        ProfessionCourse course = professionCourseService.getProfessionCourseByNo(courseNo);
        IStudentsService studentsService = new StudentsServiceImpl();
        Students student = studentsService.getStudentByNo(studentNo);
        String professionNo = null;
        if (student != null){
            professionNo = student.getProfessionNo();
        }else if (course != null){
            professionNo = course.getProfessionNo();
        }

        Map<String, Object> map = new HashMap<>();
        map.put("professionNo",professionNo);
        map.put("courseNo",courseNo);
        map.put("studentNum",studentsList.size());
        map.put("studentList",studentsList);
        map.put("studentNo",studentNo);
        map.put("courseNum",courseList.size());
        map.put("courseList",courseList);

        response.setContentType("text/json; charset=utf-8");
        PrintWriter writer = response.getWriter();
        ObjectMapper objectMapper = new ObjectMapper();
        if(id == null || "".equals(id)){
            writer.print(objectMapper.writeValueAsString(map));
        }else{
            ElectiveStudent electiveStudent = electiveStudentService.getElectiveStudentById(Integer.parseInt(id));
            writer.print(objectMapper.writeValueAsString(electiveStudent));
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
