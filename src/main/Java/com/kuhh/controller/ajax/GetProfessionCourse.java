package com.kuhh.controller.ajax;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuhh.pojo.ProfessionCourse;
import com.kuhh.service.IProfessionCourseService;
import com.kuhh.service.impl.ProfessionCourseServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/getProfessionCourse")
public class GetProfessionCourse extends HttpServlet {
    private static final long serialVersionUID = 1L;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String courseNo = request.getParameter("courseNo");
        IProfessionCourseService professionCourseService = new ProfessionCourseServiceImpl();
        ProfessionCourse professionCourse = professionCourseService.getProfessionCourseByNo(courseNo);

        response.setContentType("text/json; charset=UTF-8");
        PrintWriter writer = response.getWriter();
        ObjectMapper objectMapper = new ObjectMapper();
        writer.print(objectMapper.writeValueAsString(professionCourse));
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
