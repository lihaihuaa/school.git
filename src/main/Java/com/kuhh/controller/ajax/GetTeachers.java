package com.kuhh.controller.ajax;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuhh.pojo.Teachers;
import com.kuhh.service.ITeachersService;
import com.kuhh.service.impl.TeachersServiceImpl;

@WebServlet("/getTeachers")
public class GetTeachers extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String teacherNo = request.getParameter("teacherNo");
		ITeachersService teachersService = new TeachersServiceImpl();
		Teachers teacher = teachersService.getTeacherByTeacherNo(teacherNo);
		
		response.setContentType("text/json; charset=utf-8");
		PrintWriter writer = response.getWriter();
		ObjectMapper objectMapper = new ObjectMapper();
		writer.print(objectMapper.writeValueAsString(teacher));
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
