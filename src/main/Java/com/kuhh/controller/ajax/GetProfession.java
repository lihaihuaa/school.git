package com.kuhh.controller.ajax;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuhh.pojo.Profession;
import com.kuhh.service.IProfessionService;
import com.kuhh.service.impl.ProfessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/getProfession")
public class GetProfession extends HttpServlet {
    private static final long serialVersionUID = 1L;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String professionNo = request.getParameter("professionNo");
        IProfessionService professionService = new ProfessionServiceImpl();
        Profession profession = professionService.getProfessionByNo(professionNo);

        response.setContentType("text/json; charset=UTF-8");
        PrintWriter writer = response.getWriter();
        ObjectMapper objectMapper = new ObjectMapper();
        writer.print(objectMapper.writeValueAsString(profession));

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
