package com.kuhh.controller.ajax;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuhh.pojo.ProfessionClass;
import com.kuhh.service.IProfessionClassService;
import com.kuhh.service.impl.ProfessionClassServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/getProfessionClass")
public class GetProfessionClass extends HttpServlet {
    private static final long serialVersionUID = 1L;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String classNo = request.getParameter("classNo");
        IProfessionClassService professionClassService = new ProfessionClassServiceImpl();
        ProfessionClass professionClass = professionClassService.getProfessionClassByNo(classNo);

        response.setContentType("text/json; charset=UTF-8");
        PrintWriter writer = response.getWriter();
        ObjectMapper objectMapper = new ObjectMapper();
        writer.print(objectMapper.writeValueAsString(professionClass));
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
