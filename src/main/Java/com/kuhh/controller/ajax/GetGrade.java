package com.kuhh.controller.ajax;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuhh.pojo.Grade;
import com.kuhh.service.IGradeService;
import com.kuhh.service.impl.GradeServiceImpl;

@WebServlet("/getGrade")
public class GetGrade extends HttpServlet{

	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String gradeNo = request.getParameter("gradeNo");
		IGradeService gradeService = new GradeServiceImpl();
		Grade grade = gradeService.getGradeByNo(gradeNo);
		
		response.setContentType("text/json; charset=utf-8");
		PrintWriter writer = response.getWriter();
		ObjectMapper objectMapper = new ObjectMapper();
		writer.print(objectMapper.writeValueAsString(grade));
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
