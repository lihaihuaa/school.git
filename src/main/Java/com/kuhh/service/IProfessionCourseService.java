package com.kuhh.service;

import com.kuhh.pojo.ProfessionCourse;

import java.util.List;

public interface IProfessionCourseService {
    //获取所有专业课程信息
    List<ProfessionCourse> getProfessionCourseList();
    //根据年级号和专业号，获取符合的所有课程
    List<ProfessionCourse> getCourseByGradeNoAndProfessionNo(String gradeNo,String ProfessionNo);
    //根据课程号，获取专业课程信息
    ProfessionCourse getProfessionCourseByNo(String courseNo);
    //修改专业课程信息
    ProfessionCourse updateProfessionCourse(ProfessionCourse professionCourse);
    //增加专业课程信息
    ProfessionCourse addProfessionCourse(ProfessionCourse professionCourse);
    //根据课程号，删除专业课程信息
    boolean deleteProfessionCourseByNo(String courseNo);
    //根据教师工号，该教师下的所有课程信息
    List<ProfessionCourse> getCourseByTeacherNo(String studentTeacherNo);
}
