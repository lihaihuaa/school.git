package com.kuhh.service;

import java.util.List;

import com.kuhh.pojo.Teachers;

public interface ITeachersService {
	//获取所有老师
	List<Teachers> getTeacherList();
	//根据no教师工号获取老师
	Teachers getTeacherByTeacherNo(String teacherNo);
	//添加老师
	Teachers addTeacher(Teachers teacher);
	//修改老师
	Teachers updateTeacher(Teachers teacher);
	//根据no教师工号删除老师
	boolean deleteTeacher(String teacherNo);
	//获取未注册的老师
	List<Teachers> getTeacherUnRegister();
}
