package com.kuhh.service;

import com.kuhh.pojo.*;

import java.util.List;

public interface IPageBeanService {
    //获取分页内容
    List<Grade> findGradeByPage(String currPage, String pageSize);
    List<College> findCollegeByPage(String currPage, String pageSize);
    List<Profession> findProfessionByPage(String currPage, String pageSize);
    List<ProfessionClass> findProfessionClassByPage(String currPage, String pageSize);
    List<ProfessionCourse> findProfessionCourseByPage(String currPage, String pageSize);
    List<ElectiveStudent> findElectiveStudentByPage(String currPage,String pageSize);
    List<Scores> findScoresByPage(String currPage,String pageSize);
    List<Students> findStudentsByPage(String currPage,String pageSize);
    List<Teachers> findTeachersByPage(String currPage,String pageSize);
    List<User> findUserByPage(String currPage, String pageSize);

    //统计个数
    int getGradeCount();
    int getCollegeCount();
    int getProfessionCount();
    int getProfessionClassCount();
    int getProfessionCourseCount();
    int getElectiveStudentCount();
    int getScoresCount();
    int getStudentsCount();
    int getTeachersCount();
    int getUserCount();

    //分页
    PageBean<Grade> getGradeByPage(String currPage, String pageSize);
    PageBean<College> getCollegeByPage(String currPage, String pageSize);
    PageBean<Profession> getProfessionByPage(String currPage, String pageSize);
    PageBean<ProfessionClass> getProfessionClassByPage(String currPage, String pageSize);
    PageBean<ProfessionCourse> getProfessionCourseByPage(String currPage, String pageSize);
    PageBean<ElectiveStudent> getElectiveStudentByPage(String currPage, String pageSize);
    PageBean<Scores> getScoresByPage(String currPage, String pageSize);
    PageBean<Students> getStudentsByPage(String currPage, String pageSize);
    PageBean<Teachers> getTeachersByPage(String currPage, String pageSize);
    PageBean<User> getUserByPage(String currPage, String pageSize);
}
