package com.kuhh.service;

import java.util.List;

import com.kuhh.pojo.College;

public interface ICollegeService {
	//获取所有学院信息
	List<College> getCollegeList();
	//根据学院no号，获取学院信息
	College getCollegeByNo(String collegeNo);
	//修改学院信息
	College updateCollege(College college);
	//添加学院信息
	College addCollege(College college);
	//根据学院No号，删除学院信息
	boolean deleteCollegeByNo(String collegeNo);
}
