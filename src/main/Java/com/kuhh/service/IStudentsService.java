package com.kuhh.service;

import java.util.List;

import com.kuhh.pojo.Students;

public interface IStudentsService {
	//获取所有学生
	List<Students> getStudentList();
	//通过学生学号获取学生信息
	Students getStudentByNo(String studentNo);
	//添加学生
	Students addStudent(Students student);
	//删除学生
	boolean deleteStudent(String studentNo);
	//修改学生
	Students updateStudent(Students student);
	//获取未注册的学生
	List<Students> getStudentUnRegister();
	//根据学号修改个性签名
	Students updateStudent(String studentNo, String description);
}
