package com.kuhh.service;

import com.kuhh.pojo.ProfessionClass;

import java.util.List;

public interface IProfessionClassService {
    //获取所有的专业班级信息
    List<ProfessionClass> getProfessionClassList();
    //根据班级号，获取专业班级信息
    ProfessionClass getProfessionClassByNo(String classNo);
    //修改专业班级信息
    ProfessionClass updateProfessionClass(ProfessionClass professionClass);
    //增加专业班级信息
    ProfessionClass addProfessionClass(ProfessionClass professionClass);
    //根据班级号，删除专业班级信息
    boolean deleteProfessionClassByNo(String classNo);
    //根据教师工号，获取教师管理的班级信息
    List<ProfessionClass> getClassListByTeacherNo(String studentTeacherNo);
}
