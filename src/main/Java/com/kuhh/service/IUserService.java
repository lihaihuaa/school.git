package com.kuhh.service;

import java.util.List;

import com.kuhh.pojo.User;

//服务层，将几种操作封装起来
public interface IUserService {
	//通过名字密码获取用户
	User getUserByUserNameAndPassword(String username, String password);
	//添加用户
	void addUser(User user);
	//添加用户老师
	void addUserTeacher(User user);
	//删除用户
	void deleteUser(int id);
	//改变状态
	void changeStatus(Byte state, int userId);
	//通过名字获取用户
	User getUserByUserName(String username);
	//登录校验
	User loginCheck(String username, String password);

}
