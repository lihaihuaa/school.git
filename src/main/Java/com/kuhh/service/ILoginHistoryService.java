package com.kuhh.service;

import java.util.List;

import com.kuhh.pojo.LoginHistory;

public interface ILoginHistoryService {
	//添加登录历史
	void addLoginHistory(LoginHistory loginHistory);
	//获取访问次数
	int getVisits();
	//访问次数+1
	void addVisits();
}
