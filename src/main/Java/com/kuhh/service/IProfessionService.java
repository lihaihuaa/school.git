package com.kuhh.service;

import com.kuhh.pojo.Profession;

import java.util.List;

public interface IProfessionService {
    //获取所有专业信息
    List<Profession> getProfessionList();
    //根据专业号，获取专业信息
    Profession getProfessionByNo(String professionNo);
    //修改专业信息
    Profession updateProfession(Profession profession);
    //增加专业信息
    Profession addProfession(Profession profession);
    //根据专业号，删除专业信息
    boolean deleteProfessionByNo(String professionNo);
}
