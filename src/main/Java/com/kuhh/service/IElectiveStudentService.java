package com.kuhh.service;

import com.kuhh.pojo.ElectiveStudent;
import com.kuhh.pojo.ProfessionCourse;
import com.kuhh.pojo.Students;

import java.util.List;

public interface IElectiveStudentService {
    //根据课程号，获取所有选该课的学生
    List<Students> getElectiveStudentListByCourseNo(String courseNo);
    //根据学生号，获取所有已选课的课程
    List<ProfessionCourse> getElectiveStudentListByStudentNo(String studentNo);
    //获取所有的非必修课且人数上限未满的课程
    List<ProfessionCourse> getElectiveUnNum();
    //根据选课id号，获取选课信息
    ElectiveStudent getElectiveStudentById(Integer id);
    //修改选课信息
    ElectiveStudent updateElectiveStudent(ElectiveStudent electiveStudent);
    //添加选课信息
    ElectiveStudent addElectiveStudent(ElectiveStudent electiveStudent);
    //根据选课id号，删除选课信息
    boolean deleteElectiveStudentById(Integer id);
    //根据教师工号，获取选课信息为该教师教学的所有选课信息
    List<ElectiveStudent> getElectiveListByTeacherNo(String studentTeacherNo);
}
