package com.kuhh.service.impl;

import java.util.List;

import com.kuhh.dao.IUserDao;
import com.kuhh.dao.impl.UserDaoImpl;
import com.kuhh.pojo.Students;
import com.kuhh.pojo.Teachers;
import com.kuhh.pojo.User;
import com.kuhh.service.IUserService;
import com.kuhh.utils.EncryptUtils;

public class UserServiceImpl implements IUserService{
	private IUserDao userDao = new UserDaoImpl();

	@Override
	public User loginCheck(String username, String password) {
		User user = getUserByUserNameAndPassword(username, password);
		if (user == null){
			throw new RuntimeException("请检查用户名和密码是否正确！");
		}
		if (user.getState() == 2){
			throw new RuntimeException("该用户名已被封禁！请联系管理员！");
		}
		return user;
	}
	
	@Override
	public User getUserByUserNameAndPassword(String username, String password) {
		return userDao.getUserByUserNameAndPassword(username, EncryptUtils.MD5Encode(password));
	}

	@Override
	public void addUser(User user) {
		StudentsServiceImpl studentsService = new StudentsServiceImpl();
		Students student = studentsService.getStudentByNo(user.getStudentTeacherNo());
		user.setDisplayName(student.getStudentName());
		User res = userDao.getUserByUserName(user.getUsername());
		if(res != null) {
			return;
		}
		userDao.addUser(user);
	}

	@Override
	public void deleteUser(int id) {
		userDao.deleteUser(id);
	}

	@Override
	public void changeStatus(Byte state, int userId) {
		userDao.changeStatus(state, userId);
	}

	@Override
	public User getUserByUserName(String username) {
		return userDao.getUserByUserName(username);
	}

	@Override
	public void addUserTeacher(User user) {
		TeachersServiceImpl teachersService = new TeachersServiceImpl();
		Teachers teacher = teachersService.getTeacherByTeacherNo(user.getStudentTeacherNo());
		user.setDisplayName(teacher.getTeacherName());
		User res = userDao.getUserByUserName(user.getUsername());
		if(res != null) {
			return;
		}
		userDao.addUserTeacher(user);
		
	}

}
