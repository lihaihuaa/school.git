package com.kuhh.service.impl;

import java.util.List;

import com.kuhh.dao.ICollegeDao;
import com.kuhh.dao.impl.CollegeDaoImpl;
import com.kuhh.pojo.College;
import com.kuhh.service.ICollegeService;

public class CollegeServiceImpl implements ICollegeService{

	private ICollegeDao collegeDao = new CollegeDaoImpl();
	
	@Override
	public List<College> getCollegeList() {
		return collegeDao.getCollegeList();
	}

	@Override
	public College getCollegeByNo(String collegeNo) {
		return collegeDao.getCollegeByNo(collegeNo);
	}

	@Override
	public College updateCollege(College college) {
		College college2 = getCollegeByNo(college.getCollegeNo());
		if(college2 == null) {
			throw new RuntimeException("学院号为" + college.getCollegeNo() + "的学院不存在！");

		}
		collegeDao.updateCollege(college);
		return getCollegeByNo(college.getCollegeNo());
	}

	@Override
	public College addCollege(College college) {
		College college2 = getCollegeByNo(college.getCollegeNo());
		if(college2 != null) {
			throw new RuntimeException("学院号为" + college.getCollegeNo() + "的学院已存在！");
		}
		collegeDao.addCollege(college);
		return getCollegeByNo(college.getCollegeNo());
	}

	@Override
	public boolean deleteCollegeByNo(String collegeNo) {
		College college2 = getCollegeByNo(collegeNo);
		if(college2 != null) {
			return collegeDao.deleteCollegeByNo(collegeNo);
		}
		return false;
	}

}
