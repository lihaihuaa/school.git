package com.kuhh.service.impl;

import com.kuhh.dao.IScoresDao;
import com.kuhh.dao.impl.ScoresDaoImpl;
import com.kuhh.pojo.ProfessionCourse;
import com.kuhh.pojo.Scores;
import com.kuhh.pojo.Students;
import com.kuhh.service.IScoresService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ScoresServiceImpl implements IScoresService {
    private IScoresDao scoresDao = new ScoresDaoImpl();

    @Override
    public List<ProfessionCourse> getCoursesByProfessionNo(String professionNo) {
        return scoresDao.getCoursesByProfessionNo(professionNo);
    }

    @Override
    public List<Students> getStudentsByProfessionNo(String professionNo) {
        return scoresDao.getStudentsByProfessionNo(professionNo);
    }

    @Override
    public List<Scores> getScoreListByCourseNo(String courseNo) {
        return scoresDao.getScoreListByCourseNo(courseNo);
    }

    @Override
    public List<Scores> getScoreListByStudentNo(String studentNo) {
        return scoresDao.getScoreListByStudentNo(studentNo);
    }

    @Override
    public List<Scores> getScoreListByTeacherNo(String teacherNo) {
        return scoresDao.getScoreListByTeacherNo(teacherNo);
    }

    @Override
    public List<Object> getScoreRanking(List<Scores> scores) {
        List<Object> list = new ArrayList<>();
        for (Scores score:
             scores) {
            HashMap<String, Object> map = new HashMap<>();
            int rankingNum = scoresDao.getBeforeScoreNum(score.getCourseNo(),score.getScore());
            int num = getScoreListByCourseNo(score.getCourseNo()).size();
            map.put("courseNo",score.getCourseNo());
            map.put("studentNo",score.getStudentNo());
            map.put("ranking",rankingNum+1);
            map.put("num",num);
            list.add(map);
        }
        return list;
    }

    @Override
    public Scores getScoreByCourseNoAndStudentNo(String courseNo, String studentNo) {
        return scoresDao.getScoreByCourseNoAndStudentNo(courseNo,studentNo);
    }

    @Override
    public List<Scores> getCourseTopScoreList(String courseNo, String top) {
        if(top == null || "".equals(top)){
            top = "10";
        }
        if(courseNo == null || "".equals(courseNo)){
            return scoresDao.getTopScoreList(Integer.parseInt(top));
        }
        else {
            return scoresDao.getCourseTopScoreList(courseNo,Integer.parseInt(top));
        }
    }

    @Override
    public Scores getScoreById(Integer id) {
        return scoresDao.getScoreById(id);
    }

    @Override
    public Scores updateScore(Scores scores) {
        Scores scores2 = getScoreById(scores.getId());
        if (scores2 == null){
            throw new RuntimeException("成绩ID为"+scores.getId()+"的成绩信息不存在");
        }
        scoresDao.updateScore(scores);
        return getScoreById(scores.getId());
    }

    @Override
    public Scores addScore(Scores scores) {
        if (scores.getId() == null){
            scoresDao.addScore(scores);
            return scores;
        }
        Scores scores2 = getScoreById(scores.getId());
        if (scores2 == null){
            scoresDao.addScore(scores);
            return getScoreById(scores.getId());
        }
        return null;
    }

    @Override
    public boolean deleteScore(Integer id) {
        Scores scores2 = getScoreById(id);
        if (scores2 != null){
            return scoresDao.deleteScore(id);
        }
        return false;
    }
}
