package com.kuhh.service.impl;

import java.util.List;

import com.kuhh.dao.IStudentsDao;
import com.kuhh.dao.impl.StudentsDaoImpl;
import com.kuhh.pojo.Students;
import com.kuhh.service.IStudentsService;

public class StudentsServiceImpl implements IStudentsService{

	private IStudentsDao studentsDao = new StudentsDaoImpl();
	
	@Override
	public List<Students> getStudentList() {
		return studentsDao.getStudentList();
	}

	@Override
	public Students getStudentByNo(String studentNo) {
		return studentsDao.getStudentByNo(studentNo);
	}

	@Override
	public Students addStudent(Students student) {
		Students student2 = getStudentByNo(student.getStudentNo());
		if(student2 != null) {
			throw new RuntimeException("学生号为"+student.getStudentNo()+"的学生信息已存在！");
		}
		studentsDao.addStudent(student);
		return getStudentByNo(student.getStudentNo());
	}

	@Override
	public boolean deleteStudent(String studentNo) {
		Students student2 = studentsDao.getStudentByNo(studentNo);
		if(student2 != null) {
			studentsDao.deleteStudent(studentNo);
			return true;
		}
		return false;
	}

	@Override
	public Students updateStudent(Students student) {
		Students student2 = getStudentByNo(student.getStudentNo());
		if(student2 == null) {
			throw new RuntimeException("学生号为"+student.getStudentNo()+"的学生信息不存在！");
		}
		studentsDao.updateStudent(student);
		return getStudentByNo(student.getStudentNo());
	}

	@Override
	public List<Students> getStudentUnRegister() {
		return studentsDao.getStudentUnRegister();
	}

	@Override
	public Students updateStudent(String studentNo, String description) {
		Students student2 = studentsDao.getStudentByNo(studentNo);
		if(student2 != null) {
			studentsDao.updateStudent(studentNo, description);
			return studentsDao.getStudentByNo(studentNo);
		}
		return null;
	}

}
