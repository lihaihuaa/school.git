package com.kuhh.service.impl;

import java.util.List;

import com.kuhh.dao.ITeachersDao;
import com.kuhh.dao.impl.TeachersDaoImpl;
import com.kuhh.pojo.Teachers;
import com.kuhh.service.ITeachersService;

public class TeachersServiceImpl implements ITeachersService{
	private ITeachersDao teachersDao = new TeachersDaoImpl(); 
	
	@Override
	public List<Teachers> getTeacherList() {
		return teachersDao.getTeacherList();
	}

	@Override
	public Teachers getTeacherByTeacherNo(String teacherNo) {
		return teachersDao.getTeacherByTeacherNo(teacherNo);
	}

	@Override
	public Teachers addTeacher(Teachers teacher) {
		Teachers teacher2 = getTeacherByTeacherNo(teacher.getTeacherNo());
		if(teacher2 != null) {
			throw new RuntimeException("教师工号为"+teacher.getTeacherNo()+"的教师信息已存在！");
		}
		teachersDao.addTeacher(teacher);
		return getTeacherByTeacherNo(teacher.getTeacherNo());
	}

	@Override
	public Teachers updateTeacher(Teachers teacher) {
		Teachers teacher2 = getTeacherByTeacherNo(teacher.getTeacherNo());
		if(teacher2 == null) {
			throw new RuntimeException("教师工号为"+teacher.getTeacherNo()+"的教师信息不存在！");
		}
		teachersDao.updateTeacher(teacher);
		return getTeacherByTeacherNo(teacher.getTeacherNo());
	}

	@Override
	public boolean deleteTeacher(String teacherNo) {
		Teachers teacher = getTeacherByTeacherNo(teacherNo);
		if(teacher != null) {
			teachersDao.deleteTeacher(teacherNo);
			return true;
		}
		return false;
	}

	@Override
	public List<Teachers> getTeacherUnRegister() {
		return teachersDao.getTeacherUnRegister();
	}

}
