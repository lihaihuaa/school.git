package com.kuhh.service.impl;

import com.kuhh.dao.IProfessionClassDao;
import com.kuhh.dao.impl.ProfessionClassDaoImpl;
import com.kuhh.pojo.ProfessionClass;
import com.kuhh.service.IProfessionClassService;

import java.util.List;

public class ProfessionClassServiceImpl implements IProfessionClassService {
    private IProfessionClassDao professionClassDao = new ProfessionClassDaoImpl();

    @Override
    public List<ProfessionClass> getProfessionClassList() {
        return professionClassDao.getProfessionClassList();
    }

    @Override
    public ProfessionClass getProfessionClassByNo(String classNo) {
        return professionClassDao.getProfessionClassByNo(classNo);
    }

    @Override
    public ProfessionClass updateProfessionClass(ProfessionClass professionClass) {
        ProfessionClass professionClass2 = getProfessionClassByNo(professionClass.getClassNo());
        if (professionClass2 == null){
            throw new RuntimeException("班级号为"+professionClass.getClassNo()+"的班级不存在");
        }
        professionClassDao.updateProfessionClass(professionClass);
        return getProfessionClassByNo(professionClass.getClassNo());
    }

    @Override
    public ProfessionClass addProfessionClass(ProfessionClass professionClass) {
        ProfessionClass professionClass2 = getProfessionClassByNo(professionClass.getClassNo());
        if (professionClass2 != null){
            throw new RuntimeException("班级号为"+professionClass.getClassNo()+"的班级已存在");
        }
        professionClassDao.addProfessionClass(professionClass);
        return getProfessionClassByNo(professionClass.getClassNo());
    }

    @Override
    public boolean deleteProfessionClassByNo(String classNo) {
        ProfessionClass professionClass = getProfessionClassByNo(classNo);
        if (professionClass != null){
            return professionClassDao.deleteProfessionClassByNo(classNo);
        }
        return false;
    }

    @Override
    public List<ProfessionClass> getClassListByTeacherNo(String studentTeacherNo) {
        return professionClassDao.getClassListByTeacherNo(studentTeacherNo);
    }
}
