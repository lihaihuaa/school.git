package com.kuhh.service.impl;

import com.kuhh.dao.IProfessionDao;
import com.kuhh.dao.impl.ProfessionDaoImpl;
import com.kuhh.pojo.Profession;
import com.kuhh.service.IProfessionService;

import java.util.List;

public class ProfessionServiceImpl implements IProfessionService {
    private IProfessionDao professionDao = new ProfessionDaoImpl();
    @Override
    public List<Profession> getProfessionList() {
        return professionDao.getProfessionList();
    }

    @Override
    public Profession getProfessionByNo(String professionNo) {
        return professionDao.getProfessionByNo(professionNo);
    }

    @Override
    public Profession updateProfession(Profession profession) {
        Profession profession2 = getProfessionByNo(profession.getProfessionNo());
        if (profession2 == null){
            throw new RuntimeException("专业号为"+profession.getProfessionNo()+"的专业不存在");
        }
        professionDao.updateProfession(profession);
        return getProfessionByNo(profession.getProfessionNo());
    }

    @Override
    public Profession addProfession(Profession profession) {
        Profession profession2 = getProfessionByNo(profession.getProfessionNo());
        if (profession2 != null){
            throw new RuntimeException("专业号为"+profession.getProfessionNo()+"的专业已经存在");

        }
        professionDao.addProfession(profession);
        return getProfessionByNo(profession.getProfessionNo());
    }

    @Override
    public boolean deleteProfessionByNo(String professionNo) {
        Profession profession = getProfessionByNo(professionNo);
        if (profession != null){
            return professionDao.deleteProfession(professionNo);
        }
        return false;
    }
}
