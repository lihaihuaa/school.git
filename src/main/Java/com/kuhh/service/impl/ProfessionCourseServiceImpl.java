package com.kuhh.service.impl;

import com.kuhh.dao.IProfessionCourseDao;
import com.kuhh.dao.impl.ProfessionCourseDaoImpl;
import com.kuhh.pojo.ProfessionCourse;
import com.kuhh.service.IProfessionCourseService;

import java.util.ArrayList;
import java.util.List;

public class ProfessionCourseServiceImpl implements IProfessionCourseService {
    private IProfessionCourseDao professionCourseDao = new ProfessionCourseDaoImpl();
    @Override
    public List<ProfessionCourse> getProfessionCourseList() {
        return professionCourseDao.getProfessionCourseList();
    }

    @Override
    public List<ProfessionCourse> getCourseByGradeNoAndProfessionNo(String gradeNo, String ProfessionNo) {
        List<ProfessionCourse> courses = professionCourseDao.getCourseByProfessionNo(ProfessionNo);
        List<ProfessionCourse> courseList = new ArrayList<>();
        for (ProfessionCourse course:
                courses) {
            if (Integer.parseInt(course.getGradeNo()) <= Integer.parseInt(gradeNo)){
                courseList.add(course);
            }
        }
        return courseList;
    }

    @Override
    public ProfessionCourse getProfessionCourseByNo(String courseNo) {
        return professionCourseDao.getProfessionCourseByNo(courseNo);
    }

    @Override
    public ProfessionCourse updateProfessionCourse(ProfessionCourse professionCourse) {
        ProfessionCourse professionCourse2 = getProfessionCourseByNo(professionCourse.getCourseNo());
        if (professionCourse2 == null){
            throw new RuntimeException("课程号为"+professionCourse.getCourseNo()+"的课程不存在");
        }
        professionCourseDao.updateProfessionCourse(professionCourse);
        return getProfessionCourseByNo(professionCourse.getCourseNo());
    }

    @Override
    public ProfessionCourse addProfessionCourse(ProfessionCourse professionCourse) {
        ProfessionCourse professionCourse2 = getProfessionCourseByNo(professionCourse.getCourseNo());
        if (professionCourse2 != null){
            throw new RuntimeException("课程号为"+professionCourse.getCourseNo()+"的课程已存在");
        }
        professionCourseDao.addProfessionCourse(professionCourse);
        return getProfessionCourseByNo(professionCourse.getCourseNo());
    }

    @Override
    public boolean deleteProfessionCourseByNo(String courseNo) {
        ProfessionCourse professionCourse = getProfessionCourseByNo(courseNo);
        if (professionCourse !=null){
            return professionCourseDao.deleteProfessionCourseByNo(courseNo);
        }
        return false;
    }

    @Override
    public List<ProfessionCourse> getCourseByTeacherNo(String studentTeacherNo) {
        return professionCourseDao.getCourseByTeacherNo(studentTeacherNo);
    }
}
