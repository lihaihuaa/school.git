package com.kuhh.service.impl;

import java.util.List;

import com.kuhh.dao.IGradeDao;
import com.kuhh.dao.impl.GradeDaoImpl;
import com.kuhh.pojo.Grade;
import com.kuhh.service.IGradeService;

public class GradeServiceImpl implements IGradeService{
	private IGradeDao gradeDao = new GradeDaoImpl();
	
	@Override
	public List<Grade> getGradeList() {
		return gradeDao.getGradeList();
	}

	@Override
	public Grade getGradeByNo(String gradeNo) {
		return gradeDao.getGradeByNo(gradeNo);
	}

	@Override
	public Grade addGrade(Grade grade) {
		Grade grade2 = getGradeByNo(grade.getGradeNo());
		if(grade2 != null) {
			throw new RuntimeException("年级号为"+grade.getGradeNo()+"的年级已经存在");
		}
		gradeDao.addGrade(grade);
		return getGradeByNo(grade.getGradeNo());
	}

	@Override
	public Grade updateGrade(Grade grade) {
		Grade grade2 = getGradeByNo(grade.getGradeNo());
		if(grade2 == null) {
			throw new RuntimeException("年级号为"+grade.getGradeNo()+"的年级不存在");
		}
		gradeDao.updateGrade(grade);
		return getGradeByNo(grade.getGradeNo());
	}

	@Override
	public boolean deleteGradeByNo(String gradeNo) {
		Grade grade2 = getGradeByNo(gradeNo);
		if(grade2 != null) {
			return gradeDao.deleteGradeByNo(gradeNo);
		}
		return false;
	}

}