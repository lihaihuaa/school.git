package com.kuhh.service.impl;

import com.kuhh.dao.IElectiveStudentDao;
import com.kuhh.dao.impl.ElectiveStudentDaoImpl;
import com.kuhh.pojo.ElectiveStudent;
import com.kuhh.pojo.ProfessionCourse;
import com.kuhh.pojo.Students;
import com.kuhh.service.IElectiveStudentService;

import java.util.List;

public class ElectiveStudentServiceImpl implements IElectiveStudentService {
    private IElectiveStudentDao electiveStudentDao = new ElectiveStudentDaoImpl();

    @Override
    public List<Students> getElectiveStudentListByCourseNo(String courseNo) {
        return electiveStudentDao.getElectiveStudentListByCourseNo(courseNo);
    }

    @Override
    public List<ProfessionCourse> getElectiveStudentListByStudentNo(String studentNo) {
        return electiveStudentDao.getElectiveStudentListByStudentNo(studentNo);
    }

    @Override
    public List<ProfessionCourse> getElectiveUnNum() {
        return electiveStudentDao.getElectiveUnNum();
    }

    @Override
    public ElectiveStudent getElectiveStudentById(Integer id) {
        return electiveStudentDao.getElectiveStudentById(id);
    }

    @Override
    public ElectiveStudent updateElectiveStudent(ElectiveStudent electiveStudent) {
        ElectiveStudent electiveStudent2 = getElectiveStudentById(electiveStudent.getId());
        if (electiveStudent2 != null){
            electiveStudentDao.updateElectiveStudent(electiveStudent);
            return getElectiveStudentById(electiveStudent.getId());
        }
        return null;
    }

    @Override
    public ElectiveStudent addElectiveStudent(ElectiveStudent electiveStudent) {
        ElectiveStudent electiveStudent2 = getElectiveStudentById(electiveStudent.getId());
        if (electiveStudent2 == null){
            electiveStudentDao.addElectiveStudent(electiveStudent);
            return getElectiveStudentById(electiveStudent.getId());
        }
        return null;
    }

    @Override
    public boolean deleteElectiveStudentById(Integer id) {
        ElectiveStudent electiveStudent2 = getElectiveStudentById(id);
        if (electiveStudent2 != null){
            return electiveStudentDao.deleteElectiveStudentById(id);
        }
        return false;
    }

    @Override
    public List<ElectiveStudent> getElectiveListByTeacherNo(String studentTeacherNo) {
        return electiveStudentDao.getElectiveListByTeacherNo(studentTeacherNo);
    }
}
