package com.kuhh.service.impl;

import java.util.List;

import com.kuhh.dao.ILoginHistoryDao;
import com.kuhh.dao.impl.LoginHistoryDaoImpl;
import com.kuhh.pojo.LoginHistory;
import com.kuhh.service.ILoginHistoryService;

public class LoginHistoryServiceImpl implements ILoginHistoryService{
	private ILoginHistoryDao loginHistoryDao = new LoginHistoryDaoImpl();
	
	@Override
	public void addLoginHistory(LoginHistory loginHistory) {
		loginHistoryDao.addLoginHistory(loginHistory);
	}

	@Override
	public int getVisits() {
		return loginHistoryDao.getVisits();
	}

	@Override
	public void addVisits() {
		loginHistoryDao.addVisits();
	}

}
