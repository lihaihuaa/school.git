package com.kuhh.service.impl;

import com.kuhh.dao.IPageBeanDao;
import com.kuhh.dao.impl.PageBeanDaoImpl;
import com.kuhh.pojo.*;
import com.kuhh.service.IPageBeanService;

import java.util.List;

public class PageBeanServiceImpl implements IPageBeanService {
    private IPageBeanDao pageBeanDao = new PageBeanDaoImpl();
    @Override
    public List<Grade> findGradeByPage(String currPage, String pageSize) {
        return pageBeanDao.findGradeByPage(Integer.parseInt(currPage),Integer.parseInt(pageSize));
    }

    @Override
    public List<College> findCollegeByPage(String currPage, String pageSize) {
        return pageBeanDao.findCollegeByPage(Integer.parseInt(currPage),Integer.parseInt(pageSize));
    }

    @Override
    public List<Profession> findProfessionByPage(String currPage, String pageSize) {
        return pageBeanDao.findProfessionByPage(Integer.parseInt(currPage),Integer.parseInt(pageSize));
    }

    @Override
    public List<ProfessionClass> findProfessionClassByPage(String currPage, String pageSize) {
        return pageBeanDao.findProfessionClassByPage(Integer.parseInt(currPage),Integer.parseInt(pageSize));
    }

    @Override
    public List<ProfessionCourse> findProfessionCourseByPage(String currPage, String pageSize) {
        return pageBeanDao.findProfessionCourseByPage(Integer.parseInt(currPage),Integer.parseInt(pageSize));
    }

    @Override
    public List<ElectiveStudent> findElectiveStudentByPage(String currPage, String pageSize) {
        return pageBeanDao.findElectiveStudentByPage(Integer.parseInt(currPage),Integer.parseInt(pageSize));
    }

    @Override
    public List<Scores> findScoresByPage(String currPage, String pageSize) {
        return pageBeanDao.findScoresByPage(Integer.parseInt(currPage),Integer.parseInt(pageSize));
    }

    @Override
    public List<Students> findStudentsByPage(String currPage, String pageSize) {
        return pageBeanDao.findStudentsByPage(Integer.parseInt(currPage),Integer.parseInt(pageSize));
    }

    @Override
    public List<Teachers> findTeachersByPage(String currPage, String pageSize) {
        return pageBeanDao.findTeachersByPage(Integer.parseInt(currPage),Integer.parseInt(pageSize));
    }

    @Override
    public List<User> findUserByPage(String currPage, String pageSize) {
        return pageBeanDao.findUserByPage(Integer.parseInt(currPage),Integer.parseInt(pageSize));
    }

    @Override
    public int getGradeCount() {
        return pageBeanDao.getGradeCount();
    }

    @Override
    public int getCollegeCount() {
        return pageBeanDao.getCollegeCount();
    }

    @Override
    public int getProfessionCount() {
        return pageBeanDao.getProfessionCount();
    }

    @Override
    public int getProfessionClassCount() {
        return pageBeanDao.getProfessionClassCount();
    }

    @Override
    public int getProfessionCourseCount() {
        return pageBeanDao.getProfessionCourseCount();
    }

    @Override
    public int getElectiveStudentCount() {
        return pageBeanDao.getElectiveStudentCount();
    }

    @Override
    public int getScoresCount() {
        return pageBeanDao.getScoresCount();
    }

    @Override
    public int getStudentsCount() {
        return pageBeanDao.getStudentsCount();
    }

    @Override
    public int getTeachersCount() {
        return pageBeanDao.getTeachersCount();
    }

    @Override
    public int getUserCount() {
        return pageBeanDao.getUserCount();
    }

    @Override
    public PageBean<Grade> getGradeByPage(String currPage, String pageSize) {
        List<Grade> list = findGradeByPage(currPage,pageSize);
        int totalCount = getGradeCount();
        PageBean<Grade> pageBean = new PageBean<>();
        pageBean.setList(list);
        pageBean.setTotalCount(totalCount);
        pageBean.setCurrPage(Integer.parseInt(currPage));
        pageBean.setPageSize(Integer.parseInt(pageSize));
        return pageBean;
    }

    @Override
    public PageBean<College> getCollegeByPage(String currPage, String pageSize) {
        List<College> list = findCollegeByPage(currPage, pageSize);
        int totalCount = getCollegeCount();
        PageBean<College> pageBean = new PageBean<>();
        pageBean.setList(list);
        pageBean.setTotalCount(totalCount);
        pageBean.setCurrPage(Integer.parseInt(currPage));
        pageBean.setPageSize(Integer.parseInt(pageSize));
        return pageBean;
    }

    @Override
    public PageBean<Profession> getProfessionByPage(String currPage, String pageSize) {
        List<Profession> list = findProfessionByPage(currPage, pageSize);
        int totalCount = getProfessionCount();
        PageBean<Profession> pageBean = new PageBean<>();
        pageBean.setList(list);
        pageBean.setTotalCount(totalCount);
        pageBean.setCurrPage(Integer.parseInt(currPage));
        pageBean.setPageSize(Integer.parseInt(pageSize));
        return pageBean;
    }

    @Override
    public PageBean<ProfessionClass> getProfessionClassByPage(String currPage, String pageSize) {
        List<ProfessionClass> list = findProfessionClassByPage(currPage, pageSize);
        int totalCount = getProfessionClassCount();
        PageBean<ProfessionClass> pageBean = new PageBean<>();
        pageBean.setList(list);
        pageBean.setTotalCount(totalCount);
        pageBean.setCurrPage(Integer.parseInt(currPage));
        pageBean.setPageSize(Integer.parseInt(pageSize));
        return pageBean;
    }

    @Override
    public PageBean<ProfessionCourse> getProfessionCourseByPage(String currPage, String pageSize) {
        List<ProfessionCourse> list = findProfessionCourseByPage(currPage, pageSize);
        int totalCount = getProfessionCourseCount();
        PageBean<ProfessionCourse> pageBean = new PageBean<>();
        pageBean.setList(list);
        pageBean.setTotalCount(totalCount);
        pageBean.setCurrPage(Integer.parseInt(currPage));
        pageBean.setPageSize(Integer.parseInt(pageSize));
        return pageBean;
    }

    @Override
    public PageBean<ElectiveStudent> getElectiveStudentByPage(String currPage, String pageSize) {
        List<ElectiveStudent> list = findElectiveStudentByPage(currPage, pageSize);
        int totalCount = getElectiveStudentCount();
        PageBean<ElectiveStudent> pageBean = new PageBean<>();
        pageBean.setList(list);
        pageBean.setTotalCount(totalCount);
        pageBean.setCurrPage(Integer.parseInt(currPage));
        pageBean.setPageSize(Integer.parseInt(pageSize));
        return pageBean;
    }

    @Override
    public PageBean<Scores> getScoresByPage(String currPage, String pageSize) {
        List<Scores> list = findScoresByPage(currPage, pageSize);
        int totalCount = getScoresCount();
        PageBean<Scores> pageBean = new PageBean<>();
        pageBean.setList(list);
        pageBean.setTotalCount(totalCount);
        pageBean.setCurrPage(Integer.parseInt(currPage));
        pageBean.setPageSize(Integer.parseInt(pageSize));
        return pageBean;
    }

    @Override
    public PageBean<Students> getStudentsByPage(String currPage, String pageSize) {
        List<Students> list = findStudentsByPage(currPage, pageSize);
        int totalCount = getStudentsCount();
        PageBean<Students> pageBean = new PageBean<>();
        pageBean.setList(list);
        pageBean.setTotalCount(totalCount);
        pageBean.setCurrPage(Integer.parseInt(currPage));
        pageBean.setPageSize(Integer.parseInt(pageSize));
        return pageBean;
    }

    @Override
    public PageBean<Teachers> getTeachersByPage(String currPage, String pageSize) {
        List<Teachers> list = findTeachersByPage(currPage, pageSize);
        int totalCount = getTeachersCount();
        PageBean<Teachers> pageBean = new PageBean<>();
        pageBean.setList(list);
        pageBean.setTotalCount(totalCount);
        pageBean.setCurrPage(Integer.parseInt(currPage));
        pageBean.setPageSize(Integer.parseInt(pageSize));
        return pageBean;
    }

    @Override
    public PageBean<User> getUserByPage(String currPage, String pageSize) {
        List<User> list = findUserByPage(currPage, pageSize);
        int totalCount = getUserCount();
        PageBean<User> pageBean = new PageBean<>();
        pageBean.setList(list);
        pageBean.setTotalCount(totalCount);
        pageBean.setCurrPage(Integer.parseInt(currPage));
        pageBean.setPageSize(Integer.parseInt(pageSize));
        return pageBean;
    }

}
