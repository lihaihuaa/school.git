package com.kuhh.dao;

import java.util.List;

import com.kuhh.pojo.*;

public interface IPageBeanDao {
	//获取分页内容
	List<Grade> findGradeByPage(int currPage,int pageSize);
	List<College> findCollegeByPage(int currPage,int pageSize);
	List<Profession> findProfessionByPage(int currPage, int pageSize);
	List<ProfessionClass> findProfessionClassByPage(int currPage,int pageSize);
	List<ProfessionCourse> findProfessionCourseByPage(int currPage,int pageSize);
	List<ElectiveStudent> findElectiveStudentByPage(int currPage,int pageSize);
	List<Scores> findScoresByPage(int currPage,int pageSize);
	List<Students> findStudentsByPage(int currPage, int pageSize);
	List<Teachers> findTeachersByPage(int currPage, int pageSize);
	List<User> findUserByPage(int currPage, int pageSize);

	//统计个数
	int getGradeCount();
	int getCollegeCount();
	int getProfessionCount();
	int getProfessionClassCount();
	int getProfessionCourseCount();
	int getElectiveStudentCount();
	int getScoresCount();
	int getStudentsCount();
	int getTeachersCount();
	int getUserCount();
}
