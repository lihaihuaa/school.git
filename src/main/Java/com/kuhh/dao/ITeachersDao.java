package com.kuhh.dao;

import java.util.List;

import com.kuhh.pojo.Teachers;

public interface ITeachersDao {
	//获取所有教师
	List<Teachers> getTeacherList();
	//根据教师工号获取老师信息
	Teachers getTeacherByTeacherNo(String teacherNo);
	//添加老师
	boolean addTeacher(Teachers teacher);
	//修改老师
	boolean updateTeacher(Teachers teacher);
	//根据教师工号删除老师
	boolean deleteTeacher(String teacherNo);
	//根据user表和teachers获取未注册老师
	List<Teachers> getTeacherUnRegister();
}
