package com.kuhh.dao;

import java.util.List;

import com.kuhh.pojo.Grade;

public interface IGradeDao {
	//获取所有年级信息
	List<Grade> getGradeList();
	//根据年级no号，获取年级信息
	Grade getGradeByNo(String gradeNo);
	//添加年级
	boolean addGrade(Grade grade);
	//修改年级信息
	boolean updateGrade(Grade grade);
	//根据年级No号，删除年级信息
	boolean deleteGradeByNo(String gradeNo);
}
