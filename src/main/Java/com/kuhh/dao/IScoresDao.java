package com.kuhh.dao;

import com.kuhh.pojo.ProfessionCourse;
import com.kuhh.pojo.Scores;
import com.kuhh.pojo.Students;

import java.util.List;

public interface IScoresDao {
    //获取指定专业号下，所有的课程信息
    List<ProfessionCourse> getCoursesByProfessionNo(String professionNo);
    //获取指定专业号下，所有的学生信息
    List<Students> getStudentsByProfessionNo(String professionNo);
    //根据课程号，查询所有该课程下的学生成绩信息
    List<Scores> getScoreListByCourseNo(String courseNo);
    //根据学生号，查询所有该学生下的课程成绩信息
    List<Scores> getScoreListByStudentNo(String studentNo);
    //根据教师工号，查询所有该学生下的课程成绩信息
    List<Scores> getScoreListByTeacherNo(String teacherNo);
    //根据课程号和学号获取成绩
    Scores getScoreByCourseNoAndStudentNo(String courseNo, String studentNo);
    //获取所有课程的排名前top的学霸
    List<Scores> getTopScoreList(Integer top);
    //获取某课程下的排名前top的学霸
    List<Scores> getCourseTopScoreList(String courseNo,Integer top);
    //根据成绩序号id，查询该成绩的信息
    Scores getScoreById(Integer id);
    //修改成绩分数信息
    boolean updateScore(Scores scores);
    //添加成绩分数信息
    boolean addScore(Scores scores);
    //根据成绩序号id，删除该成绩的信息
    boolean deleteScore(Integer id);
    //根据课程号和分数统计人数
    int getBeforeScoreNum(String courseNo, float score);
}
