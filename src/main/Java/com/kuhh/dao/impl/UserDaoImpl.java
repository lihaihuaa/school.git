package com.kuhh.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.kuhh.dao.IUserDao;
import com.kuhh.pojo.User;
import com.kuhh.utils.JdbcUtils;

public class UserDaoImpl implements IUserDao{

	private Connection con = null;
	private PreparedStatement pre = null;
	private ResultSet resultSet = null;
	@Override
	public User getUserByUserNameAndPassword(String username, String password) {
		try {
			con = JdbcUtils.getConnection();
			String sql = "select * from tb_user where username = ? and password = ?";
			pre = con.prepareStatement(sql);
			pre.setString(1, username);
			pre.setString(2, password);
			resultSet = pre.executeQuery();
			while (resultSet.next()) {
				Integer userId = resultSet.getInt("user_id");
				String psd = resultSet.getString("psd");
				Byte userType = resultSet.getByte("user_type");
				Byte state = resultSet.getByte("state");
				Date createTime = resultSet.getDate("create_time");
				Date updateTime = resultSet.getDate("update_time");
				String displayName = resultSet.getString("display_name");
				String studentTeacherNo = resultSet.getString("student_teacher_no");
				User user = new User();

				user.setUserId(userId);
				user.setUserType(userType);
				user.setPassword(password);
				user.setPsd(psd);
				user.setUsername(username);
				user.setState(state);
				user.setDisplayName(displayName);
				user.setCreateTime(createTime);
				user.setUpdateTime(updateTime);
				user.setStudentTeacherNo(studentTeacherNo);
				return user;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JdbcUtils.close(con, pre, resultSet);
		}
		return null;
	}

	@Override
	public void addUser(User user) {
		// TODO 自动生成的方法存根

		try {
			con = JdbcUtils.getConnection();
			String sql = "insert into tb_user(user_type, username, password,psd, student_teacher_no, state, create_time, update_time, display_name) "
					+ "values(?, ?, ?,?, ?, ?, ? , ?, ?)";
			pre = con.prepareStatement(sql);
		    pre.setInt(1, user.getUserType());
		    pre.setString(2, user.getUsername());
		    pre.setString(3, user.getPassword());
		    pre.setString(4, user.getPsd());
		    pre.setString(5, user.getStudentTeacherNo());
		    pre.setByte(6, user.getState());
		    pre.setDate(7, new java.sql.Date(user.getCreateTime().getTime()));
		    pre.setDate(8, new java.sql.Date(user.getUpdateTime().getTime()));
		    pre.setString(9, user.getDisplayName());

		    pre.execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JdbcUtils.close(con, pre, resultSet);
		}
	}

	@Override
	public void deleteUser(int id) {
		// TODO 自动生成的方法存根
		try {
			con = JdbcUtils.getConnection();
			String sql = "delete from tb_user where user_id = ? and username != 'admin'";
			pre = con.prepareStatement(sql);
		    pre.setInt(1, id);
		
		    pre.execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JdbcUtils.close(con, pre, resultSet);
		}
	}

	@Override
	public void changeStatus(Byte state, int userId) {
		// TODO 自动生成的方法存根
		try {
			con = JdbcUtils.getConnection();
			String sql = "update tb_user set state = ? where user_id = ?";
			pre = con.prepareStatement(sql);
		    pre.setByte(1, state);
		    pre.setInt(2, userId);
		    
		    pre.execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JdbcUtils.close(con, pre, resultSet);
		}
	}

	@Override
	public User getUserByUserName(String username) {
		// TODO 自动生成的方法存根
		try {
			con = JdbcUtils.getConnection();
			String sql = "select * from tb_user where username = ?";
			pre = con.prepareStatement(sql);
			pre.setString(1, username);
			resultSet = pre.executeQuery();
			while (resultSet.next()) {
				Integer userId = resultSet.getInt("user_id");

				Byte userType = resultSet.getByte("user_type");
				Byte state = resultSet.getByte("state");
				Date createTime = resultSet.getDate("create_time");
				Date updateTime = resultSet.getDate("update_time");
				String displayName = resultSet.getString("display_name");
				String password = resultSet.getString("password");
				String psd = resultSet.getString("psd");
				User user = new User();

				user.setUserId(userId);
				user.setUserType(userType);
				user.setPassword(password);
				user.setPsd(psd);
				user.setUsername(username);
				user.setDisplayName(displayName);
				user.setState(state);
				user.setCreateTime(createTime);
				user.setUpdateTime(updateTime);
				return user;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JdbcUtils.close(con, pre, resultSet);
		}
		return null;
	}

	@Override
	public void addUserTeacher(User user) {
		try {
			con = JdbcUtils.getConnection();
			String sql = "insert into tb_user(user_type, username, password,psd, student_teacher_no, state, create_time, update_time, display_name) "
					+ "values(?, ?, ?,?, ?, ?, ? , ?, ?)";
			pre = con.prepareStatement(sql);
		    pre.setInt(1, user.getUserType());
		    pre.setString(2, user.getUsername());
		    pre.setString(3, user.getPassword());
		    pre.setString(4, user.getPsd());
		    pre.setString(5, user.getStudentTeacherNo());
		    pre.setByte(6, user.getState());
		    pre.setDate(7, new java.sql.Date(user.getCreateTime().getTime()));
		    pre.setDate(8, new java.sql.Date(user.getUpdateTime().getTime()));
		    pre.setString(9, user.getDisplayName());

		    pre.execute();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JdbcUtils.close(con, pre, resultSet);
		}
		
	}

}
