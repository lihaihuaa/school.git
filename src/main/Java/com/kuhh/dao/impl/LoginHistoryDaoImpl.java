package com.kuhh.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.kuhh.dao.ILoginHistoryDao;
import com.kuhh.pojo.LoginHistory;
import com.kuhh.utils.JdbcUtils;

public class LoginHistoryDaoImpl implements ILoginHistoryDao{

	private Connection con = null;
	private PreparedStatement pre = null;
	private ResultSet resultSet = null;
	@Override
	public void addLoginHistory(LoginHistory loginHistory) {
		// TODO 自动生成的方法存根
		try {
			con = JdbcUtils.getConnection();
			String sql = "insert into tb_login_history(user_id, ip, create_time) values(?, ?, ?)";
			pre = con.prepareStatement(sql);
			pre.setInt(1, loginHistory.getUserId());
			pre.setString(2, loginHistory.getIp());
			pre.setDate(3, new java.sql.Date(loginHistory.getCreateTime().getTime()));
			pre.execute();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JdbcUtils.close(con, pre, resultSet);
		}
	}

	@Override
	public int getVisits() {
		// TODO 自动生成的方法存根
		try {
			con = JdbcUtils.getConnection();
			String sql = "select * from tb_login_history where id = 1";
			pre = con.prepareStatement(sql);
			resultSet = pre.executeQuery();
			while (resultSet.next()) {
				Integer num = resultSet.getInt("num");
				return num;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JdbcUtils.close(con, pre, resultSet);
		}
		return 0;
	}

	@Override
	public void addVisits() {
		// TODO 自动生成的方法存根
		try {
			con = JdbcUtils.getConnection();
			String sql = "update tb_login_history set num = num + 1 where id = 1";
			pre = con.prepareStatement(sql);
			pre.executeUpdate();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			JdbcUtils.close(con, pre, resultSet);
		}
	}

}
