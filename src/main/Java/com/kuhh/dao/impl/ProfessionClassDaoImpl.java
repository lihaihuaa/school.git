package com.kuhh.dao.impl;

import com.kuhh.dao.IProfessionClassDao;
import com.kuhh.pojo.ProfessionClass;
import com.kuhh.utils.JdbcUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProfessionClassDaoImpl implements IProfessionClassDao {
    private Connection conn;
    private PreparedStatement ps;
    private ResultSet rs;

    @Override
    public List<ProfessionClass> getProfessionClassList() {
        List<ProfessionClass> list = new ArrayList<>();
        try {
            conn = JdbcUtils.getConnection();
            String sql = "select * from tb_profession_class order by create_time desc";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()){
                ProfessionClass professionClass = new ProfessionClass();
                professionClass.setClassNo(rs.getString(1));
                professionClass.setClassName(rs.getString(2));
                professionClass.setProfessionNo(rs.getString(3));
                professionClass.setTeacherNo(rs.getString(4));
                professionClass.setCreateTime(rs.getTime(5));
                professionClass.setUpdateTime(rs.getTime(6));
                list.add(professionClass);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JdbcUtils.close(conn,ps,rs);
        }
        return list;
    }

    @Override
    public ProfessionClass getProfessionClassByNo(String classNo) {
        try {
            conn = JdbcUtils.getConnection();
            String sql = "select * from tb_profession_class where class_no = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1,classNo);
            rs = ps.executeQuery();
            while (rs.next()){
                ProfessionClass professionClass = new ProfessionClass();
                professionClass.setClassNo(rs.getString(1));
                professionClass.setClassName(rs.getString(2));
                professionClass.setProfessionNo(rs.getString(3));
                professionClass.setTeacherNo(rs.getString(4));
                professionClass.setCreateTime(rs.getTime(5));
                professionClass.setUpdateTime(rs.getTime(6));
                return professionClass;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JdbcUtils.close(conn,ps,rs);
        }
        return null;
    }

    @Override
    public boolean updateProfessionClass(ProfessionClass professionClass) {
        try {
            conn = JdbcUtils.getConnection();
            String sql = "update tb_profession_class set class_name = ?,profession_no = ?,teacher_no = ?,update_time = ? where class_no = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1,professionClass.getClassName());
            ps.setString(2,professionClass.getProfessionNo());
            ps.setString(3,professionClass.getTeacherNo());
            ps.setDate(4, new Date(professionClass.getUpdateTime().getTime()));
            ps.setString(5,professionClass.getClassNo());
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JdbcUtils.close(conn,ps,rs);
        }
        return false;
    }

    @Override
    public boolean addProfessionClass(ProfessionClass professionClass) {
        try {
            conn = JdbcUtils.getConnection();
            String sql = "insert into tb_profession_class values(?,?,?,?,?,?)";
            ps = conn.prepareStatement(sql);
            ps.setString(1,professionClass.getClassNo());
            ps.setString(2,professionClass.getClassName());
            ps.setString(3,professionClass.getProfessionNo());
            ps.setString(4,professionClass.getTeacherNo());
            ps.setDate(5, new Date(professionClass.getCreateTime().getTime()));
            ps.setDate(6, new Date(professionClass.getUpdateTime().getTime()));
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JdbcUtils.close(conn,ps,rs);
        }
        return false;
    }

    @Override
    public boolean deleteProfessionClassByNo(String classNo) {
        try {
            conn = JdbcUtils.getConnection();
            String sql = "delete from tb_profession_class where class_no = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1,classNo);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JdbcUtils.close(conn,ps,rs);
        }
        return false;
    }

    @Override
    public List<ProfessionClass> getClassListByTeacherNo(String studentTeacherNo) {
        List<ProfessionClass> list = new ArrayList<>();
        try {
            conn = JdbcUtils.getConnection();
            String sql = "select * from tb_profession_class where teacher_no = ? order by create_time desc";
            ps = conn.prepareStatement(sql);
            ps.setString(1,studentTeacherNo);
            rs = ps.executeQuery();
            while (rs.next()){
                ProfessionClass professionClass = new ProfessionClass();
                professionClass.setClassNo(rs.getString(1));
                professionClass.setClassName(rs.getString(2));
                professionClass.setProfessionNo(rs.getString(3));
                professionClass.setTeacherNo(rs.getString(4));
                professionClass.setCreateTime(rs.getTime(5));
                professionClass.setUpdateTime(rs.getTime(6));
                list.add(professionClass);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JdbcUtils.close(conn,ps,rs);
        }
        return list;
    }
}
