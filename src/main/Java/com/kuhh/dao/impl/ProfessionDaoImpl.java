package com.kuhh.dao.impl;

import com.kuhh.dao.IProfessionDao;
import com.kuhh.pojo.Profession;
import com.kuhh.utils.JdbcUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProfessionDaoImpl implements IProfessionDao {
    private Connection conn;
    private PreparedStatement ps;
    private ResultSet rs;

    @Override
    public List<Profession> getProfessionList() {
        List<Profession> list = new ArrayList<>();
        try {
            conn = JdbcUtils.getConnection();
            String sql = "select * from tb_profession order by create_time desc";
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                Profession profession = new Profession();
                profession.setProfessionNo(rs.getString(1));
                profession.setProfessionName(rs.getString(2));
                profession.setCollegeNo(rs.getString(3));
                profession.setCreateTime(rs.getTime(4));
                profession.setUpdateTime(rs.getTime(5));
                list.add(profession);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JdbcUtils.close(conn,ps,rs);
        }
        return list;
    }

    @Override
    public Profession getProfessionByNo(String professionNo) {
        try {
            conn = JdbcUtils.getConnection();
            String sql = "select * from tb_profession where profession_no = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1,professionNo);
            rs = ps.executeQuery();
            while(rs.next()){
                Profession profession = new Profession();
                profession.setProfessionNo(rs.getString(1));
                profession.setProfessionName(rs.getString(2));
                profession.setCollegeNo(rs.getString(3));
                profession.setCreateTime(rs.getTime(4));
                profession.setUpdateTime(rs.getTime(5));
                return profession;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JdbcUtils.close(conn,ps,rs);
        }
        return null;
    }

    @Override
    public boolean updateProfession(Profession profession) {
        try {
            conn = JdbcUtils.getConnection();
            String sql = "update tb_profession set profession_name = ?,college_no = ?,update_time = ? where profession_no = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1,profession.getProfessionName());
            ps.setString(2,profession.getCollegeNo());
            ps.setDate(3, new Date(profession.getUpdateTime().getTime()));
            ps.setString(4,profession.getProfessionNo());
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JdbcUtils.close(conn,ps,rs);
        }
        return false;
    }

    @Override
    public boolean addProfession(Profession profession) {
        try {
            conn = JdbcUtils.getConnection();
            String sql = "insert into tb_profession values(?,?,?,?,?)";
            ps = conn.prepareStatement(sql);
            ps.setString(1,profession.getProfessionNo());
            ps.setString(2,profession.getProfessionName());
            ps.setString(3,profession.getCollegeNo());
            ps.setDate(4, new Date(profession.getCreateTime().getTime()));
            ps.setDate(5, new Date(profession.getUpdateTime().getTime()));
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JdbcUtils.close(conn,ps,rs);
        }
        return false;
    }

    @Override
    public boolean deleteProfession(String professionNo) {
        try {
            conn = JdbcUtils.getConnection();
            String sql = "delete from tb_peofession where profession_no = ?";
            ps = conn.prepareStatement(sql);
            ps.setString(1,professionNo);
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            JdbcUtils.close(conn,ps,rs);
        }
        return false;
    }
}
