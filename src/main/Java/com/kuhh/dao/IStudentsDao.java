package com.kuhh.dao;

import java.util.List;

import com.kuhh.pojo.Students;

public interface IStudentsDao {
	//获取所有学生
	List<Students> getStudentList();
	//通过学生学号获取学生信息
	Students getStudentByNo(String studentNo);
	//添加学生
	boolean addStudent(Students student);
	//修改学生
	boolean updateStudent(Students student);
	//删除学生
	boolean deleteStudent(String studentNo);
	//根据user表和students获取未注册学生
	List<Students> getStudentUnRegister();
	//根据学号修改个性签名
	void updateStudent(String studentNo, String description);
}
