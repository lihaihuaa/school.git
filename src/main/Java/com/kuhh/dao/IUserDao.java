package com.kuhh.dao;

import java.util.List;
import com.kuhh.pojo.User;

//数据访问层
public interface IUserDao {
	//通过名字密码获取用户
	User getUserByUserNameAndPassword(String username, String password);
	//添加用户
	void addUser(User user);
	//添加用户老师
	void addUserTeacher(User user);
	//删除用户
	void deleteUser(int id);
	//改变状态
	void changeStatus(Byte state, int userId);
	//通过名字获取用户
	User getUserByUserName(String username);

}
