package com.kuhh.dao;

import com.kuhh.pojo.Profession;

import java.util.List;

public interface IProfessionDao {
    //获取所有的专业信息
    List<Profession> getProfessionList();
    //根据专业号，获取专业信息
    Profession getProfessionByNo(String professionNo);
    //修改专业信息
    boolean updateProfession(Profession profession);
    //添加专业信息
    boolean addProfession(Profession profession);
    //根据专业号，删除专业信息
    boolean deleteProfession(String professionNo);
}
