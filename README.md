# 学校教务管理系统
-  **问题交流QQ群：660697693** 
-  **前台演示地址（服务器1核2G内存，请温和测试 ^ ^）：** [http://school.kuhh.cn](http://school.kuhh.cn)

#### 介绍

![输入图片说明](https://images.gitee.com/uploads/images/2021/1027/201744_6f656c9d_5742476.png "屏幕截图.png")

基于Maven的学校教务管理系统，快速部署运行，所用技术 MVC框架/MySQL/JavaBean/Servlet/Maven/Jsp/Bootstrap/Ajax/Jquery/Tomcat。

 **本系统利用Java Web技术实现了学校教务管理系统，具有简单的学生信息管理和教师信息管理功能。** 

作为学校教务管理系统的核心组成部分之一，教务系统管理主要分为三大模块，一是管理员管理模块、二是学生用户模块、三是教师用户模块，其中三大模块内具体又分多个不同的模块管理，实现了对整个管理系统的一站式管理和维护。

所有页面均兼容IE10及以上现代浏览器。

#### 软件架构

```
MVC框架、MySQL、JavaBean、Servlet、Maven、Jsp、Bootstrap、Ajax、Jquery、Tomcat8。
```

#### 项目默认运行地址

- 地址：[http://localhost:8080/school](http://localhost:8080/school)


#### 部署方式

1. 项目使用IDEA开发，请使用IDEA的版本控制检出功能，输入“[https://gitee.com/lihaihuaa/school.git](https://gitee.com/lihaihuaa/school.git)”拉取项目即可。
2. 项目使用Eclipse、STS开发，请使用相应的编程软件的版本控制检出功能，输入“[https://gitee.com/lihaihuaa/school.git](https://gitee.com/lihaihuaa/school.git)”拉取项目即可。
3. 项目数据库为MySQL 5.7版本，请在**码云附件**或**问题交流群文件**上下载SQL文件并导入到数据库中。
4. 使用IDEA、Eclipse、STS打开项目后，在maven面板刷新项目，下载依赖包。
5. 注意连接数据库的端口和用户名以及密码、还有Tomcat服务器端口的修改


#### 注意事项：

1. 该系统必须先登录后才可访问其他资源。
2. 该项目同时兼容Eclipse、STS，但如有自行扩展代码的意愿，建议使用IDEA。
3. 该项目是我们几个学生在校合作完成的一个练习项目，目的是让编程初学者和应届毕业生可以参考一下用较少的代码实现一个完整MVC模式，相关领域大神们可以给我们建议，让我们做得更好。
4. 注意使用Tomcat8以下版本时，记得修改编码格式哟。也就是Controller层的接收前端数据的时候需要new String(data.getBytes("xxx"),"utf-8");编码转换一下。

#### 项目界面

- ##### 后台管理员界面(部分)
![登录](https://cdn.jsdelivr.net/gh/lihaihuaa/imgbed/image-20211102194450224.png)
![主页](https://images.gitee.com/uploads/images/2021/1102/194613_8d816f96_5742476.png "屏幕截图.png")
![专业管理](https://images.gitee.com/uploads/images/2021/1102/194630_318143e8_5742476.png "屏幕截图.png")
![课程管理](https://images.gitee.com/uploads/images/2021/1102/194644_31ddace5_5742476.png "屏幕截图.png")
![选课管理](https://images.gitee.com/uploads/images/2021/1102/194738_7374e8c9_5742476.png "屏幕截图.png")
![成绩管理](https://images.gitee.com/uploads/images/2021/1102/194748_7057a653_5742476.png "屏幕截图.png")
![学生管理](https://images.gitee.com/uploads/images/2021/1102/194810_90d05d65_5742476.png "屏幕截图.png")
![账号管理](https://images.gitee.com/uploads/images/2021/1102/194847_7f3dfd8d_5742476.png "屏幕截图.png")

- ##### 后台管学生界面
![课程信息](https://images.gitee.com/uploads/images/2021/1102/195047_2e6cc45a_5742476.png "屏幕截图.png")
![选课信息](https://images.gitee.com/uploads/images/2021/1102/195057_b6b550ac_5742476.png "屏幕截图.png")
![成绩信息](https://images.gitee.com/uploads/images/2021/1102/195105_bb2b00cc_5742476.png "屏幕截图.png")
![档案信息](https://images.gitee.com/uploads/images/2021/1102/195133_fd90cfd2_5742476.png "屏幕截图.png")

- ##### 后台管教师界面
![班级信息](https://images.gitee.com/uploads/images/2021/1102/195231_de69d2bb_5742476.png "屏幕截图.png")
![课程信息](https://images.gitee.com/uploads/images/2021/1102/195246_fc2bec23_5742476.png "屏幕截图.png")
![选课信息](https://images.gitee.com/uploads/images/2021/1102/195258_1fd80013_5742476.png "屏幕截图.png")
![成绩信息](https://images.gitee.com/uploads/images/2021/1102/195306_a9bfd3d2_5742476.png "屏幕截图.png")
![档案信息](https://images.gitee.com/uploads/images/2021/1102/195314_fd4e070a_5742476.png "屏幕截图.png")