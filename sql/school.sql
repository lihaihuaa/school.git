/*
 Navicat Premium Data Transfer

 Source Server         : ceshi
 Source Server Type    : MySQL
 Source Server Version : 50729
 Source Host           : localhost:3306
 Source Schema         : new_training

 Target Server Type    : MySQL
 Target Server Version : 50729
 File Encoding         : 65001

 Date: 24/11/2021 10:13:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_college
-- ----------------------------
DROP TABLE IF EXISTS `tb_college`;
CREATE TABLE `tb_college`  (
  `college_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学院',
  `college_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '学院名字',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`college_no`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_college
-- ----------------------------
INSERT INTO `tb_college` VALUES ('DJ', '电气电子与计算机科学学院', NULL, '2021-10-26 00:00:00');
INSERT INTO `tb_college` VALUES ('GC', '工程技术学院', NULL, NULL);
INSERT INTO `tb_college` VALUES ('GJ', '国际教育学院', NULL, NULL);
INSERT INTO `tb_college` VALUES ('JG', '经济与管理学院', NULL, NULL);
INSERT INTO `tb_college` VALUES ('JP', '继续教育与培训学院', NULL, NULL);
INSERT INTO `tb_college` VALUES ('JQ', '机械与汽车工程学院', NULL, NULL);
INSERT INTO `tb_college` VALUES ('JT', '计通学院', NULL, NULL);
INSERT INTO `tb_college` VALUES ('LI', '理学院', NULL, NULL);
INSERT INTO `tb_college` VALUES ('MK', '马克思主义学院', NULL, NULL);
INSERT INTO `tb_college` VALUES ('QD', '启迪数字学院', NULL, NULL);
INSERT INTO `tb_college` VALUES ('RS', '人文艺术与设计学院', NULL, NULL);
INSERT INTO `tb_college` VALUES ('SH', '生物与化学工程学院', NULL, NULL);
INSERT INTO `tb_college` VALUES ('TJ', '土木建筑工程学院', NULL, NULL);
INSERT INTO `tb_college` VALUES ('TY', '体育学院', NULL, NULL);
INSERT INTO `tb_college` VALUES ('VR', '宏达威爱科技学院', NULL, NULL);
INSERT INTO `tb_college` VALUES ('WC', '微电子与材料工程学院', NULL, NULL);
INSERT INTO `tb_college` VALUES ('WG', '外国语学院', NULL, NULL);
INSERT INTO `tb_college` VALUES ('YX', '医学部', NULL, NULL);

-- ----------------------------
-- Table structure for tb_elective_student
-- ----------------------------
DROP TABLE IF EXISTS `tb_elective_student`;
CREATE TABLE `tb_elective_student`  (
  `id` int(11) NOT NULL,
  `profession_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '专业',
  `course_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '选修课',
  `student_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '学生',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `new_es_course_no`(`course_no`) USING BTREE,
  INDEX `new_es_student_no`(`student_no`) USING BTREE,
  INDEX `new_es_profession_no`(`profession_no`) USING BTREE,
  CONSTRAINT `new_es_course_no` FOREIGN KEY (`course_no`) REFERENCES `tb_profession_course` (`course_no`) ON DELETE SET NULL ON UPDATE RESTRICT,
  CONSTRAINT `new_es_profession_no` FOREIGN KEY (`profession_no`) REFERENCES `tb_profession` (`profession_no`) ON DELETE SET NULL ON UPDATE RESTRICT,
  CONSTRAINT `new_es_student_no` FOREIGN KEY (`student_no`) REFERENCES `tb_students` (`student_no`) ON DELETE SET NULL ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_elective_student
-- ----------------------------
INSERT INTO `tb_elective_student` VALUES (1, 'QD001', 'QD_2_C001', 'S001', NULL, NULL);
INSERT INTO `tb_elective_student` VALUES (2, 'QD001', 'QD_2_C001', 'S002', NULL, NULL);
INSERT INTO `tb_elective_student` VALUES (3, 'QD001', 'QD_2_C001', 'S003', NULL, NULL);
INSERT INTO `tb_elective_student` VALUES (4, 'QD001', 'QD_2_C001', 'S004', NULL, NULL);
INSERT INTO `tb_elective_student` VALUES (5, 'QD001', 'QD_2_C002', 'S001', NULL, NULL);
INSERT INTO `tb_elective_student` VALUES (6, 'QD001', 'QD_2_C003', 'S001', NULL, NULL);
INSERT INTO `tb_elective_student` VALUES (7, 'QD001', 'QD_2_C004', 'S001', NULL, NULL);
INSERT INTO `tb_elective_student` VALUES (8, 'QD001', 'QD_2_C005', 'S001', NULL, NULL);
INSERT INTO `tb_elective_student` VALUES (9, 'QD001', 'QD_2_C006', 'S001', NULL, NULL);
INSERT INTO `tb_elective_student` VALUES (10, 'QD001', 'QD_2_C007', 'S001', NULL, NULL);
INSERT INTO `tb_elective_student` VALUES (11, 'QD001', 'QD_2_C008', 'S001', NULL, NULL);
INSERT INTO `tb_elective_student` VALUES (12, 'QD001', 'QD_2_C009', 'S001', NULL, NULL);
INSERT INTO `tb_elective_student` VALUES (13, 'QD001', 'QD_2_C010', 'S001', NULL, NULL);
INSERT INTO `tb_elective_student` VALUES (14, 'QD001', 'QD_3_C001', 'S001', NULL, NULL);
INSERT INTO `tb_elective_student` VALUES (15, 'QD001', 'QD_3_C002', 'S001', NULL, NULL);
INSERT INTO `tb_elective_student` VALUES (16, 'QD001', 'QD_3_C003', 'S001', NULL, NULL);
INSERT INTO `tb_elective_student` VALUES (17, 'QD001', 'QD_3_C004', 'S001', NULL, NULL);

-- ----------------------------
-- Table structure for tb_grade
-- ----------------------------
DROP TABLE IF EXISTS `tb_grade`;
CREATE TABLE `tb_grade`  (
  `grade_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '年级',
  `grade_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '年级名字',
  `teacher_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '教师',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`grade_no`) USING BTREE,
  INDEX `new_g_teacher_no`(`teacher_no`) USING BTREE,
  CONSTRAINT `new_g_teacher_no` FOREIGN KEY (`teacher_no`) REFERENCES `tb_teachers` (`teacher_no`) ON DELETE SET NULL ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_grade
-- ----------------------------
INSERT INTO `tb_grade` VALUES ('1', '一年级', 'T001', NULL, NULL);
INSERT INTO `tb_grade` VALUES ('2', '二年级', 'T002', NULL, NULL);
INSERT INTO `tb_grade` VALUES ('3', '三年级', 'T003', NULL, NULL);
INSERT INTO `tb_grade` VALUES ('4', '四年级', 'T004', NULL, NULL);

-- ----------------------------
-- Table structure for tb_grade_college
-- ----------------------------
DROP TABLE IF EXISTS `tb_grade_college`;
CREATE TABLE `tb_grade_college`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grade_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '年级',
  `college_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '学院',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `new_gc_grade_no`(`grade_no`) USING BTREE,
  INDEX `new_gc_college_no`(`college_no`) USING BTREE,
  CONSTRAINT `new_gc_college_no` FOREIGN KEY (`college_no`) REFERENCES `tb_college` (`college_no`) ON DELETE SET NULL ON UPDATE RESTRICT,
  CONSTRAINT `new_gc_grade_no` FOREIGN KEY (`grade_no`) REFERENCES `tb_grade` (`grade_no`) ON DELETE SET NULL ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 74 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_grade_college
-- ----------------------------
INSERT INTO `tb_grade_college` VALUES (2, '1', 'QD', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (3, '1', 'JT', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (4, '1', 'JQ', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (5, '1', 'SH', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (6, '1', 'TJ', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (7, '1', 'WC', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (8, '1', 'DJ', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (9, '1', 'JG', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (10, '1', 'GC', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (11, '1', 'LI', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (12, '1', 'WG', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (13, '1', 'RS', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (14, '1', 'MK', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (15, '1', 'TY', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (16, '1', 'GJ', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (17, '1', 'JP', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (18, '1', 'VR', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (19, '1', 'YX', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (20, '2', 'QD', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (21, '2', 'JT', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (22, '2', 'JQ', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (23, '2', 'SH', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (24, '2', 'TJ', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (25, '2', 'WC', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (26, '2', 'DJ', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (27, '2', 'JG', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (28, '2', 'GC', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (29, '2', 'LI', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (30, '2', 'WG', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (31, '2', 'RS', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (32, '2', 'MK', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (33, '2', 'TY', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (34, '2', 'GJ', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (35, '2', 'JP', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (36, '2', 'VR', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (37, '2', 'YX', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (38, '3', 'QD', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (39, '3', 'JT', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (40, '3', 'JQ', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (41, '3', 'SH', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (42, '3', 'TJ', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (43, '3', 'WC', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (44, '3', 'DJ', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (45, '3', 'JG', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (46, '3', 'GC', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (47, '3', 'LI', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (48, '3', 'WG', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (49, '3', 'RS', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (50, '3', 'MK', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (51, '3', 'TY', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (52, '3', 'GJ', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (53, '3', 'JP', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (54, '3', 'VR', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (55, '3', 'YX', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (56, '4', 'QD', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (57, '4', 'JT', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (58, '4', 'JQ', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (59, '4', 'SH', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (60, '4', 'TJ', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (61, '4', 'WC', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (62, '4', 'DJ', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (63, '4', 'JG', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (64, '4', 'GC', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (65, '4', 'LI', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (66, '4', 'WG', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (67, '4', 'RS', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (68, '4', 'MK', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (69, '4', 'TY', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (70, '4', 'GJ', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (71, '4', 'JP', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (72, '4', 'VR', NULL, NULL);
INSERT INTO `tb_grade_college` VALUES (73, '4', 'YX', NULL, NULL);

-- ----------------------------
-- Table structure for tb_login_history
-- ----------------------------
DROP TABLE IF EXISTS `tb_login_history`;
CREATE TABLE `tb_login_history`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT NULL,
  `num` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 685 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_login_history
-- ----------------------------
INSERT INTO `tb_login_history` VALUES (1, NULL, NULL, NULL, 450);
INSERT INTO `tb_login_history` VALUES (3, 1, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (4, 1, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (5, 1, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (6, 1, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (7, 1, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (8, 1, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (9, 1, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (10, 7, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (11, 1, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (12, 1, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (13, 7, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (14, 1, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (15, 1, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (16, 1, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (17, 1, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (18, 1, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (19, 1, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (20, 1, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (21, 1, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (22, 7, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (23, 1, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (24, 1, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (25, 8, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (26, 7, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (27, 8, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (28, 8, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (29, 8, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (30, 8, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (31, 8, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (32, 7, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (33, 1, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (34, 1, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (35, 1, '0:0:0:0:0:0:0:1', '2020-12-29 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (36, 7, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (37, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (38, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (39, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (40, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (41, 7, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (42, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (43, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (44, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (45, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (46, 10, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (47, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (48, 7, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (49, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (50, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (51, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (52, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (53, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (54, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (55, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (56, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (57, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (58, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (59, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (60, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (61, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (62, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (63, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (64, 8, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (65, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (66, 8, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (67, 8, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (68, 8, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (69, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (70, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (71, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (72, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (73, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (74, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (75, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (76, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (77, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (78, 7, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (79, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (80, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (81, 8, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (82, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (83, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (84, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (85, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (86, 7, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (87, 1, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (88, 7, '0:0:0:0:0:0:0:1', '2020-12-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (89, 1, '0:0:0:0:0:0:0:1', '2020-12-31 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (90, 1, '127.0.0.1', '2020-12-31 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (91, 1, '0:0:0:0:0:0:0:1', '2020-12-31 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (92, 1, '0:0:0:0:0:0:0:1', '2020-12-31 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (93, 7, '0:0:0:0:0:0:0:1', '2020-12-31 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (94, 1, '0:0:0:0:0:0:0:1', '2020-12-31 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (95, 1, '0:0:0:0:0:0:0:1', '2020-12-31 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (96, 1, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (97, 1, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (98, 1, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (99, 1, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (100, 1, '127.0.0.1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (101, 1, '127.0.0.1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (102, 1, '127.0.0.1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (103, 1, '127.0.0.1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (104, 1, '127.0.0.1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (105, 1, '127.0.0.1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (106, 1, '127.0.0.1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (107, 1, '127.0.0.1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (108, 1, '127.0.0.1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (109, 1, '127.0.0.1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (110, 1, '127.0.0.1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (111, 1, '127.0.0.1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (112, 1, '127.0.0.1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (113, 1, '127.0.0.1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (114, 1, '127.0.0.1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (115, 1, '127.0.0.1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (116, 1, '127.0.0.1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (117, 11, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (118, 11, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (119, 11, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (120, 11, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (121, 11, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (122, 11, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (123, 11, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (124, 1, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (125, 12, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (126, 12, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (127, 12, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (128, 12, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (129, 12, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (130, 12, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (131, 7, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (132, 12, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (133, 1, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (134, 12, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (135, 12, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (136, 1, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (137, 1, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (138, 12, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (139, 1, '0:0:0:0:0:0:0:1', '2021-01-02 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (140, 1, '0:0:0:0:0:0:0:1', '2021-01-03 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (141, 1, '0:0:0:0:0:0:0:1', '2021-01-04 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (142, 15, '0:0:0:0:0:0:0:1', '2021-01-04 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (143, 1, '0:0:0:0:0:0:0:1', '2021-01-04 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (144, 1, '0:0:0:0:0:0:0:1', '2021-01-04 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (145, 1, '0:0:0:0:0:0:0:1', '2021-01-04 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (146, 1, '0:0:0:0:0:0:0:1', '2021-01-04 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (147, 1, '0:0:0:0:0:0:0:1', '2021-01-04 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (148, 7, '0:0:0:0:0:0:0:1', '2021-01-04 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (149, 1, '0:0:0:0:0:0:0:1', '2021-01-04 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (150, 1, '0:0:0:0:0:0:0:1', '2021-01-05 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (151, 1, '0:0:0:0:0:0:0:1', '2021-01-05 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (152, 1, '0:0:0:0:0:0:0:1', '2021-01-05 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (153, 1, '0:0:0:0:0:0:0:1', '2021-01-05 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (154, 1, '0:0:0:0:0:0:0:1', '2021-01-05 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (155, 1, '0:0:0:0:0:0:0:1', '2021-01-05 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (156, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (157, 12, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (158, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (159, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (160, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (161, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (162, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (163, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (164, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (165, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (166, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (167, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (168, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (169, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (170, 12, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (171, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (172, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (173, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (174, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (175, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (176, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (177, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (178, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (179, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (180, 17, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (181, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (182, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (183, 12, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (184, 17, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (185, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (186, 1, '0:0:0:0:0:0:0:1', '2021-01-06 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (187, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (188, 17, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (189, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (190, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (191, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (192, 12, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (193, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (194, 12, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (195, 17, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (196, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (197, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (198, 17, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (199, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (200, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (201, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (202, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (203, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (204, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (205, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (206, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (207, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (208, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (209, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (210, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (211, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (212, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (213, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (214, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (215, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (216, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (217, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (218, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (219, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (220, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (221, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (222, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (223, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (224, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (225, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (226, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (227, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (228, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (229, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (230, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (231, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (232, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (233, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (234, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (235, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (236, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (237, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (238, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (239, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (240, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (241, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (242, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (243, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (244, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (245, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (246, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (247, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (248, 1, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (249, 17, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (250, 17, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (251, 12, '0:0:0:0:0:0:0:1', '2021-01-07 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (252, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (253, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (254, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (255, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (256, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (257, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (258, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (259, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (260, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (261, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (262, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (263, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (264, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (265, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (266, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (267, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (268, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (269, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (270, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (271, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (272, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (273, 17, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (274, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (275, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (276, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (277, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (278, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (279, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (280, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (281, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (282, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (283, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (284, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (285, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (286, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (287, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (288, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (289, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (290, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (291, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (292, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (293, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (294, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (295, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (296, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (297, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (298, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (299, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (300, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (301, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (302, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (303, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (304, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (305, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (306, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (307, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (308, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (309, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (310, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (311, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (312, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (313, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (314, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (315, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (316, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (317, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (318, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (319, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (320, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (321, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (322, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (323, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (324, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (325, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (326, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (327, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (328, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (329, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (330, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (331, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (332, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (333, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (334, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (335, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (336, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (337, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (338, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (339, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (340, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (341, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (342, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (343, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (344, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (345, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (346, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (347, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (348, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (349, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (350, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (351, 12, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (352, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (353, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (354, 1, '0:0:0:0:0:0:0:1', '2021-01-08 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (355, 1, '0:0:0:0:0:0:0:1', '2021-01-09 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (356, 1, '0:0:0:0:0:0:0:1', '2021-01-09 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (357, 17, '0:0:0:0:0:0:0:1', '2021-01-09 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (358, 1, '0:0:0:0:0:0:0:1', '2021-01-09 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (359, 1, '0:0:0:0:0:0:0:1', '2021-01-10 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (360, 16, '0:0:0:0:0:0:0:1', '2021-01-10 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (361, 1, '0:0:0:0:0:0:0:1', '2021-01-11 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (362, 1, '0:0:0:0:0:0:0:1', '2021-06-17 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (363, 1, '0:0:0:0:0:0:0:1', '2021-06-17 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (364, 1, '0:0:0:0:0:0:0:1', '2021-09-22 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (365, 1, '0:0:0:0:0:0:0:1', '2021-09-22 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (366, 16, '0:0:0:0:0:0:0:1', '2021-09-22 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (367, 1, '0:0:0:0:0:0:0:1', '2021-09-22 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (368, 1, '0:0:0:0:0:0:0:1', '2021-09-22 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (369, 1, '0:0:0:0:0:0:0:1', '2021-09-22 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (370, 1, '0:0:0:0:0:0:0:1', '2021-09-22 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (371, 1, '0:0:0:0:0:0:0:1', '2021-09-22 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (372, 1, '0:0:0:0:0:0:0:1', '2021-09-22 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (373, 1, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (374, 1, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (375, 1, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (376, 1, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (377, 1, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (378, 1, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (379, 1, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (380, 1, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (381, 1, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (382, 1, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (383, 1, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (384, 1, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (385, 12, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (386, 16, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (387, 16, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (388, 12, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (389, 12, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (390, 1, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (391, 12, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (392, 1, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (393, 1, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (394, 12, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (395, 17, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (396, 17, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (397, 1, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (398, 17, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (399, 12, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (400, 12, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (401, 12, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (402, 1, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (403, 17, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (404, 17, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (405, 17, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (406, 1, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (407, 1, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (408, 17, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (409, 12, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (410, 15, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (411, 15, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (412, 1, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (413, 15, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (414, 17, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (415, 1, '0:0:0:0:0:0:0:1', '2021-09-23 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (416, 1, '0:0:0:0:0:0:0:1', '2021-09-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (417, 1, '0:0:0:0:0:0:0:1', '2021-09-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (418, 1, '0:0:0:0:0:0:0:1', '2021-09-30 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (419, 1, '0:0:0:0:0:0:0:1', '2021-10-11 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (420, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (421, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (422, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (423, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (424, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (425, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (426, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (427, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (428, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (429, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (430, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (431, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (432, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (433, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (434, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (435, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (436, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (437, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (438, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (439, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (440, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (441, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (442, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (443, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (444, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (445, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (446, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (447, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (448, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (449, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (450, 1, '127.0.0.1', '2021-10-12 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (451, 1, '127.0.0.1', '2021-10-14 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (452, 1, '127.0.0.1', '2021-10-14 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (453, 1, '127.0.0.1', '2021-10-15 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (454, 1, '127.0.0.1', '2021-10-15 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (455, 1, '127.0.0.1', '2021-10-15 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (456, 1, '127.0.0.1', '2021-10-15 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (457, 1, '127.0.0.1', '2021-10-15 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (458, 1, '127.0.0.1', '2021-10-18 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (459, 1, '127.0.0.1', '2021-10-18 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (460, 1, '127.0.0.1', '2021-10-18 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (461, 1, '127.0.0.1', '2021-10-18 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (462, 1, '127.0.0.1', '2021-10-18 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (463, 1, '127.0.0.1', '2021-10-18 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (464, 1, '127.0.0.1', '2021-10-18 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (465, 1, '127.0.0.1', '2021-10-19 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (466, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (467, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (468, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (469, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (470, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (471, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (472, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (473, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (474, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (475, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (476, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (477, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (478, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (479, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (480, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (481, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (482, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (483, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (484, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (485, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (486, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (487, 1, '127.0.0.1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (488, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (489, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (490, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (491, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (492, 1, '127.0.0.1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (493, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (494, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (495, 17, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (496, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (497, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (498, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (499, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (500, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (501, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (502, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (503, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (504, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (505, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (506, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (507, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (508, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (509, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (510, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (511, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (512, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (513, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (514, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (515, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (516, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (517, 1, '127.0.0.1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (518, 1, '127.0.0.1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (519, 1, '127.0.0.1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (520, 1, '127.0.0.1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (521, 1, '127.0.0.1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (522, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (523, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (524, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (525, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (526, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (527, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (528, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (529, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (530, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (531, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (532, 1, '127.0.0.1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (533, 1, '127.0.0.1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (534, 1, '127.0.0.1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (535, 1, '127.0.0.1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (536, 1, '127.0.0.1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (537, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (538, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (539, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (540, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (541, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (542, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (543, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (544, 1, '127.0.0.1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (545, 1, '127.0.0.1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (546, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (547, 1, '0:0:0:0:0:0:0:1', '2021-10-20 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (548, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (549, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (550, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (551, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (552, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (553, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (554, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (555, 1, '127.0.0.1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (556, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (557, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (558, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (559, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (560, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (561, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (562, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (563, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (564, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (565, 1, '127.0.0.1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (566, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (567, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (568, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (569, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (570, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (571, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (572, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (573, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (574, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (575, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (576, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (577, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (578, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (579, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (580, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (581, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (582, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (583, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (584, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (585, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (586, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (587, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (588, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (589, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (590, 1, '127.0.0.1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (591, 1, '127.0.0.1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (592, 1, '127.0.0.1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (593, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (594, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (595, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (596, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (597, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (598, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (599, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (600, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (601, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (602, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (603, 1, '0:0:0:0:0:0:0:1', '2021-10-21 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (604, 1, '0:0:0:0:0:0:0:1', '2021-10-22 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (605, 1, '0:0:0:0:0:0:0:1', '2021-10-22 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (606, 1, '0:0:0:0:0:0:0:1', '2021-10-22 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (607, 1, '0:0:0:0:0:0:0:1', '2021-10-22 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (608, 1, '0:0:0:0:0:0:0:1', '2021-10-22 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (609, 1, '0:0:0:0:0:0:0:1', '2021-10-22 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (610, 1, '0:0:0:0:0:0:0:1', '2021-10-22 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (611, 1, '0:0:0:0:0:0:0:1', '2021-10-22 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (612, 1, '0:0:0:0:0:0:0:1', '2021-10-22 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (613, 1, '0:0:0:0:0:0:0:1', '2021-10-22 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (614, 1, '0:0:0:0:0:0:0:1', '2021-10-22 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (615, 1, '0:0:0:0:0:0:0:1', '2021-10-22 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (616, 1, '0:0:0:0:0:0:0:1', '2021-10-22 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (617, 1, '0:0:0:0:0:0:0:1', '2021-10-22 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (618, 1, '0:0:0:0:0:0:0:1', '2021-10-22 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (619, 1, '0:0:0:0:0:0:0:1', '2021-10-22 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (620, 1, '0:0:0:0:0:0:0:1', '2021-10-25 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (621, 1, '0:0:0:0:0:0:0:1', '2021-10-25 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (622, 1, '0:0:0:0:0:0:0:1', '2021-10-25 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (623, 1, '0:0:0:0:0:0:0:1', '2021-10-25 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (624, 1, '0:0:0:0:0:0:0:1', '2021-10-25 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (625, 1, '0:0:0:0:0:0:0:1', '2021-10-25 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (626, 1, '0:0:0:0:0:0:0:1', '2021-10-25 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (627, 1, '0:0:0:0:0:0:0:1', '2021-10-25 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (628, 1, '0:0:0:0:0:0:0:1', '2021-10-25 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (629, 1, '0:0:0:0:0:0:0:1', '2021-10-25 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (630, 1, '0:0:0:0:0:0:0:1', '2021-10-25 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (631, 1, '127.0.0.1', '2021-10-25 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (632, 1, '0:0:0:0:0:0:0:1', '2021-10-25 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (633, 1, '0:0:0:0:0:0:0:1', '2021-10-25 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (634, 1, '0:0:0:0:0:0:0:1', '2021-10-25 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (635, 1, '0:0:0:0:0:0:0:1', '2021-10-25 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (636, 1, '0:0:0:0:0:0:0:1', '2021-10-25 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (637, 1, '0:0:0:0:0:0:0:1', '2021-10-25 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (638, 1, '0:0:0:0:0:0:0:1', '2021-10-25 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (639, 1, '0:0:0:0:0:0:0:1', '2021-10-25 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (640, 1, '0:0:0:0:0:0:0:1', '2021-10-25 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (641, 1, '0:0:0:0:0:0:0:1', '2021-10-25 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (642, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (643, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (644, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (645, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (646, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (647, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (648, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (649, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (650, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (651, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (652, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (653, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (654, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (655, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (656, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (657, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (658, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (659, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (660, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (661, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (662, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (663, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (664, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (665, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (666, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (667, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (668, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (669, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (670, 10, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (671, 15, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (672, 15, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (673, 1, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (674, 19, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (675, 18, '0:0:0:0:0:0:0:1', '2021-10-26 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (676, 19, '0:0:0:0:0:0:0:1', '2021-10-27 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (677, 19, '0:0:0:0:0:0:0:1', '2021-10-27 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (678, 18, '0:0:0:0:0:0:0:1', '2021-10-27 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (679, 18, '0:0:0:0:0:0:0:1', '2021-10-27 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (680, 18, '0:0:0:0:0:0:0:1', '2021-10-27 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (681, 18, '0:0:0:0:0:0:0:1', '2021-10-27 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (682, 19, '0:0:0:0:0:0:0:1', '2021-10-27 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (683, 19, '0:0:0:0:0:0:0:1', '2021-10-27 00:00:00', NULL);
INSERT INTO `tb_login_history` VALUES (684, 19, '0:0:0:0:0:0:0:1', '2021-10-27 00:00:00', NULL);

-- ----------------------------
-- Table structure for tb_profession
-- ----------------------------
DROP TABLE IF EXISTS `tb_profession`;
CREATE TABLE `tb_profession`  (
  `profession_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '专业',
  `profession_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '专业名字',
  `college_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '学院',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`profession_no`) USING BTREE,
  INDEX `profession_no`(`profession_no`) USING BTREE,
  INDEX `new_p_college_no`(`college_no`) USING BTREE,
  CONSTRAINT `new_p_college_no` FOREIGN KEY (`college_no`) REFERENCES `tb_college` (`college_no`) ON DELETE SET NULL ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_profession
-- ----------------------------
INSERT INTO `tb_profession` VALUES ('JG001', '信息管理与信息系统', 'JG', NULL, NULL);
INSERT INTO `tb_profession` VALUES ('JG002', '物流管理', 'JG', NULL, NULL);
INSERT INTO `tb_profession` VALUES ('JG003', '人力资源管理', 'JG', NULL, NULL);
INSERT INTO `tb_profession` VALUES ('JG004', '市场营销', 'JG', NULL, NULL);
INSERT INTO `tb_profession` VALUES ('JG005', '会计学', 'JG', NULL, NULL);
INSERT INTO `tb_profession` VALUES ('JG006', '财务管理', 'JG', NULL, NULL);
INSERT INTO `tb_profession` VALUES ('JG007', '金融学', 'JG', NULL, NULL);
INSERT INTO `tb_profession` VALUES ('JG008', '经济学', 'JG', NULL, NULL);
INSERT INTO `tb_profession` VALUES ('JG009', '国际经济与贸易', 'JG', NULL, NULL);
INSERT INTO `tb_profession` VALUES ('QD001', '软件工程专业（启迪）', 'QD', NULL, NULL);
INSERT INTO `tb_profession` VALUES ('QD002', '数据科学与大数据技术专业', 'QD', NULL, NULL);
INSERT INTO `tb_profession` VALUES ('QD003', '网络空间安全专业', 'QD', NULL, NULL);
INSERT INTO `tb_profession` VALUES ('QD004', '智能科学与技术专业', 'QD', NULL, NULL);
INSERT INTO `tb_profession` VALUES ('VR001', '软件工程专业（威爱）', 'VR', NULL, NULL);
INSERT INTO `tb_profession` VALUES ('VR002', '新媒体技术专业', 'VR', NULL, NULL);

-- ----------------------------
-- Table structure for tb_profession_class
-- ----------------------------
DROP TABLE IF EXISTS `tb_profession_class`;
CREATE TABLE `tb_profession_class`  (
  `class_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '班级',
  `class_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '班级名字',
  `profession_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '专业',
  `teacher_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '教师',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`class_no`) USING BTREE,
  INDEX `new_pclass_profession_no`(`profession_no`) USING BTREE,
  INDEX `class_no`(`class_no`) USING BTREE,
  INDEX `new_pclass_teacher_no`(`teacher_no`) USING BTREE,
  CONSTRAINT `new_pclass_profession_no` FOREIGN KEY (`profession_no`) REFERENCES `tb_profession` (`profession_no`) ON DELETE SET NULL ON UPDATE RESTRICT,
  CONSTRAINT `new_pclass_teacher_no` FOREIGN KEY (`teacher_no`) REFERENCES `tb_teachers` (`teacher_no`) ON DELETE SET NULL ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_profession_class
-- ----------------------------
INSERT INTO `tb_profession_class` VALUES ('QD1_18_001', '软件Q181', 'QD001', 'T001', NULL, NULL);
INSERT INTO `tb_profession_class` VALUES ('QD1_18_002', '软件Q182', 'QD001', 'T001', NULL, NULL);
INSERT INTO `tb_profession_class` VALUES ('QD1_18_003', '软件Q183', 'QD001', 'T002', NULL, NULL);
INSERT INTO `tb_profession_class` VALUES ('QD2_18_001', '数据Q181', 'QD002', 'T003', NULL, NULL);
INSERT INTO `tb_profession_class` VALUES ('QD2_18_002', '数据Q182', 'QD002', 'T003', NULL, NULL);
INSERT INTO `tb_profession_class` VALUES ('QD3_18_001', '网安Q181', 'QD003', 'T004', NULL, NULL);
INSERT INTO `tb_profession_class` VALUES ('QD3_18_002', '网安Q182', 'QD003', 'T004', NULL, NULL);
INSERT INTO `tb_profession_class` VALUES ('QD4_18_001', '智科Q181', 'QD004', 'T005', NULL, NULL);
INSERT INTO `tb_profession_class` VALUES ('QD4_18_002', '智科Q182', 'QD004', 'T005', NULL, NULL);
INSERT INTO `tb_profession_class` VALUES ('VR1_18_001', '软件V181', 'VR001', 'T002', NULL, NULL);
INSERT INTO `tb_profession_class` VALUES ('VR1_18_002', '软件V182', 'VR001', 'T002', NULL, NULL);

-- ----------------------------
-- Table structure for tb_profession_course
-- ----------------------------
DROP TABLE IF EXISTS `tb_profession_course`;
CREATE TABLE `tb_profession_course`  (
  `course_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '课程',
  `course_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '课程名字',
  `course_type` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '课程类型，1必修课，2选修课，3实践课',
  `course_credit` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '课程学分',
  `course_num` int(10) NULL DEFAULT NULL COMMENT '课程上限人数，默认为空，表示无上限',
  `grade_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '年级',
  `profession_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '专业',
  `teacher_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任课教师',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`course_no`) USING BTREE,
  INDEX `new_pcourse_profession_no`(`profession_no`) USING BTREE,
  INDEX `course_no`(`course_no`) USING BTREE,
  INDEX `new_pcourse_grade_no`(`grade_no`) USING BTREE,
  INDEX `new_pcourse_teacher_no`(`teacher_no`) USING BTREE,
  CONSTRAINT `new_pcourse_grade_no` FOREIGN KEY (`grade_no`) REFERENCES `tb_grade` (`grade_no`) ON DELETE SET NULL ON UPDATE RESTRICT,
  CONSTRAINT `new_pcourse_profession_no` FOREIGN KEY (`profession_no`) REFERENCES `tb_profession` (`profession_no`) ON DELETE SET NULL ON UPDATE RESTRICT,
  CONSTRAINT `new_pcourse_teacher_no` FOREIGN KEY (`teacher_no`) REFERENCES `tb_teachers` (`teacher_no`) ON DELETE SET NULL ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_profession_course
-- ----------------------------
INSERT INTO `tb_profession_course` VALUES ('QD_1_C001', '高等数学B1', '1', '4.5', NULL, '1', 'QD001', 'T004', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C002', '大学英语A1', '1', '2.0', NULL, '1', 'QD001', 'T005', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C003', '中国近现代史纲要', '1', '2.5', NULL, '1', 'QD001', 'T004', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C004', '体育A1', '1', '1.0', NULL, '1', 'QD001', 'T003', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C005', '计算机导论', '1', '2.0', NULL, '1', 'QD001', 'T002', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C006', '高级语言程序设计', '1', '4.0', NULL, '1', 'QD001', 'T001', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C007', '高级语言程序设计实验', '1', '2.0', NULL, '1', 'QD001', 'T002', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C008', '高等数学B2', '1', '4.5', NULL, '1', 'QD001', 'T003', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C009', '大学英语A2', '1', '3.0', NULL, '1', 'QD001', 'T004', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C010', '思想道德修养与法律基础', '1', '2.5', NULL, '1', 'QD001', 'T005', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C011', '大学生心理健康教育', '1', '1.0', NULL, '1', 'QD001', 'T004', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C012', '体育A2', '1', '1.0', NULL, '1', 'QD001', 'T003', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C013', '数字逻辑基础', '1', '3.0', NULL, '1', 'QD001', 'T002', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C014', '离散数学', '1', '3.0', NULL, '1', 'QD001', 'T001', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C015', '面向对象程序设计', '1', '4.0', NULL, '1', 'QD001', 'T002', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C016', '面向对象程序设计课程实践', '1', '1.5', NULL, '1', 'QD001', 'T003', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C017', '线性代数A', '1', '2.5', NULL, '2', 'QD001', 'T004', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C018', '大学英语A3', '1', '3.0', NULL, '2', 'QD001', 'T003', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C019', '计算机组成原理', '1', '3.0', NULL, '2', 'QD001', 'T002', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C020', '数据结构与算法', '1', '4.0', NULL, '2', 'QD001', 'T001', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C021', '体育A3', '1', '1.0', NULL, '2', 'QD001', 'T002', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C022', '数据库系统', '1', '4.0', NULL, '2', 'QD001', 'T003', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C023', '软件工程导论', '1', '3.0', NULL, '2', 'QD001', 'T004', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C024', '概率论与数理统计', '1', '3.0', NULL, '2', 'QD001', 'T002', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C025', '大学英语A4', '1', '2.0', NULL, '2', 'QD001', 'T001', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C026', '马克思主义基本原理', '1', '2.5', NULL, '2', 'QD001', 'T002', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C027', 'Python及其应用', '1', '3.0', NULL, '2', 'QD001', 'T003', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C028', '体育A4', '1', '1.0', NULL, '2', 'QD001', 'T004', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C029', '操作系统', '1', '4.0', NULL, '2', 'QD001', 'T005', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C030', '计算机网络', '1', '3.0', NULL, '2', 'QD001', 'T004', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C031', '算法设计与分析', '1', '3.0', NULL, '2', 'QD001', 'T003', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C032', '系统设计与分析', '1', '3.0', NULL, '2', 'QD001', 'T002', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C033', '毛泽东思想和中国特色社会主义理论A1', '1', '2.5', NULL, '3', 'QD001', 'T002', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C034', '信息搜索与分析', '1', '1.0', NULL, '3', 'QD001', 'T003', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C035', '毛泽东思想和中国特色社会主义理论A2', '1', '2.0', NULL, '3', 'QD001', 'T005', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C036', '大学生就业与创业指导', '1', '1.0', NULL, '3', 'QD001', 'T004', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C037', '形势与政策', '1', '2.0', NULL, '3', 'QD001', 'T003', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C038', '云计算技术', '1', '3.0', NULL, '3', 'QD001', 'T002', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C039', '软件质量保证与测试', '1', '3.0', NULL, '3', 'QD001', 'T001', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C040', '个性化线上课程包（一）', '1', '3.0', NULL, '4', 'QD001', 'T001', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C041', '个性化线上课程包（二）', '1', '3.0', NULL, '4', 'QD001', 'T002', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C042', '个性化线上课程包（三）', '1', '3.0', NULL, '4', 'QD001', 'T003', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C043', '软件产品综合实践', '1', '8.0', NULL, '4', 'QD001', 'T004', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C044', '毕业教育', '1', '0.0', NULL, '4', 'QD001', 'T005', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_1_C045', '毕业设计', '1', '10.0', NULL, '4', 'QD001', 'T005', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_2_C001', '足球赛事欣赏', '2', '2.0', 100, '1', 'QD001', 'T004', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_2_C002', '数学——宇宙的语言（智慧树）', '2', '2.0', 200, '1', 'QD001', 'T005', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_2_C003', '上大学，不迷茫（智慧树）', '2', '2.0', 50, '2', 'QD001', 'T005', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_2_C004', '大学生创业概论与实践（智慧树）', '2', '2.0', 300, '2', 'QD001', 'T004', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_2_C005', 'Web应用开发基础', '2', '3.0', 50, '3', 'QD001', 'T001', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_2_C006', '数据仓库与数据挖掘', '2', '3.0', 100, '3', 'QD001', 'T001', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_2_C007', 'Hadoop编程技术', '2', '3.0', 50, '3', 'QD001', 'T002', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_2_C008', 'NoSQL数据库技术', '2', '3.0', 80, '3', 'QD001', 'T001', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_2_C009', '数据分析方法', '2', '3.0', 200, '3', 'QD001', 'T002', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_2_C010', '大数据处理技术', '2', '3.0', 200, '3', 'QD001', 'T003', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_3_C001', '数据结构与算法课程实践', '3', '2.0', NULL, '2', 'QD001', 'T003', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_3_C002', '数据库技术课程实践', '3', '2.0', NULL, '2', 'QD001', 'T001', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_3_C003', '项目实训（一）', '3', '4.0', NULL, '3', 'QD001', 'T003', NULL, NULL);
INSERT INTO `tb_profession_course` VALUES ('QD_3_C004', '项目实训（二）', '3', '4.0', NULL, '3', 'QD001', 'T004', NULL, NULL);

-- ----------------------------
-- Table structure for tb_scores
-- ----------------------------
DROP TABLE IF EXISTS `tb_scores`;
CREATE TABLE `tb_scores`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '课程号',
  `student_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '学生学号',
  `score` float(11, 0) NULL DEFAULT NULL COMMENT '分数',
  `grade_point` int(11) NULL DEFAULT NULL COMMENT '绩点',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `new_scores_course_no`(`course_no`) USING BTREE,
  INDEX `new_scores_student_no`(`student_no`) USING BTREE,
  CONSTRAINT `new_scores_course_no` FOREIGN KEY (`course_no`) REFERENCES `tb_profession_course` (`course_no`) ON DELETE SET NULL ON UPDATE RESTRICT,
  CONSTRAINT `new_scores_student_no` FOREIGN KEY (`student_no`) REFERENCES `tb_students` (`student_no`) ON DELETE SET NULL ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_scores
-- ----------------------------
INSERT INTO `tb_scores` VALUES (1, 'QD_1_C001', 'S001', 98, 9, '2021-10-25 18:32:58', '2021-10-26 00:00:00');
INSERT INTO `tb_scores` VALUES (2, 'QD_1_C001', 'S002', 88, 8, '2021-10-26 00:00:00', '2021-10-26 00:00:00');

-- ----------------------------
-- Table structure for tb_students
-- ----------------------------
DROP TABLE IF EXISTS `tb_students`;
CREATE TABLE `tb_students`  (
  `student_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学生',
  `student_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '学生姓名',
  `grade_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '年级',
  `college_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '学院',
  `profession_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '专业',
  `class_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '班级',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '个性签名',
  `id_card` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '身份证号',
  `age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  `gender` tinyint(4) NULL DEFAULT NULL COMMENT '性别，1男，2女',
  `year` char(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '入学年份',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`student_no`) USING BTREE,
  INDEX `student_no`(`student_no`) USING BTREE,
  INDEX `new_ss_class_no`(`class_no`) USING BTREE,
  INDEX `new_ss_grade`(`college_no`) USING BTREE,
  INDEX `new_ss_grade_no`(`grade_no`) USING BTREE,
  INDEX `new_ss_profession_no`(`profession_no`) USING BTREE,
  CONSTRAINT `new_ss_class_no` FOREIGN KEY (`class_no`) REFERENCES `tb_profession_class` (`class_no`) ON DELETE SET NULL ON UPDATE RESTRICT,
  CONSTRAINT `new_ss_grade` FOREIGN KEY (`college_no`) REFERENCES `tb_college` (`college_no`) ON DELETE SET NULL ON UPDATE RESTRICT,
  CONSTRAINT `new_ss_grade_no` FOREIGN KEY (`grade_no`) REFERENCES `tb_grade` (`grade_no`) ON DELETE SET NULL ON UPDATE RESTRICT,
  CONSTRAINT `new_ss_profession_no` FOREIGN KEY (`profession_no`) REFERENCES `tb_profession` (`profession_no`) ON DELETE SET NULL ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_students
-- ----------------------------
INSERT INTO `tb_students` VALUES ('S001', '小李子', '4', 'QD', 'QD001', 'QD1_18_003', '小栗子，举个栗子', '45252525', 18, 1, '2018', NULL, NULL);
INSERT INTO `tb_students` VALUES ('S002', '小红', '1', 'QD', 'QD001', 'QD1_18_002', '小红同学', '468468468', 20, 2, '2018', NULL, NULL);
INSERT INTO `tb_students` VALUES ('S003', '小张', '2', 'QD', 'QD001', 'QD1_18_001', '小张同学', '135135135', 21, 1, '2018', NULL, NULL);
INSERT INTO `tb_students` VALUES ('S004', '卢姥爷', '3', 'QD', 'QD001', 'QD1_18_003', '卢本伟卢姥爷同学', '696969', 66, 1, '2018', NULL, NULL);

-- ----------------------------
-- Table structure for tb_teachers
-- ----------------------------
DROP TABLE IF EXISTS `tb_teachers`;
CREATE TABLE `tb_teachers`  (
  `teacher_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '教师',
  `teacher_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '教师名字',
  `gender` tinyint(4) NULL DEFAULT NULL COMMENT '1男，2女',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`teacher_no`) USING BTREE,
  INDEX `teacher_no`(`teacher_no`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_teachers
-- ----------------------------
INSERT INTO `tb_teachers` VALUES ('T001', '李老师', 1, '2020-12-29 00:00:00', '2020-12-29 00:00:00');
INSERT INTO `tb_teachers` VALUES ('T002', '吴老师', 2, '2020-12-29 00:00:00', '2020-12-29 00:00:00');
INSERT INTO `tb_teachers` VALUES ('T003', '陈老师', 1, '2020-12-29 00:00:00', '2021-01-07 00:00:00');
INSERT INTO `tb_teachers` VALUES ('T004', '韦老师', 1, '2021-01-05 00:00:00', '2021-01-05 00:00:00');
INSERT INTO `tb_teachers` VALUES ('T005', '杨老师', 1, '2021-01-05 00:00:00', '2021-01-05 00:00:00');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` tinyint(4) NULL DEFAULT NULL COMMENT '1管理员，2学生，3老师',
  `username` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '账号',
  `password` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'MD5密码',
  `psd` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `student_teacher_no` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '学号或工号',
  `display_name` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示的名字',
  `state` tinyint(4) NOT NULL DEFAULT 1 COMMENT '账号状态，1正常，2封禁',
  `create_time` datetime(0) NULL DEFAULT NULL,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES (1, 1, 'admin', '202cb962ac59075b964b07152d234b70', '123', '0', '管理员', 1, '2020-12-29 10:48:24', '2020-12-29 10:48:26');
INSERT INTO `tb_user` VALUES (10, 2, 'S003', 'caf1a3dfb505ffed0d024130f58c5cfa', '321', 'S003', '小杨同学', 1, '2020-12-30 00:00:00', '2020-12-30 00:00:00');
INSERT INTO `tb_user` VALUES (12, 3, 'T002', '202cb962ac59075b964b07152d234b70', '123', 'T002', '吴老师', 1, '2021-01-02 00:00:00', '2021-01-02 00:00:00');
INSERT INTO `tb_user` VALUES (15, 3, 'T001', 'a8ae104615cb4e966ddb435f3e575a02', '12333', 'T001', '李老师', 1, '2021-01-04 00:00:00', '2021-01-04 00:00:00');
INSERT INTO `tb_user` VALUES (16, 3, 'T003', '202cb962ac59075b964b07152d234b70', '123', 'T003', '王老师', 1, '2021-01-04 00:00:00', '2021-01-04 00:00:00');
INSERT INTO `tb_user` VALUES (18, 2, 'S001', 'e10adc3949ba59abbe56e057f20f883e', '123456', 'S001', '小李子', 1, '2021-10-26 00:00:00', '2021-10-26 00:00:00');
INSERT INTO `tb_user` VALUES (19, 3, 'T004', 'e10adc3949ba59abbe56e057f20f883e', '123456', 'T004', '韦老师', 1, '2021-10-26 00:00:00', '2021-10-26 00:00:00');

SET FOREIGN_KEY_CHECKS = 1;
